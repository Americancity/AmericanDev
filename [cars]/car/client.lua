-- No need to modify any of this, but I tried to document what it's doing
local isBlackedOut = false
local oldBodyDamage = 0
local oldSpeed = 0
Config = {} 
Config.DamageNeeded = 100.0 -- 100.0 being broken and 1000.0 being fixed a lower value than 100.0 will break it

local function blackout()
  -- Only blackout once to prevent an extended blackout if both speed and damage thresholds were met
  if not isBlackedOut then
    isBlackedOut = true
    -- This thread will black out the user's screen for the specified time
    Citizen.CreateThread(function()
      DoScreenFadeOut(100)
      while not IsScreenFadedOut() do
        Citizen.Wait(0)
      end
      Citizen.Wait(Config.BlackoutTime)
      DoScreenFadeIn(250)
      isBlackedOut = false
    end)
  end
end

Citizen.CreateThread(function()
  while true do
    Citizen.Wait(0)
    -- Get the vehicle the player is in, and continue if it exists
    local vehicle = GetVehiclePedIsIn(PlayerPedId(), false)
    if DoesEntityExist(vehicle) then
      -- Check if damage blackout is enabled
      if Config.BlackoutFromDamage then
        local currentDamage = GetVehicleBodyHealth(vehicle)
        -- If the damage changed, see if it went over the threshold and blackout if necesary
        if currentDamage ~= oldBodyDamage then
          if not isBlackedOut and (currentDamage < oldBodyDamage) and ((oldBodyDamage - currentDamage) >= Config.BlackoutDamageRequired) then
            blackout()
          end
          oldBodyDamage = currentDamage
        end
      end
      
      -- Check if speed blackout is enabled
      if Config.BlackoutFromSpeed then
        local currentSpeed = GetEntitySpeed(vehicle) * 2.23
        -- If the speed changed, see if it went over the threshold and blackout if necesary
        if currentSpeed ~= oldSpeed then
          if not isBlackedOut and (currentSpeed < oldSpeed) and ((oldSpeed - currentSpeed) >= Config.BlackoutSpeedRequired) then
            blackout()
          end
          oldSpeed = currentSpeed
        end
      end
    else
      oldBodyDamage = 0
      oldSpeed = 0
    end
    
    if isBlackedOut and Config.DisableControlsOnBlackout then
      -- Borrowed controls from https://github.com/Sighmir/FiveM-Scripts/blob/master/vrp/vrp_hotkeys/client.lua
      DisableControlAction(0,71,true) -- veh forward
      DisableControlAction(0,72,true) -- veh backwards
      DisableControlAction(0,63,true) -- veh turn left
      DisableControlAction(0,64,true) -- veh turn right
      DisableControlAction(0,75,true) -- disable exit vehicle
    end
  end
end)


local knockedOut = false
local wait = 60
local count = 60
local wait2 = 8

Citizen.CreateThread(function()
  while true do
    Wait(1)
    local myPed = GetPlayerPed(-1)
    if IsPedInMeleeCombat(myPed) then
      if GetEntityHealth(myPed) < 115 then
      	blackoutKO()
        SetPlayerInvincible(PlayerId(), true)
        SetPedToRagdoll(myPed, 1000, 1000, 0, 0, 0, 0)
        ShowNotification("~r~ KO!")
        wait = 60
        knockedOut = true
        SetEntityHealth(myPed, 116)
      end
    elseif GetEntityHealth(myPed) < 115 and not IsPedInMeleeCombat(myPed)and GetEntityHealth(myPed) > 1 then
        SetPedToRagdoll(myPed, 1000, 1000, 0, 0, 0, 0)
        wait2 = 8
        knockedOutnc = true
        SetEntityHealth(myPed, 116)
    end
    if knockedOutnc == true then
      DisablePlayerFiring(PlayerId(), true)
      SetPedToRagdoll(myPed, 1000, 1000, 0, 0, 0, 0)
      ResetPedRagdollTimer(myPed)
      
      if wait2 >= 0 then
        count = count - 1
        if count == 0 then
          count = 60
          wait2 = wait2 - 1
        end
      else
        SetPlayerInvincible(PlayerId(), false)
        knockedOutnc = false
      end
    elseif knockedOut == true then
      SetPlayerInvincible(PlayerId(), true)
      DisablePlayerFiring(PlayerId(), true)
      SetPedToRagdoll(myPed, 1000, 1000, 0, 0, 0, 0)
      ResetPedRagdollTimer(myPed)
      
      if wait >= 0 then
        count = count - 1
        if count == 0 then
          count = 60
          wait = wait - 1
          SetEntityHealth(myPed, GetEntityHealth(myPed)+4)
        end
      else
        SetPlayerInvincible(PlayerId(), false)
        knockedOut = false
      end
    end
  end
end)

function ShowNotification(text)
  SetNotificationTextEntry("STRING")
  AddTextComponentString(text)
  DrawNotification(false, false)
end

function blackoutKO()
	-- Only blackout once to prevent an extended blackout if both speed and damage thresholds were met
	if not isBlackedOut then
		isBlackedOut = true
		-- This thread will black out the user's screen for the specified time
		Citizen.CreateThread(function()
			DoScreenFadeOut(100)
			while not IsScreenFadedOut() do
				Citizen.Wait(0)
			end
			Citizen.Wait(57000)
			DoScreenFadeIn(250)
			isBlackedOut = false
		end)
	end
end
