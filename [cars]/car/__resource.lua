resource_manifest_version '77731fab-63ca-442c-a67b-abc70f28dfa5'

data_file 'HANDLING_FILE' 'handling.meta'
data_file 'VEHICLE_METADATA_FILE' 'vehicles.meta'

files {
    'vehicles.meta',
	'handling.meta'
}
client_scripts {
  'config.lua',
  'client.lua'
}