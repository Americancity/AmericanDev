Config                			= {}
Config.DrawDistance   			= 0
Config.Size           			= { x = 2.0, y = 2.0, z = 1.5 }
Config.Color          			= { r = 255, g = 0, b = 0 }
Config.Type           			= 28
Config.Locale         			= 'fr'
Config.EnableLicense  			= true
Config.LicensePrice   			= 5000
Config.EnableClipGunShop		= true
Config.EnableClipBlackWeashop	= true

Config.EnableClip = {
	GunShop = {
		Price = 5000,
		Label = "Chargeur"
	},
	BlackWeashop = {
		Price = 8750,
		Label = "Chargeur"
	}
}

Config.Zones = {

    GunShop = {
        legal = 0,
        Items = {
        {name  = "WEAPON_PISTOL", price = 3000, label = "M9"},
        {name  = "WEAPON_SNSPISTOL", price = 2100, label = "SNS"},
        {name  = "WEAPON_SNSPISTOL_MK2", price = 4000, label = "SNS V2"},
        {name  = "WEAPON_FLAREGUN", price = 800, label = "Pistolet de detresse"},
        {name  = "WEAPON_KNIFE", price = 126, label = "Couteau de chasse"},
        {name  = "WEAPON_VINTAGEPISTOL", price = 3500, label = "Pistolet vintage"},
        {name  = "WEAPON_STUNGUN", price = 355, label = "Tazer"},
        {name  = "clip", price = 450, label = "boite de munitions"},
        },
        Pos   = {
            { x = -662.180,   y = -934.961,   z = 20.829 },
            { x = 810.25,     y = -2157.60,   z = 28.62 },
            { x = 1693.44,    y = 3760.16,    z = 33.71 },
            { x = -330.24,    y = 6083.88,    z = 30.45 },
            { x = 252.63,     y = -50.00,     z = 68.94 },
            { x = 22.09,      y = -1107.28,   z = 28.80 },
            { x = 2567.69,    y = 294.38,     z = 107.73 },
            { x = -1117.58,   y = 2698.61,    z = 17.55 },
            { x = 842.44,     y = -1033.42,   z = 27.19 },

        }
    },

    BlackWeashop = {
        legal = 1,
        Items = {
        {name  = "WEAPON_PISTOL_MK2", price = 1500, label = "M9 V2 - 5 500$"},
        {name  = "WEAPON_PISTOL50", price = 7500, label = "Desert Egle - 25 000$"},
        {name  = "WEAPON_HEAVYPISTOL", price = 1700, label = "M1911 - 6 000$"},
        {name  = "WEAPON_MICROSMG", price = 2500, label = "Micro SMG - 13 000$"},
        {name  = "WEAPON_SMG", price = 2700, label = "MP5 - 15 000$"},
        {name  = "WEAPON_ASSAULTSMG", price = 100, label = "WIP"},
        {name  = "WEAPON_MG", price = 20000, label = "PKP - 60 000$"},
        {name  = "WEAPON_COMBATPDW", price = 4500, label = "PDW - 20 000$"},
        {name  = "WEAPON_GUSENBERG", price = 18000, label = "Tompson tambour - 45 000$"},
        {name  = "WEAPON_MACHINEPISTOL", price = 1700, label = "Tek 9 - 9 000"},
        {name  = "WEAPON_ASSAULTRIFLE", price = 8500, label = "AK 47 - 21 000$"},
        {name  = "WEAPON_ASSAULTRIFLE_MK2", price = 100, label = "WIP "},
        {name  = "WEAPON_CARBINERIFLE", price = 100, label = "WIP - M4"},
        {name  = "WEAPON_ADVANCEDRIFLE", price = 100, label = "WIP"},
        {name  = "WEAPON_SPECIALCARBINE", price = 24000, label = "G36 - 48 000$"},
        {name  = "WEAPON_BULLPUPRIFLE", price = 100, label = "WIP"},
        {name  = "WEAPON_COMPACTRIFLE", price = 8500, label = "AK 74 U - 24 000$"},
        {name  = "WEAPON_PUMPSHOTGUN", price = 6500, label = "Fusil a pompe 20 000$"},
        {name  = "WEAPON_SWEEPERSHOTGUN", price = 50000, label = "Fusil a pompe tambour - 170 000$"},
        {name  = "WEAPON_SAWNOFFSHOTGUN", price = 2500, label = "Fusil a pompe scié - 24 000$"},
        {name  = "WEAPON_BULLPUPSHOTGUN", price = 45000, label = "Fusil a pompe Bullpup - 145 000$"},
        {name  = "WEAPON_DBSHOTGUN", price = 8500, label = "Double canon scié - 15 000$"},
        {name  = "WEAPON_SNIPERRIFLE", price = 50000, label = "Sniper 125 000$"},
        {name  = "WEAPON_BULLPUPRIFLE_MK2", price = 100, label = "WIP"},
        {name  = "WEAPON_SMOKEGRENADE", price = 8500, label = "Gaz lacrimo x1 - 13 000$"},
        {name  = "WEAPON_HEAVYSNIPER", price = 85000, label = "Sniper calibre 50 200 000$"},
        {name  = "WEAPON_STICKYBOMB", price = 85000, label = "C4 x1 150 000$"},
        {name  = "WEAPON_KNUCLE", price = 250, label = "Poin américain - 850$"},
        {name  = "WEAPON_DAGGER", price = 100, label = "Dague ancienne - 1 000$"},
        {name  = "WEAPON_Machete", price = 100, label = "Machette - 850$"},
        {name  = "WEAPON_SWITCHBLADE", price = 100, label = "Couteau papillon - 650$"},
        {name  = "WEAPON_MOLOTOV", price = 100, label = "Coctail molotov - 75 000$"},
        {name  = "clip", price = 1500, label = "boite de munitions"},
        },
        Pos   = {
            { x = 887.38726,   y = -952.561462,  z = 38.70 },
        }
    },
    Handshop = {
        legal = 2,
        Items = {
        {name  = "WEAPON_BAT", price = 120, label = "Batte de baseball"},
        {name  = "WEAPON_GOLFCLUB", price = 210, label = "Club de golf"},
        {name  = "WEAPON_FLARE", price = 75, label = "Baton de détresse"},
        {name  = "WEAPON_HAMMER", price = 45, label = "Marteau"},
        {name  = "WEAPON_CROWBAR", price = 85, label = "Pied de biche"},
        {name  = "WEAPON_BOTTLE", price = 21, label = "Bouteille de vin (mauvais état)"},
        {name  = "WEAPON_HATCHET", price = 65, label = "Hache"},
        {name  = "WEAPON_FLASHLIGHT", price = 20, label = "Lamp torche"},
        {name  = "WEAPON_BALL", price = 7, label = "Ball de baseball"},
        {name  = "WEAPON_POOLCUE", price = 35, label = "Queu de billard"}

        },
        Pos   = {
            { x = 68.91,   y = -1569.59,  z = 29.59 },
        }
    },

}
