Locales['fr'] = {
  ['whitelist_check']     = 'making sure you\'re whitelisted on this server . . .',
  ['not_whitelisted']     = 'Le serveur est en cour de déveulopement https://discord.gg/bbsqVjD',
  ['steamid_error']       = 'nous n\'arrivons pas à lire votre SteamID',
  ['whitelist_empty']     = 'the whitelist hasn\'t been loaded yet, or alternatively no one has been whitelisted!',
  ['help_whitelist_add']  = 'ajouter quelqu\'un dans la Whitelist',
  ['help_whitelist_load'] = 'recharger la Whitelist',
}
