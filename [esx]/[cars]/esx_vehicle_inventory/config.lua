--Truck
Config	=	{}

 -- Limit, unit can be whatever you want. Originally grams (as average people can hold 25kg)
Config.Limit = 25000

-- Default weight for an item:
	-- weight == 0 : The item do not affect character inventory weight
	-- weight > 0 : The item cost place on inventory
	-- weight < 0 : The item add place on inventory. Smart people will love it.
Config.DefaultWeight = 2000



-- If true, ignore rest of file
Config.WeightSqlBased = false

-- I Prefer to edit weight on the config.lua and I have switched Config.WeightSqlBased to false:

Config.localWeight = {
 bandage = 200,
 beer = 500,
 cigaretpack = 30,
 cigarett = 3,
 clip = 2000,
 chocolate = 100,
 coffe = 300,
 black_money = 0.1,
 WEAPON_PISTOL = 1500,
 bread = 75,  		--30
 canette = 2000,
 cannabis = 2000,
 cupcake = 100,
 ecola = 1000,
 hamburger = 100,
 milk = 100,
 saucisson = 1000,
 phone = 50,
 sprunk = 200,
 sandwich = 100,
 plongee1 = 20050,
 plongee2 = 23750,
 marijuana = 50,
 jambon = 2000,
 lighter = 150,
 water = 150,		--30
 fish = 100,
 stone = 2000,
 tequila = 200,
 vodka = 200,
 viande = 2000,
 washed_stone = 2000,
 copper = 250,
 iron = 332,
 gold = 665,
 diamond = 1000,
 wood = 1500,
 cutted_wood = 1500,
 packaged_plank = 200,
 petrol = 200,
 petrol_raffin = 400,
 essence = 200,
 whool = 300,
 fabric = 150,
 clothe = 330,
 gazbottle = 13000,
 fixtool = 500,
 carotool = 900,
 blowpipe = 1200,
 fixkit = 500,
 carokit = 500,
 bandage = 50,
 medikit = 300,
 weed = 50,
 weed_pooch = 250,
 meth = 50,
 meth_pooch = 250,
 coke = 50,
 coke_pooch = 250,
 opium = 50,
 opium_pooch = 250,
 croquettes = 50,
 raisin = 1000,
 jus_raisin = 400,
 vine = 400,
 grand_cru = 400,
 jusfruit = 600,
 clip = 1000,
 cola = 500,
 brolly = 200,
 bong = 500,
 whisky = 300,
 rose = 10,
 notepad = 10,
 icetea = 500,
 grapperaisin = 50,
 alive_chicken = 800,
 slaughtered_chicken = 800,
 packaged_chicken =160,
 croco = 50
}

Config.VehicleLimit ={
    [0]=100000,
    [1]=12000,
    [2]=16000,
    [3]=100000,
    [4]=100000,
    [5]=60000,
    [6]=60000,
    [7]=60000,
    [8]=20000,
    [9]=100000,
    [10]=300000,
    [11]=160000,
    [12]=150000,
    [13]=10000,
    [14]=18000,
    [15]=200000,
    [16]=300000,
    [17]=100000,
    [18]=100000,
    [19]=100000,
    [20]=300000,
}
