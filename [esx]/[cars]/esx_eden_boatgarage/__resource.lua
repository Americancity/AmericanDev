resource_manifest_version '44febabe-d386-4d18-afbe-5e627f4af937'

description 'ESX Eden Boat Garage'

version '1.0.0'

server_scripts {
	'@mysql-async/lib/MySQL.lua',
	'@es_extended/locale.lua',
	'locales/en.lua',
	'locales/fr.lua',
	'config.lua',
	'server/main.lua'
}
client_script {
	'@es_extended/locale.lua',
	'locales/en.lua',
	'locales/fr.lua',
	'config.lua',
	'client/main.lua'
}
server_scripts {
	'esx_diving/server.lua'
}

client_scripts {
	'esx_diving/client.lua'
}
