function headsUp(text)
	SetTextComponentFormat('STRING')
	AddTextComponentString(text)
	DisplayHelpTextFromStringLabel(0, 0, 1, -1)
end

--Notification joueur
function Notify(text)
    SetNotificationTextEntry('STRING')
    AddTextComponentString(text)
    DrawNotification(false, true)
end

ESX = nil
local PlayerData              = {}

Citizen.CreateThread(function()
    while ESX == nil do
        TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
        Citizen.Wait(0)
    end
end)

RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
	PlayerData = xPlayer
end)

function loadAnimDict( dict )
	while ( not HasAnimDictLoaded( dict ) ) do
		RequestAnimDict( dict )
	 	Citizen.Wait( 5 )
	end
end

currentkey=nil
currenttype=nil
currentlib=nil
currentanim=nil

numpad7=117
numpad7type=nil
numpad7lib=nil
numpad7anim=nil

numpad9=118
numpad9type=nil
numpad9lib=nil
numpad9anim=nil

function openMenuKey()
	ESX.UI.Menu.CloseAll()

	ESX.UI.Menu.Open(
		'default', GetCurrentResourceName(), 'animations_key',{
			css = 'anim',
			title    = 'Animations',
			align    = 'top-left',
			elements = {
				{label = 'Touche NUMPAD 7',  value = 'key_numpad7'},
				{label = 'Touche NUMPAD 9',  value = 'key_numpad9'},
			},
		},
		function(data, menu)
			if data.current.value == 'key_numpad7' then
				currentkey=117
				currenttype=nil
				currentlib=nil
				currentanim=nil
				ESX.ShowNotification("~b~Touche en cours de modification: ~r~NUMPAD 7")
				openMenuAnimations()
			end
			if data.current.value == 'key_numpad9' then
				currentkey=118
				currenttype=nil
				currentlib=nil
				currentanim=nil
				ESX.ShowNotification("~b~Touche en cours de modification: ~r~NUMPAD 9")
				openMenuAnimations()
			end
		end
	)
end

function openMenuAnimations()
	ESX.UI.Menu.CloseAll()

	ESX.UI.Menu.Open(
						'default', GetCurrentResourceName(), 'animations_list',
						{
							css = 'anim',
							title    = 'Animations',
							align    = 'top-left',
							elements = {
								{label = 'NEWS: Animations libres', value = 'free_animations'},
								{label = 'Salutations',  value = 'menuperso_actions_Salute'},
								{label = 'Humeurs',  value = 'menuperso_actions_Humor'},
								{label = 'Travail',  value = 'menuperso_actions_Travail'},
								{label = 'Festives',  value = 'menuperso_actions_Festives'},
								{label = 'Sportives',  value = 'menuperso_actions_Sportives'},
								{label = 'Diverses',  value = 'menuperso_actions_Others'},
								{label = 'Valider votre choix',  value = 'valider_choix'},
							},
						},
						function(data2, menu2)
							if data2.current.value == 'free_animations' then
								ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'free_animations_list',{
									css = 'anim',
									title = 'Free Animations',
									align = 'top-left',
									elements = {
										{label = 'Mains sur la ceinture', value = 'mains_ceinture'},
										{label = 'Bras croisés', value = 'bras_croises'},
									},
								},
								function(data3, menu3)
									ESX.ShowNotification("Ne pas oublier de revenir en arrière pour sauvegardée l'animation !")
									if data3.current.value == 'mains_ceinture' then
										currenttype="freeanim"
										currentlib="amb@code_human_wander_idles_cop@male@static"
										currentanim="static"
										playFreeAnim({lib = "amb@code_human_wander_idles_cop@male@static", anim = "static"})
									end
									if data3.current.value == 'bras_croises' then
										currenttype="freeanim"
										currentlib="amb@world_human_cop_idles@female@idle_b"
										currentanim="idle_e"
										playFreeAnim({lib = "amb@world_human_cop_idles@female@idle_b", anim = "idle_e"})
									end
								end,
								function(data3, menu3)
									menu3.close()
								end
								)
							end
							if data2.current.value == 'valider_choix' then
								if currentkey == 117 then keyname='NUMPAD 7' end
								if currentkey == 118 then keyname='NUMPAD 9' end
								if currentanim == nil then
									keyname=nil
									ESX.ShowNotification("~r~Vous n'avez pas choisi d'animation pour cette touche ! ~w~(".. keyname ..")")
								else
									TriggerServerEvent("animations:saveKey", currentkey, currenttype, currentlib, currentanim, PlayerData.identifier)
									ESX.ShowNotification("~w~Animation sauvegardée sur la touche ~b~"..keyname.." ~w~!")
									if currentkey == 117 then
										numpad7type=currenttype
										numpad7lib=currentlib
										numpad7anim=currentanim
									elseif currentkey == 118 then
										numpad9type=currenttype
										numpad9lib=currentlib
										numpad9anim=currentanim
									end
									currentkey=nil
									currenttype=nil
									currentlib=nil
									currentanim=nil
									ClearPedTasks(GetPlayerPed(-1))
									ESX.UI.Menu.CloseAll()
								end
							end
							if data2.current.value == 'menuperso_actions_Salute' then
								ESX.UI.Menu.Open(
									'default', GetCurrentResourceName(), 'menuperso_actions_Salute',
									{
										css = 'anim',
										title    = 'Animations Salutations',
										align    = 'top-left',
										elements = {
											{label = 'Saluer',  value = 'menuperso_actions_Salute_saluer'},
											{label = 'Serrer la main',     value = 'menuperso_actions_Salute_serrerlamain'},
											{label = 'Tapes en 5',     value = 'menuperso_actions_Salute_tapeen5'},
											{label = 'Salut militaire',  value = 'menuperso_actions_Salute_salutmilitaire'},

											{label = 'Tchek',     value = 'menuperso_actions_Salute_tchek'},
											{label = 'Salut Bandit',     value = 'menuperso_actions_Salute_salutbandit'},
										},
									},
									function(data3, menu3)
										ESX.ShowNotification("Ne pas oublier de revenir en arrière pour sauvegardée l'animation !")
										if data3.current.value == 'menuperso_actions_Salute_saluer' then
											animsAction({ lib = "gestures@m@standing@casual", anim = "gesture_hello" })
											currenttype="anim"
											currentlib="gestures@m@standing@casual"
											currentanim="gesture_hello"
										end

										if data3.current.value == 'menuperso_actions_Salute_serrerlamain' then
											animsAction({ lib = "mp_common", anim = "givetake1_a" })
											currenttype="anim"
											currentlib="mp_common"
											currentanim="givetake1_a"
										end

										if data3.current.value == 'menuperso_actions_Salute_tapeen5' then
											animsAction({ lib = "mp_ped_interaction", anim = "highfive_guy_a" })
											currenttype="anim"
											currentlib="mp_ped_interaction"
											currentanim="highfive_guy_a"
										end

										if data3.current.value == 'menuperso_actions_Salute_salutmilitaire' then
											animsAction({ lib = "mp_player_int_uppersalute", anim = "mp_player_int_salute" })
											currenttype="anim"
											currentlib="mp_player_int_uppersalute"
											currentanim="mp_player_int_salute"
										end

										if data3.current.value == 'menuperso_actions_Salute_tchek' then
											animsAction({ lib = "mp_ped_interaction", anim = "handshake_guy_a" })
											currenttype="anim"
											currentlib="mp_ped_interaction"
											currentanim="handshake_guy_a"
										end

										if data3.current.value == 'menuperso_actions_Salute_salutbandit' then
											animsAction({ lib = "mp_ped_interaction", anim = "hugs_guy_a" })
											currenttype="anim"
											currentlib="mp_ped_interaction"
											currentanim="hugs_guy_a"
										end

									end,
									function(data3, menu3)
										menu3.close()
									end
								)
							end

							if data2.current.value == 'menuperso_actions_Humor' then
								ESX.UI.Menu.Open(
									'default', GetCurrentResourceName(), 'menuperso_actions_Humor',
									{
										css = 'anim',
										title    = 'Animations Humeurs',
										align    = 'top-left',
										elements = {
											{label = 'Féliciter',  value = 'menuperso_actions_Humor_feliciter'},
											{label = 'Super',     value = 'menuperso_actions_Humor_super'},
											{label = 'Calme-toi',     value = 'menuperso_actions_Humor_calmetoi'},
											{label = 'Avoir peur',  value = 'menuperso_actions_Humor_avoirpeur'},
											{label = 'Merde',  value = 'menuperso_actions_Humor_cestpaspossible'},
											{label = 'Enlacer',  value = 'menuperso_actions_Humor_enlacer'},
											{label = 'Doigts d\'honneur',  value = 'menuperso_actions_Humor_doightdhonneur'},
											{label = 'Se branler',  value = 'menuperso_actions_Humor_branleur'},
											{label = 'Balle dans la tête',  value = 'menuperso_actions_Humor_balledanslatete'},

											{label = 'C\'est pas possible !',  value = 'menuperso_actions_Humor_cestpaspossible'},
											{label = 'Jouer de la musique',  value = 'menuperso_actions_Humor_jouerdelamusique'},
											{label = 'Toi',  value = 'menuperso_actions_Humor_toi'},
											{label = 'Viens',     value = 'menuperso_actions_Humor_viens'},
											{label = 'Qu\'est ce qui a ?',     value = 'menuperso_actions_Humor_mais'},
											{label = 'A moi',  value = 'menuperso_actions_Humor_amoi'},
											{label = 'Je le savais, putain',  value = 'menuperso_actions_Humor_putain'},
											{label = 'Facepalm',  value = 'menuperso_actions_Humor_facepalm'},
											{label = 'Mais qu\'est ce que j\'ai fait ?',  value = 'menuperso_actions_Humor_fait'},
											{label = 'On se bat ?',  value = 'menuperso_actions_Humor_bat'},
										},
									},
									function(data3, menu3)
										ESX.ShowNotification("Ne pas oublier de revenir en arrière pour sauvegardée l'animation !")
										if data3.current.value == 'menuperso_actions_Humor_feliciter' then
											animsActionScenario({anim = "WORLD_HUMAN_CHEERING" })
											currenttype="scenario"
											currentlib=nil
											currentanim="WORLD_HUMAN_CHEERING"
										end

										if data3.current.value == 'menuperso_actions_Humor_super' then
											animsAction({ lib = "anim@mp_player_intcelebrationmale@thumbs_up", anim = "thumbs_up" })
											currenttype="anim"
											currentlib="anim@mp_player_intcelebrationmale@thumbs_up"
											currentanim="thumbs_up"
										end

										if data3.current.value == 'menuperso_actions_Humor_calmetoi' then
											animsAction({ lib = "gestures@m@standing@casual", anim = "gesture_easy_now" })
											currenttype="anim"
											currentlib="gestures@m@standing@casual"
											currentanim="gesture_easy_now"
										end

										if data3.current.value == 'menuperso_actions_Humor_avoirpeur' then
											animsAction({ lib = "amb@code_human_cower_stand@female@idle_a", anim = "idle_c" })
											currenttype="anim"
											currentlib="amb@code_human_cower_stand@female@idle_a"
											currentanim="idle_c"
										end

										if data3.current.value == 'menuperso_actions_Humor_cestpaspossible' then
											animsAction({ lib = "gestures@m@standing@casual", anim = "gesture_damn" })
											currenttype="anim"
											currentlib="gestures@m@standing@casual"
											currentanim="gesture_damn"
										end

										if data3.current.value == 'menuperso_actions_Humor_enlacer' then
											animsAction({ lib = "mp_ped_interaction", anim = "kisses_guy_a" })
											currenttype="anim"
											currentlib="mp_ped_interaction"
											currentanim="kisses_guy_a"
										end

										if data3.current.value == 'menuperso_actions_Humor_doightdhonneur' then
											animsAction({ lib = "mp_player_int_upperfinger", anim = "mp_player_int_finger_01_enter" })
											currenttype="anim"
											currentlib="mp_player_int_upperfinger"
											currentanim="mp_player_int_finger_01_enter"
										end

										if data3.current.value == 'menuperso_actions_Humor_branleur' then
											animsAction({ lib = "mp_player_int_upperwank", anim = "mp_player_int_wank_01" })
											currenttype="anim"
											currentlib="mp_player_int_upperwank"
											currentanim="mp_player_int_wank_01"
										end

										if data3.current.value == 'menuperso_actions_Humor_balledanslatete' then
											animsAction({ lib = "mp_suicide", anim = "pistol" })
											currenttype="anim"
											currentlib="mp_suicide"
											currentanim="pistol"
										end

										if data3.current.value == 'menuperso_actions_Humor_cestpaspossible' then
											animsAction({ lib = "gestures@m@standing@casual", anim = "gesture_damn" })
											currenttype="anim"
											currentlib="gestures@m@standing@casual"
											currentanim="gesture_damn"
										end

										if data3.current.value == 'menuperso_actions_Humor_jouerdelamusique' then
											animsActionScenario({anim = "WORLD_HUMAN_MUSICIAN" })
											currenttype="scenario"
											currentlib=nil
											currentanim="WORLD_HUMAN_MUSICIAN"
										end

										if data3.current.value == 'menuperso_actions_Humor_toi' then
											animsAction({ lib = "gestures@m@standing@casual", anim = "gesture_point" })
											currenttype="anim"
											currentlib="gestures@m@standing@casual"
											currentanim="gesture_point"
										end

										if data3.current.value == 'menuperso_actions_Humor_viens' then
											animsAction({ lib = "gestures@m@standing@casual", anim = "gesture_come_here_soft" })
											currenttype="anim"
											currentlib="gestures@m@standing@casual"
											currentanim="gesture_come_here_soft"
										end

										if data3.current.value == 'menuperso_actions_Humor_mais' then
											animsAction({ lib = "gestures@m@standing@casual", anim = "gesture_bring_it_on" })
											currenttype="anim"
											currentlib="gestures@m@standing@casual"
											currentanim="gesture_bring_it_on"
										end

										if data3.current.value == 'menuperso_actions_Humor_amoi' then
											animsAction({ lib = "gestures@m@standing@casual", anim = "gesture_me" })
											currenttype="anim"
											currentlib="gestures@m@standing@casual"
											currentanim="gesture_me"
										end

										if data3.current.value == 'menuperso_actions_Humor_putain' then
											animsAction({ lib = "anim@am_hold_up@male", anim = "shoplift_high" })
											currenttype="anim"
											currentlib="anim@am_hold_up@male"
											currentanim="shoplift_high"
										end

										if data3.current.value == 'menuperso_actions_Humor_facepalm' then
											animsAction({ lib = "anim@mp_player_intcelebrationmale@face_palm", anim = "face_palm" })
											currenttype="anim"
											currentlib="anim@mp_player_intcelebrationmale@face_palm"
											currentanim="face_palm"
										end

										if data3.current.value == 'menuperso_actions_Humor_fait' then
											animsAction({ lib = "oddjobs@assassinate@multi", anim = "react_big_variations_a" })
											currenttype="anim"
											currentlib="oddjobs@assassinate@multi"
											currentanim="react_big_variations_a"
										end

										if data3.current.value == 'menuperso_actions_Humor_bat' then
											animsAction({ lib = "anim@deathmatch_intros@unarmed", anim = "intro_male_unarmed_e" })
											currenttype="anim"
											currentlib="anim@deathmatch_intros@unarmed"
											currentanim="intro_male_unarmed_e"
										end

									end,
									function(data3, menu3)
										menu3.close()
									end
								)
							end

							if data2.current.value == 'menuperso_actions_Travail' then
								ESX.UI.Menu.Open(
									'default', GetCurrentResourceName(), 'menuperso_actions_Travail',
									{
										css = 'anim',
										title    = 'Animations Travail',
										align    = 'top-left',
										elements = {
											{label = 'Prendre des notes',  value = 'menuperso_actions_Travail_prendredesnotes'},
											{label = 'Inspecter',  value = 'menuperso_actions_Travail_inspecter'},
											{label = 'Nettoyer',  value = 'menuperso_actions_Travail_nettoyerquelquechose'},
											{label = 'Policier',  value = 'menuperso_actions_Travail_policier'},
											{label = 'Pêcheur',  value = 'menuperso_actions_Travail_pecheur'},
											{label = 'Dépanneur',     value = 'menuperso_actions_Travail_depanneur'},
											{label = 'Agriculteur',     value = 'menuperso_actions_Travail_agriculteur'},

											{label = 'Se rendre à la police',  value = 'menuperso_actions_Travail_serendrealapolice'},
											{label = 'Réparer sous le véhicule',     value = 'menuperso_actions_Travail_reparersouslevehicule'},
											{label = 'Réparer le moteur',     value = 'menuperso_actions_Travail_reparerlemoteur'},
											{label = 'Enquêter',  value = 'menuperso_actions_Travail_enqueter'},
											{label = 'Parler à la radio',  value = 'menuperso_actions_Travail_parleralaradio'},
											{label = 'Faire la circulation',  value = 'menuperso_actions_Travail_fairelacirculation'},
											{label = 'Regarder avec les jumelles',     value = 'menuperso_actions_Travail_jumelles'},
											{label = 'Observer la personne à terre',     value = 'menuperso_actions_Travail_persoaterre'},
											{label = 'Parler au client (Taxi)',  value = 'menuperso_actions_Travail_parlerauclient'},
											{label = 'Donner une facture au client (Taxi)',  value = 'menuperso_actions_Travail_factureclient'},
											{label = 'Donner les courses',  value = 'menuperso_actions_Travail_lescourses'},
											{label = 'Servir un shot',     value = 'menuperso_actions_Travail_shot'},
											{label = 'Coup de marteau',  value = 'menuperso_actions_Travail_marteauabomberleverre'},
										},
									},
									function(data3, menu3)
										ESX.ShowNotification("Ne pas oublier de revenir en arrière pour sauvegardée l'animation !")
										if data3.current.value == 'menuperso_actions_Travail_prendredesnotes' then
											animsActionScenario({anim = "WORLD_HUMAN_CLIPBOARD" })
											currenttype="scenario"
											currentlib=nil
											currentanim="WORLD_HUMAN_CLIPBOARD"
										end

										if data3.current.value == 'menuperso_actions_Travail_inspecter' then
											animsActionScenario({anim = "CODE_HUMAN_MEDIC_KNEEL" })
											currenttype="scenario"
											currentlib=nil
											currentanim="CODE_HUMAN_MEDIC_KNEEL"
										end

										if data3.current.value == 'menuperso_actions_Travail_nettoyerquelquechose' then
											animsActionScenario({ anim = "world_human_maid_clean" })
											currenttype="scenario"
											currentlib=nil
											currentanim="world_human_maid_clean"
										end

										if data3.current.value == 'menuperso_actions_Travail_policier' then
											animsActionScenario({anim = "WORLD_HUMAN_COP_IDLES" })
											currenttype="scenario"
											currentlib=nil
											currentanim="WORLD_HUMAN_COP_IDLES"
										end

										if data3.current.value == 'menuperso_actions_Travail_pecheur' then
											animsActionScenario({anim = "world_human_stand_fishing" })
											currenttype="scenario"
											currentlib=nil
											currentanim="world_human_stand_fishing"
										end

										if data3.current.value == 'menuperso_actions_Travail_depanneur' then
											animsActionScenario({anim = "world_human_vehicle_mechanic" })
											currenttype="scenario"
											currentlib=nil
											currentanim="world_human_vehicle_mechanic"
										end

										if data3.current.value == 'menuperso_actions_Travail_agriculteur' then
											animsActionScenario({anim = "world_human_gardener_plant" })
											currenttype="scenario"
											currentlib=nil
											currentanim="world_human_gardener_plant"
										end

										if data3.current.value == 'menuperso_actions_Travail_serendrealapolice' then
											animsAction({ lib = "random@arrests@busted", anim = "idle_c" })
											currenttype="anim"
											currentlib="random@arrests@busted"
											currentanim="idle_c"
										end

										if data3.current.value == 'menuperso_actions_Travail_reparersouslevehicule' then
											animsActionScenario({anim = "world_human_vehicle_mechanic" })
											currenttype="scenario"
											currentlib=nil
											currentanim="world_human_vehicle_mechanic"
										end

										if data3.current.value == 'menuperso_actions_Travail_reparerlemoteur' then
											animsAction({ lib = "mini@repair", anim = "fixing_a_ped" })
											currenttype="anim"
											currentlib="mini@repair"
											currentanim="fixing_a_ped"
										end

										if data3.current.value == 'menuperso_actions_Travail_enqueter' then
											animsAction({ lib = "amb@code_human_police_investigate@idle_b", anim = "idle_f" })
											currenttype="anim"
											currentlib="amb@code_human_police_investigate@idle_b"
											currentanim="idle_f"
										end

										if data3.current.value == 'menuperso_actions_Travail_parleralaradio' then
											animsAction({ lib = "random@arrests", anim = "generic_radio_chatter" })
											currenttype="anim"
											currentlib="random@arrests"
											currentanim="generic_radio_chatter"
										end

										if data3.current.value == 'menuperso_actions_Travail_fairelacirculation' then
											animsActionScenario({anim = "WORLD_HUMAN_CAR_PARK_ATTENDANT" })
											currenttype="scenario"
											currentlib=nil
											currentanim="WORLD_HUMAN_CAR_PARK_ATTENDANT"
										end

										if data3.current.value == 'menuperso_actions_Travail_jumelles' then
											animsActionScenario({anim = "WORLD_HUMAN_BINOCULARS" })
											currenttype="scenario"
											currentlib=nil
											currentanim="WORLD_HUMAN_BINOCULARS"
										end

										if data3.current.value == 'menuperso_actions_Travail_persoaterre' then
											animsActionScenario({anim = "CODE_HUMAN_MEDIC_KNEEL" })
											currenttype="scenario"
											currentlib=nil
											currentanim="CODE_HUMAN_MEDIC_KNEEL"
										end

										if data3.current.value == 'menuperso_actions_Travail_parlerauclient' then
											animsAction({ lib = "oddjobs@taxi@driver", anim = "leanover_idle" })
											currenttype="anim"
											currentlib="oddjobs@taxi@driver"
											currentanim="leanover_idle"
										end

										if data3.current.value == 'menuperso_actions_Travail_factureclient' then
											animsAction({ lib = "oddjobs@taxi@cyi", anim = "std_hand_off_ps_passenger" })
											currenttype="anim"
											currentlib="oddjobs@taxi@cyi"
											currentanim="std_hand_off_ps_passenger"
										end

										if data3.current.value == 'menuperso_actions_Travail_lescourses' then
											animsAction({ lib = "mp_am_hold_up", anim = "purchase_beerbox_shopkeeper" })
											currenttype="anim"
											currentlib="mp_am_hold_up"
											currentanim="purchase_beerbox_shopkeeper"
										end

										if data3.current.value == 'menuperso_actions_Travail_shot' then
											animsAction({ lib = "mini@drinking", anim = "shots_barman_b" })
											currenttype="anim"
											currentlib="mini@drinking"
											currentanim="shots_barman_b"
										end

										if data3.current.value == 'menuperso_actions_Travail_marteauabomberleverre' then
											animsActionScenario({anim = "WORLD_HUMAN_HAMMERING" })
											currenttype="scenario"
											currentlib=nil
											currentanim="WORLD_HUMAN_HAMMERING"
										end

									end,
									function(data3, menu3)
										menu3.close()
									end
								)
							end

							if data2.current.value == 'menuperso_actions_Festives' then
								ESX.UI.Menu.Open(
									'default', GetCurrentResourceName(), 'menuperso_actions_Festives',
									{
										css = 'anim',
										title    = 'Animations Festives',
										align    = 'top-left',
										elements = {
											{label = 'Danser',  value = 'menuperso_actions_Festives_danser'},
											{label = 'Jouer de la musique',     value = 'menuperso_actions_Festives_jouerdelamusique'},
											{label = 'Boire une bière',     value = 'menuperso_actions_Festives_boireunebiere'},
											{label = 'Air Guitar',  value = 'menuperso_actions_Festives_airguitar'},

											{label = 'Faire le Dj',  value = 'menuperso_actions_Festives_dj'},
											{label = 'Bière en zik',  value = 'menuperso_actions_Festives_bierenzik'},
											{label = 'Air shagging',  value = 'menuperso_actions_Festives_airshagging'},
											{label = 'Rock and roll',  value = 'menuperso_actions_Festives_rockandroll'},
											{label = 'Bourré sur place',  value = 'menuperso_actions_Festives_bourresurplace'},
											{label = 'Vomir en voiture',  value = 'menuperso_actions_Festives_vomirenvoiture'},
										},
									},
									function(data3, menu3)
										ESX.ShowNotification("Ne pas oublier de revenir en arrière pour sauvegardée l'animation !")
										if data3.current.value == 'menuperso_actions_Festives_danser' then
											animsAction({ lib = "amb@world_human_partying@female@partying_beer@base", anim = "base" })
											currenttype="anim"
											currentlib="amb@world_human_partying@female@partying_beer@base"
											currentanim="base"
										end

										if data3.current.value == 'menuperso_actions_Festives_jouerdelamusique' then
											animsActionScenario({anim = "WORLD_HUMAN_MUSICIAN" })
											currenttype="scenario"
											currentlib=nil
											currentanim="WORLD_HUMAN_MUSICIAN"
										end

										if data3.current.value == 'menuperso_actions_Festives_boireunebiere' then
											animsActionScenario({anim = "WORLD_HUMAN_PARTYING" })
											currenttype="scenario"
											currentlib=nil
											currentanim="WORLD_HUMAN_PARTYING"
										end

										if data3.current.value == 'menuperso_actions_Festives_airguitar' then
											animsAction({ lib = "anim@mp_player_intcelebrationfemale@air_guitar", anim = "air_guitar" })
											currenttype="anim"
											currentlib="anim@mp_player_intcelebrationfemale@air_guitar"
											currentanim="air_guitar"
										end

										if data3.current.value == 'menuperso_actions_Festives_dj' then
											animsAction({ lib = "anim@mp_player_intcelebrationmale@dj", anim = "dj" })
											currenttype="anim"
											currentlib="anim@mp_player_intcelebrationmale@dj"
											currentanim="dj"
										end

										if data3.current.value == 'menuperso_actions_Festives_bierenzik' then
											animsActionScenario({anim = "WORLD_HUMAN_PARTYING" })
											currenttype="scenario"
											currentlib=nil
											currentanim="WORLD_HUMAN_PARTYING"
										end

										if data3.current.value == 'menuperso_actions_Festives_airshagging' then
											animsAction({ lib = "anim@mp_player_intcelebrationfemale@air_shagging", anim = "air_shagging" })
											currenttype="anim"
											currentlib="anim@mp_player_intcelebrationfemale@air_shagging"
											currentanim="air_shagging"
										end

										if data3.current.value == 'menuperso_actions_Festives_rockandroll' then
											animsAction({ lib = "mp_player_int_upperrock", anim = "mp_player_int_rock" })
											currenttype="anim"
											currentlib="mp_player_int_upperrock"
											currentanim="mp_player_int_rock"
										end

										if data3.current.value == 'menuperso_actions_Festives_bourresurplace' then
											animsAction({ lib = "amb@world_human_bum_standing@drunk@idle_a", anim = "idle_a" })
											currenttype="anim"
											currentlib="amb@world_human_bum_standing@drunk@idle_a"
											currentanim="idle_a"
										end

										if data3.current.value == 'menuperso_actions_Festives_vomirenvoiture' then
											animsAction({ lib = "oddjobs@taxi@tie", anim = "vomit_outside" })
											currenttype="anim"
											currentlib="oddjobs@taxi@tie"
											currentanim="vomit_outside"
										end

									end,
									function(data3, menu3)
										menu3.close()
									end
								)
							end

							if data2.current.value == 'menuperso_actions_Sportives' then
								ESX.UI.Menu.Open(
									'default', GetCurrentResourceName(), 'menuperso_actions_Sportives',
									{
										css = 'anim',
										title    = 'Animations Sportives',
										align    = 'top-left',
										elements = {
											{label = 'Montrer ses muscles',     value = 'menuperso_actions_Sportives_fairedesetirements'},
											{label = 'Faire des pompes',     value = 'menuperso_actions_Sportives_fairedespompes'},
											{label = 'Faire des abdos',     value = 'menuperso_actions_Sportives_fairedesabdos'},
											{label = 'Lever des poids',     value = 'menuperso_actions_Sportives_leverdespoids'},
											{label = 'Faire du jogging',     value = 'menuperso_actions_Sportives_fairedujogging'},
											{label = 'Faire du Yoga',     value = 'menuperso_actions_Sportives_faireduyoga'},
										},
									},
									function(data3, menu3)
										ESX.ShowNotification("Ne pas oublier de revenir en arrière pour sauvegardée l'animation !")
										if data3.current.value == 'menuperso_actions_Sportives_fairedesetirements' then
											animsActionScenario({ anim = "WORLD_HUMAN_MUSCLE_FLEX" })
											currenttype="scenario"
											currentlib=nil
											currentanim="WORLD_HUMAN_MUSCLE_FLEX"
										end

										if data3.current.value == 'menuperso_actions_Sportives_fairedespompes' then
											animsActionScenario({ anim = "WORLD_HUMAN_PUSH_UPS" })
											currenttype="scenario"
											currentlib=nil
											currentanim="WORLD_HUMAN_PUSH_UPS"
										end

										if data3.current.value == 'menuperso_actions_Sportives_fairedesabdos' then
											animsActionScenario({anim = "WORLD_HUMAN_SIT_UPS" })
											currenttype="scenario"
											currentlib=nil
											currentanim="WORLD_HUMAN_SIT_UPS"
										end

										if data3.current.value == 'menuperso_actions_Sportives_leverdespoids' then
											animsActionScenario({anim = "WORLD_HUMAN_MUSCLE_FREE_WEIGHTS" })
											currenttype="scenario"
											currentlib=nil
											currentanim="WORLD_HUMAN_MUSCLE_FREE_WEIGHTS"
										end

										if data3.current.value == 'menuperso_actions_Sportives_fairedujogging' then
											animsActionScenario({ anim = "WORLD_HUMAN_JOG_STANDING" })
											currenttype="scenario"
											currentlib=nil
											currentanim="WORLD_HUMAN_JOG_STANDING"
										end

										if data3.current.value == 'menuperso_actions_Sportives_faireduyoga' then
											animsActionScenario({ anim = "WORLD_HUMAN_YOGA" })
											currenttype="scenario"
											currentlib=nil
											currentanim="WORLD_HUMAN_YOGA"
										end

									end,
									function(data3, menu3)
										menu3.close()
									end
								)
							end

							if data2.current.value == 'menuperso_actions_Others' then
								ESX.UI.Menu.Open(
									'default', GetCurrentResourceName(), 'menuperso_actions_Others',
									{
										css = 'anim',
										title    = 'Animations Diverses',
										align    = 'top-left',
										elements = {
											--{label = 'Fumer une clope',     value = 'menuperso_actions_Others_fumeruneclope'},
											{label = 'S\'asseoir',     value = 'menuperso_actions_Others_sasseoir'},
											{label = 'S\'asseoir (par terre)',     value = 'menuperso_actions_Others_sasseoirparterre'},
											{label = 'S\'allonger (sur le ventre)',     value = 'menuperso_actions_Others_sallongersurleventre'},
											{label = 'S\'allonger (sur le dos)',     value = 'menuperso_actions_Others_sallongersurledos'},
											{label = 'S\'appuyer',     value = 'menuperso_actions_Others_attendre'},
											{label = 'Prendre un selfie',     value = 'menuperso_actions_Others_prendreunselfie'},
											{label = 'Prendre une photo',     value = 'menuperso_actions_Others_prendreunephoto'},
											--{label = 'Regarder avec des jumelles',     value = 'menuperso_actions_Others_regarderauxjumelles'},
											{label = 'Faire la statue',     value = 'menuperso_actions_Others_fairelastatut'},
											{label = 'Position de fouille',     value = 'menuperso_actions_Others_positiondefouille'},
											{label = 'Se gratter les parties',     value = 'menuperso_actions_Others_segratterlesc'},

											--{label = 'Boire un café',  value = 'menuperso_actions_Others_boireuncafe'},
											{label = 'Attendre',     value = 'menuperso_actions_Others_attendre'},
											{label = 'Nettoyer quelque chose',     value = 'menuperso_actions_Others_nettoyerquelquechose'},
											{label = 'Prendre un selfie',     value = 'menuperso_actions_Others_prendreunselfie'},
											{label = 'Faire la manche',     value = 'menuperso_actions_Others_manche'},
											{label = 'Attendre contre un mur',  value = 'menuperso_actions_Others_unmur'},
											{label = 'Préparer à manger',  value = 'menuperso_actions_Others_mangerrrrrrr'},
											{label = 'Ecouter à une porte',  value = 'menuperso_actions_Others_uneporte'},
										},
									},
									function(data3, menu3)
										ESX.ShowNotification("Ne pas oublier de revenir en arrière pour sauvegardée l'animation !")
										if data3.current.value == 'menuperso_actions_Others_boireuncafe' then
											animsAction({ lib = "amb@world_human_aa_coffee@idle_a", anim = "idle_a" })
											currenttype="anim"
											currentlib="amb@world_human_aa_coffee@idle_a"
											currentanim="idle_a"
										end

										if data3.current.value == 'menuperso_actions_Others_uneporte' then
											animsAction({ lib = "mini@safe_cracking", anim = "idle_base" })
											currenttype="anim"
											currentlib="mini@safe_cracking"
											currentanim="idle_base"
										end

										if data3.current.value == 'menuperso_actions_Others_fumeruneclope' then
											animsActionScenario({ anim = "WORLD_HUMAN_SMOKING" })
											currenttype="scenario"
											currentlib=nil
											currentanim="WORLD_HUMAN_SMOKING"
										end

										if data3.current.value == 'menuperso_actions_Others_sasseoir' then
											animsAction({ lib = "anim@heists@prison_heistunfinished_biztarget_idle", anim = "target_idle" })
											currenttype="anim"
											currentlib="anim@heists@prison_heistunfinished_biztarget_idle"
											currentanim="target_idle"
										end

										if data3.current.value == 'menuperso_actions_Others_sasseoirparterre' then
											animsActionScenario({ anim = "WORLD_HUMAN_PICNIC" })
											currenttype="scenario"
											currentlib=nil
											currentanim="WORLD_HUMAN_PICNIC"
										end

										if data3.current.value == 'menuperso_actions_Others_sallongersurleventre' then
											animsActionScenario({ anim = "WORLD_HUMAN_SUNBATHE" })
											currenttype="scenario"
											currentlib=nil
											currentanim="WORLD_HUMAN_SUNBATHE"
										end

										if data3.current.value == 'menuperso_actions_Others_sallongersurledos' then
											animsActionScenario({ anim = "WORLD_HUMAN_SUNBATHE_BACK" })
											currenttype="scenario"
											currentlib=nil
											currentanim="WORLD_HUMAN_SUNBATHE_BACK"
										end

										if data3.current.value == 'menuperso_actions_Others_attendre' then
											animsActionScenario({ anim = "world_human_leaning" })
											currenttype="scenario"
											currentlib=nil
											currentanim="world_human_leaning"
										end

										if data3.current.value == 'menuperso_actions_Others_prendreunselfie' then
											animsActionScenario({ anim = "world_human_tourist_mobile" })
											currenttype="scenario"
											currentlib=nil
											currentanim="world_human_tourist_mobile"
										end

										if data3.current.value == 'menuperso_actions_Others_prendreunephoto' then
											animsActionScenario({ anim = "WORLD_HUMAN_MOBILE_FILM_SHOCKING" })
											currenttype="scenario"
											currentlib=nil
											currentanim="WORLD_HUMAN_MOBILE_FILM_SHOCKING"
										end

										if data3.current.value == 'menuperso_actions_Others_regarderauxjumelles' then
											animsActionScenario({ anim = "WORLD_HUMAN_BINOCULARS" })
											currenttype="scenario"
											currentlib=nil
											currentanim="WORLD_HUMAN_BINOCULARS"
										end

										if data3.current.value == 'menuperso_actions_Others_fairelastatut' then
											animsActionScenario({ anim = "WORLD_HUMAN_HUMAN_STATUE" })
											currenttype="scenario"
											currentlib=nil
											currentanim="WORLD_HUMAN_HUMAN_STATUE"
										end

										if data3.current.value == 'menuperso_actions_Others_positiondefouille' then
											animsAction({ lib = "mini@prostitutes@sexlow_veh", anim = "low_car_bj_to_prop_female" })
											currenttype="anim"
											currentlib="mini@prostitutes@sexlow_veh"
											currentanim="low_car_bj_to_prop_female"
										end

										if data3.current.value == 'menuperso_actions_Others_segratterlesc' then
											animsAction({ lib = "mp_player_int_uppergrab_crotch", anim = "mp_player_int_grab_crotch" })
											currenttype="anim"
											currentlib="mp_player_int_uppergrab_crotch"
											currentanim="mp_player_int_grab_crotch"
										end

										if data3.current.value == 'menuperso_actions_Others_attendre' then
											animsActionScenario({ anim = "world_human_leaning" })
											currenttype="scenario"
											currentlib=nil
											currentanim="world_human_leaning"
										end

										if data3.current.value == 'menuperso_actions_Others_nettoyerquelquechose' then
											animsActionScenario({ anim = "world_human_maid_clean" })
											currenttype="scenario"
											currentlib=nil
											currentanim="world_human_maid_clean"
										end

										if data3.current.value == 'menuperso_actions_Others_prendreunselfie' then
											animsActionScenario({ anim = "world_human_tourist_mobile" })
											currenttype="scenario"
											currentlib=nil
											currentanim="world_human_tourist_mobile"
										end

										if data3.current.value == 'menuperso_actions_Others_manche' then
											animsActionScenario({ anim = "WORLD_HUMAN_BUM_FREEWAY" })
											currenttype="scenario"
											currentlib=nil
											currentanim="WORLD_HUMAN_BUM_FREEWAY"
										end

										if data3.current.value == 'menuperso_actions_Others_unmur' then
											animsActionScenario({ anim = "world_human_leaning" })
											currenttype="scenario"
											currentlib=nil
											currentanim="world_human_leaning"
										end

										if data3.current.value == 'menuperso_actions_Others_mangerrrrrrr' then
											animsActionScenario({ anim = "PROP_HUMAN_BBQ" })
											currenttype="scenario"
											currentlib=nil
											currentanim="PROP_HUMAN_BBQ"
										end

									end,
									function(data3, menu3)
										menu3.close()
									end
								)
							end
						end,
						function(data2, menu2)
							menu2.close()
						end
					)
end

local playAnim = false
local dataAnim = {}

function animsAction(animObj)
	if IsPedSittingInAnyVehicle(GetPlayerPed(-1)) then
		local source = GetPlayerServerId();
		Notify("Sortez de votre véhicule pour faire cela !")
    else
		Citizen.CreateThread(function()
			if not playAnim then
				local playerPed = GetPlayerPed(-1);
				if DoesEntityExist(playerPed) then -- Ckeck if ped exist
					dataAnim = animObj

					-- Play Animation
					RequestAnimDict(dataAnim.lib)
					while not HasAnimDictLoaded(dataAnim.lib) do
						Citizen.Wait(0)
					end
					if HasAnimDictLoaded(dataAnim.lib) then
						local flag = 0
						if dataAnim.loop ~= nil and dataAnim.loop then
							flag = 1
						elseif dataAnim.move ~= nil and dataAnim.move then
							flag = 49
						end

						TaskPlayAnim(playerPed, dataAnim.lib, dataAnim.anim, 8.0, -8.0, -1, flag, 0, 0, 0, 0)
						playAnimation = true
					end

					-- Wait end annimation
					while true do
						Citizen.Wait(0)
						if not IsEntityPlayingAnim(playerPed, dataAnim.lib, dataAnim.anim, 3) then
							playAnim = false
							TriggerEvent('ft_animation:ClFinish')
							break
						end
					end
				end -- end ped exist
			end
		end)
	end
end

function playFreeAnim(animObj)
	if IsPedSittingInAnyVehicle(GetPlayerPed(-1)) then
		local source = GetPlayerServerId();
		Notify("Sortez de votre véhicule pour faire cela !")
	else
		dict = animObj.lib
		loadAnimDict(dict)
		TaskPlayAnim(GetPlayerPed(-1), dict, animObj.anim, 8.0, -8, -1, 49, 0, 0, 0, 0)
	end
end


function animsActionScenario(animObj)
	if IsPedSittingInAnyVehicle(GetPlayerPed(-1)) then
		local source = GetPlayerServerId();
		Notify("Sortez de votre véhicule pour faire cela !")
	else
		Citizen.CreateThread(function()
			if not playAnim then
				local playerPed = GetPlayerPed(-1);
				if DoesEntityExist(playerPed) then
					dataAnim = animObj
					TaskStartScenarioInPlace(playerPed, dataAnim.anim, 0, false)
					playAnimation = true
				end
			end
		end)
	end
end

AddEventHandler("playerSpawned", function()
    Citizen.Wait(1000)
    TriggerServerEvent("animations:loadAnim",117,PlayerData.identifier)
    TriggerServerEvent("animations:loadAnim",118,PlayerData.identifier)
end)

RegisterNetEvent("animations:setAnim")
AddEventHandler("animations:setAnim", function(key, type, lib, anim)
	if key == 117 then
		numpad7type=type
		numpad7lib=lib
		numpad7anim=anim
	elseif key == 118 then
		numpad9type=type
		numpad9lib=lib
		numpad9anim=anim
	end
end)

RegisterNetEvent("animations:openMenu")
AddEventHandler("animations:openMenu", function()
	openMenuKey()
end)
isAnim = false
isFreeAnim = false
Citizen.CreateThread(function()
    while true do
        Citizen.Wait(0)
        if IsControlJustPressed(0, 117) then -- NUMPAD 7
            if IsPedSittingInAnyVehicle(GetPlayerPed(-1)) then
            	ESX.ShowNotification("Vous ne pouvez pas faire ça en voiture !")
            else
            	if numpad7anim ~= nil then
            		if isAnim == false then
            			if numpad7type == "anim" then
            				animsAction({ lib = numpad7lib, anim = numpad7anim })
            			elseif numpad7type == "scenario" then
            				animsActionScenario({ anim = numpad7anim })
            			elseif numpad7type == "freeanim" then
            				isFreeAnim = true
            				playFreeAnim({lib = numpad7lib, anim = numpad7anim})
            			end
            			isAnim = true
            		else
            			ClearPedTasks(GetPlayerPed(-1))
            			isAnim = false
            			isFreeAnim = false
            		end
            	else
            		ESX.ShowNotification("Aucune animation sur cette touche !")
            	end
            end
        end
        if IsControlJustPressed(0, 118) then -- NUMPAD 9
            if IsPedSittingInAnyVehicle(GetPlayerPed(-1)) then
            	ESX.ShowNotification("Vous ne pouvez pas faire ça en voiture !")
            else
            	if numpad9anim ~= nil then
            		if isAnim == false then
            			if numpad9type == "anim" then
            				animsAction({ lib = numpad9lib, anim = numpad9anim })
            			elseif numpad9type == "scenario" then
            				animsActionScenario({ anim = numpad9anim })
            			elseif numpad9type == "freeanim" then
            				playFreeAnim({lib = numpad9lib, anim = numpad9anim})
            				isFreeAnim = true
            			end
            			isAnim = true
            		else
            			ClearPedTasks(GetPlayerPed(-1))
            			isAnim = false
            			isFreeAnim = false
            		end
            	else
            		ESX.ShowNotification("Aucune animation sur cette touche !")
            	end
            end
        end
    end
end)

Citizen.CreateThread(function()
    while true do
        Citizen.Wait(2000)
        if IsPedSittingInAnyVehicle(GetPlayerPed(-1)) then
        	if isFreeAnim then
        		ClearPedTasks(GetPlayerPed(-1))
        		isFreeAnim = false
        	end
        end
    end
end)

Citizen.CreateThread(function()
    while true do
        Citizen.Wait(0)
        if IsControlJustPressed(0, 83) then -- =
            ClearPedTasks(GetPlayerPed(-1))
            ESX.ShowNotification("~y~Fin de l'animation")
        end
    end
end)
