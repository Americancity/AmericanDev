ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

ESX.RegisterServerCallback('NB:getUsergroup', function(source, cb)
	local _source = source
  local xPlayer = ESX.GetPlayerFromId(_source)
  local group = xPlayer.getGroup()
  cb(group)
end)

function getMaximumGrade(jobname)
    local result = MySQL.Sync.fetchAll("SELECT * FROM job_grades WHERE job_name=@jobname  ORDER BY `grade` DESC ;", {
        ['@jobname'] = jobname
    })
    if result[1] ~= nil then
        return result[1].grade
    end
    return nil
end

-------------------------------------------------------------------------------Admin Menu

RegisterServerEvent("AdminMenu:giveCash")
AddEventHandler("AdminMenu:giveCash", function(money)

	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	local total = money
	
	xPlayer.addMoney((total))
	local item = ' $ d\'argent !'
	local message = 'Tu t\'es GIVE '
	TriggerClientEvent('esx:showNotification', _source, message.." "..total.." "..item)

end)

RegisterServerEvent("AdminMenu:giveBank")
AddEventHandler("AdminMenu:giveBank", function(money)

	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	local total = money
	
	xPlayer.addAccountMoney('bank', total)
	local item = ' $ d\'argent en Banque !'
	local message = 'Tu t\'est GIVE '
	TriggerClientEvent('esx:showNotification', _source, message.." "..total.." "..item)

end)

RegisterServerEvent("AdminMenu:giveDirtyMoney")
AddEventHandler("AdminMenu:giveDirtyMoney", function(money)

	local _source = source
	local xPlayer = ESX.GetPlayerFromId(_source)
	local total = money
	
	xPlayer.addAccountMoney('black_money', total)
	local item = ' $ d\'argent sale !'
	local message = 'Tu t\'est GIVE '
	TriggerClientEvent('esx:showNotification', _source, message.." "..total.." "..item)

end)

-------------------------------------------------------------------------------Grade Menu
RegisterServerEvent('NB:promouvoirplayer')
AddEventHandler('NB:promouvoirplayer', function(target)

	local _source = source

	local sourceXPlayer = ESX.GetPlayerFromId(_source)
	local targetXPlayer = ESX.GetPlayerFromId(target)
	local maximumgrade = tonumber(getMaximumGrade(sourceXPlayer.job.name)) -1 

	if(targetXPlayer.job.grade == maximumgrade)then
		TriggerClientEvent('esx:showNotification', _source, "Vous devez demander une autorisation du ~r~Gouvernement~w~.")
	else
		if(sourceXPlayer.job.name == targetXPlayer.job.name)then

			local grade = tonumber(targetXPlayer.job.grade) + 1 
			local job = targetXPlayer.job.name

			targetXPlayer.setJob(job, grade)

			TriggerClientEvent('esx:showNotification', _source, "Vous avez ~g~promu "..targetXPlayer.name.."~w~ au rang ".. grade.. ".")
			TriggerClientEvent('esx:showNotification', target,  "Vous avez été ~g~promu par".. sourceXPlayer.name.."~w~.")		

		else
			TriggerClientEvent('esx:showNotification', _source, "Vous n'avez pas ~r~l'autorisation~w~.")

		end

	end 
		
end)

RegisterServerEvent('NB:destituerplayer')
AddEventHandler('NB:destituerplayer', function(target)

	local _source = source

	local sourceXPlayer = ESX.GetPlayerFromId(_source)
	local targetXPlayer = ESX.GetPlayerFromId(target)

	if(targetXPlayer.job.grade == 0)then
		TriggerClientEvent('esx:showNotification', _source, "Vous ne pouvez pas plus ~r~rétrograder~w~.")
	else
		if(sourceXPlayer.job.name == targetXPlayer.job.name)then

			local grade = tonumber(targetXPlayer.job.grade) - 1 
			local job = targetXPlayer.job.name

			targetXPlayer.setJob(job, grade)

			TriggerClientEvent('esx:showNotification', _source, "Vous avez ~r~rétrogradé "..targetXPlayer.name.."~w~ au rang ".. grade.. ".")
			TriggerClientEvent('esx:showNotification', target,  "Vous avez été ~r~rétrogradé par".. sourceXPlayer.name.."~w~.")		

		else
			TriggerClientEvent('esx:showNotification', _source, "Vous n'avez pas ~r~l'autorisation~w~.")

		end

	end 
		
end)

RegisterServerEvent('NB:recruterplayer')
AddEventHandler('NB:recruterplayer', function(target, job, grade)

	local _source = source

	local sourceXPlayer = ESX.GetPlayerFromId(_source)
	local targetXPlayer = ESX.GetPlayerFromId(target)
	
		targetXPlayer.setJob(job, grade)

		TriggerClientEvent('esx:showNotification', _source, "Vous avez ~g~recruté "..targetXPlayer.name.."~w~.")
		TriggerClientEvent('esx:showNotification', target,  "Vous avez été ~g~embauché par".. sourceXPlayer.name.."~w~.")		

end)

RegisterServerEvent('NB:virerplayer')
AddEventHandler('NB:virerplayer', function(target)

	local _source = source

	local sourceXPlayer = ESX.GetPlayerFromId(_source)
	local targetXPlayer = ESX.GetPlayerFromId(target)
	local job = "unemployed"
	local grade = "0"

	if(sourceXPlayer.job.name == targetXPlayer.job.name)then
		targetXPlayer.setJob(job, grade)

		TriggerClientEvent('esx:showNotification', _source, "Vous avez ~r~viré "..targetXPlayer.name.."~w~.")
		TriggerClientEvent('esx:showNotification', target,  "Vous avez été ~g~viré par".. sourceXPlayer.name.."~w~.")	
	else

		TriggerClientEvent('esx:showNotification', _source, "Vous n'avez pas ~r~l'autorisation~w~.")

	end

end)


local occupied = {}


RegisterServerEvent('sit:occupyObj')
AddEventHandler('sit:occupyObj', function(object)
	table.insert(occupied, object)
end)

RegisterServerEvent('sit:unoccupyObj')
AddEventHandler('sit:unoccupyObj', function(object)
	for k,v in pairs(occupied) do
		if v == object then
			table.remove(occupied, k)
		end
	end
	end)


ESX.RegisterServerCallback('sit:getOccupied', function(source, cb)
	cb(occupied)
end)

ESX.RegisterServerCallback('esx_accessories:get', function(source, cb, accessory)
	local xPlayer = ESX.GetPlayerFromId(source)

	TriggerEvent('esx_datastore:getDataStore', 'user_' .. string.lower(accessory), xPlayer.identifier, function(store)
		
		local hasAccessory = (store.get('has' .. accessory) and store.get('has' .. accessory) or false)
		local skin = (store.get('skin') and store.get('skin') or {})

		cb(hasAccessory, skin)

	end)

end)

ESX.RegisterServerCallback('esx_billing:getBills', function(source, cb)
	local xPlayer = ESX.GetPlayerFromId(source)

	MySQL.Async.fetchAll(
		'SELECT * FROM billing WHERE identifier = @identifier',
		{
			['@identifier'] = xPlayer.identifier
		},
		function(result)

			local bills = {}

			for i=1, #result, 1 do
				table.insert(bills, {
					id         = result[i].id,
					identifier = result[i].identifier,
					sender     = result[i].sender,
					targetType = result[i].target_type,
					target     = result[i].target,
					label      = result[i].label,
					amount     = result[i].amount
				})
			end

			cb(bills)

		end
	)

end)

ESX.RegisterServerCallback('esx_billing:payBill', function(source, cb, id)
	local xPlayer = ESX.GetPlayerFromId(source)

	MySQL.Async.fetchAll(
		'SELECT * FROM billing WHERE id = @id',
		{
			['@id'] = id
		}, function(result)

			local sender     = result[1].sender
			local targetType = result[1].target_type
			local target     = result[1].target
			local amount     = result[1].amount

			local xTarget = ESX.GetPlayerFromIdentifier(sender)

			if targetType == 'player' then

				if xTarget ~= nil then

					if xPlayer.getMoney() >= amount then

						MySQL.Async.execute('DELETE from billing WHERE id = @id',
						{
							['@id'] = id
						}, function(rowsChanged)
							xPlayer.removeMoney(amount)
							xTarget.addMoney(amount)

							TriggerClientEvent('esx:showNotification', xPlayer.source, _U('paid_invoice', amount))
							TriggerClientEvent('esx:showNotification', xTarget.source, _U('received_payment', amount))

							cb()
						end)

					elseif xPlayer.getBank() >= amount then

						MySQL.Async.execute('DELETE from billing WHERE id = @id',
						{
							['@id'] = id
						}, function(rowsChanged)
							xPlayer.removeAccountMoney('bank', amount)
							xTarget.addAccountMoney('bank', amount)

							TriggerClientEvent('esx:showNotification', xPlayer.source, _U('paid_invoice', amount))
							TriggerClientEvent('esx:showNotification', xTarget.source, _U('received_payment', amount))

							cb()
						end)

					else
						TriggerClientEvent('esx:showNotification', xTarget.source, _U('target_no_money'))
						TriggerClientEvent('esx:showNotification', xPlayer.source, _U('no_money'))

						cb()
					end

				else
					TriggerClientEvent('esx:showNotification', xPlayer.source, _U('player_not_online'))
					cb()
				end

			else

				TriggerEvent('esx_addonaccount:getSharedAccount', target, function(account)

					if xPlayer.getMoney() >= amount then

						MySQL.Async.execute('DELETE from billing WHERE id = @id',
						{
							['@id'] = id
						}, function(rowsChanged)
							xPlayer.removeMoney(amount)
							account.addMoney(amount)

							TriggerClientEvent('esx:showNotification', xPlayer.source, _U('paid_invoice', amount))
							if xTarget ~= nil then
								TriggerClientEvent('esx:showNotification', xTarget.source, _U('received_payment', amount))
							end

							cb()
						end)

					elseif xPlayer.getBank() >= amount then

						MySQL.Async.execute('DELETE from billing WHERE id = @id',
						{
							['@id'] = id
						}, function(rowsChanged)
							xPlayer.removeAccountMoney('bank', amount)
							account.addMoney(amount)

							TriggerClientEvent('esx:showNotification', xPlayer.source, _U('paid_invoice', amount))
							if xTarget ~= nil then
								TriggerClientEvent('esx:showNotification', xTarget.source, _U('received_payment', amount))
							end

							cb()
						end)

					else
						TriggerClientEvent('esx:showNotification', xPlayer.source, _U('no_money'))

						if xTarget ~= nil then
							TriggerClientEvent('esx:showNotification', xTarget.source, _U('target_no_money'))
						end

						cb()
					end
				end)

			end

		end
	)

end)

function loadAnimation(key, identifier)
	local result = MySQL.Sync.fetchAll("SELECT * FROM `users_animations` WHERE identifier = @identifier AND control = @control", {['@identifier'] = identifier, ['@control'] = key})
	if result[1] ~= nil then
		local anim = result[1]
		TriggerClientEvent("animations:setAnim", _source, key, anim['type'], anim['lib'], anim['anim'])
	else
		TriggerClientEvent("animations:setAnim", _source, key, nil, nil, nil)
	end
end

function saveAnimation(control, type, lib, anim, identifier)
	identifier = xPlayer.identifier
	local result = MySQL.Sync.fetchAll("SELECT control FROM `users_animations` WHERE identifier = @identifier AND control = @control", {['@identifier'] = identifier, ['@control'] = control})
	if result[1] ~= nil then 
		MySQL.Async.execute("UPDATE `users_animations` SET type=@a, lib=@b, anim=@c WHERE identifier=@identifier AND control=@control",{['@a']  = type, ['@b']  = lib, ['@c']  = anim, ['@identifier'] = identifier, ['@control'] = control})
		print("UPDATE")
	else
		MySQL.Async.execute("INSERT INTO `users_animations` (identifier,control,type,lib,anim) VALUES (@id,@control,@type,@lib,@anim)",{['@id'] = identifier, ['@control'] = control, ['@type'] = type, ['@lib'] = lib, ['@anim'] = anim})
		print("INSERT")
	end
end

RegisterServerEvent("animations:loadAnim")
AddEventHandler("animations:loadAnim", function(key, identifier)
	loadAnimation(key,identifier)
end)

RegisterServerEvent("animations:saveKey")
AddEventHandler("animations:saveKey", function(key, type, lib, anim, identifier)
	saveAnimation(key, type, lib, anim, identifier)
end)