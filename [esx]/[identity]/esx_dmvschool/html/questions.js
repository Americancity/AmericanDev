var tableauQuestion = [
	{
		question: "Si vous conduisez à 80km/h, que vous approchez d'un lieu de résidence, cela veut dire que :",
		propositionA: "Vous devez accélérer",
		propositionB: "Vous pouvez garder votre vitesse, si vous ne croisez pas d'autres véhicules",
		propositionC: "Vous devez ralentir",
		propositionD: "Vous pouvez garder votre vitesse",
		reponse: "C"
	},

	{
		question: "Vous vous apprétez à tourner à droite au feu vert, mais vous voyez un piéton qui traverse :",
		propositionA: "Vous passez avant le piéton",
		propositionB: "Vous vérifiez qu'il n'y a pas d'autre véhicule et vous passez",
		propositionC: "Vous attendez que le piéton est terminé",
		propositionD: "vous klaxonnez",
		reponse: "C"
	},

	{
		question: "Sans aucune indication : La vitesse dans une zone résidentielle est de : __ km/h",
		propositionA: "60",
		propositionB: "70",
		propositionC: "90",
		propositionD: "100",
		reponse: "B"
	},

	{
		question: "Si je possède une classe super car et que je fait un gros exces de vitesse, je risque :",
		propositionA: "Une amende",
		propositionB: "La saisie de mon véhicule",
		propositionC: "Un séjour en prison fédéral",
		propositionD: "Tout cela",
		reponse: "D"
	},

	{
		question: "Avant chaque changement de file vous devez :",
		propositionA: "Vérifiez vos rétroviseurs",
		propositionB: "Vérifiez vos angles morts",
		propositionC: "Signalez vos intentions",
		propositionD: "Tout cela",
		reponse: "D"
	},

	{
		question: "A quel moment vous pouvez passer aux feux ?",
		propositionA: "Quand il est vert",
		propositionB: "Quand il n'y a personne sur l'intersection",
		propositionC: "Vous êtes dans une zone d'école",
		propositionD: "Quand il est vert et/ou rouge et que je tourne à droite",
		reponse: "D"
	},

	{
		question: "Un piéton est au feu rouge pour les piétons",
		propositionA: "Vous le laissez passer",
		propositionB: "Vous observez avant de continuer",
		propositionC: "Vous lui faite un signe de la main",
		propositionD: "Vous continuez votre chemin car votre feu est vert",
		reponse: "D"
	},

	{
		question: "Qu'est ce qui est permit quand vous dépassez un autre véhicule",
		propositionA: "Le suivre de près pour le doubler plus vite",
		propositionB: "Le doubler en quittant la route",
		propositionC: "Conduire sur la route opposé pour le dépasser",
		propositionD: "Dépasser la vitesse limite",
		reponse: "C"
	},

	{
		question: "Vous conduisez sur une rocade qui indique une vitesse maximum de 140 km/h. La plupart du traffic roule à 150 km/h, alors vous ne devriez pas conduire plus vite que :",
		propositionA: "80 kmh",
		propositionB: "40 kmh",
		propositionC: "162 kmh",
		propositionD: "140 kmh",
		reponse: "D"
	},

	{
		question: "Quand vous êtes dépassé par un autre véhicule il est important de ne PAS :",
		propositionA: "Ralentir",
		propositionB: "Vérifiez vos rétroviseurs",
		propositionC: "Regarder les autres conducteurs",
		propositionD: "Augmenter votre vitesse",
		reponse: "D"
	},
]
