Config              = {}
Config.DrawDistance = 100.0
Config.MaxDelivery	= 10
Config.TruckPrice	= 1500
Config.Locale       = 'fr'

Config.Trucks = {
	"mule3",
	"phantom3"
}

Config.Cloakroom = {
	CloakRoom = {
			Pos   = {x = 146.0555, y = -3219.3471, z = 4.5},
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1
		},
}

Config.Zones = {
	VehicleSpawner = {
			Pos   = {x = 152.3740, y = -3212.1042, z = 4.5},
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1
		},

	VehicleSpawnPoint = {
			Pos   = {x = 145.7359, y = -3204.5605, z = 5.0},
			Size  = {x = 3.0, y = 3.0, z = 1.0},
			Type  = -1
		},
}

Config.Livraison = {
-------------------------------------------Los Santos
	-- Strawberry avenue et Davis avenue M
	Delivery1LS = {
			Pos   = {x = 155.57, y = -1511.24, z = 28.14},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 125
		},
	-- a cot� des flic M
	Delivery2LS = {
			Pos   = {x = 382.16, y = -754.20, z = 28.29},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 150
		},
	-- vers la plage M
	Delivery3LS = {
			Pos   = {x = -1149.19, y = -1584.45, z = 3.38},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 200
		},
	-- studio 1 M
	Delivery4LS = {
			Pos   = {x = -1038.73, y = -499.33, z = 35.24},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 225
		},
	-- popular street et el rancho boulevard M
	Delivery5LS = {
			Pos   = {x = 837.44, y = -1934.57, z = 28.04},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 300
		},
	--Alta street et las lagunas boulevard M
	Delivery6LS = {
			Pos   = {x = 21.46, y = -219.25, z = 50.81},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 125
		},
	--Rockford Drive Noth et boulevard del perro M
	Delivery7LS = {
			Pos   = {x = -1338.83, y = -394.62, z = 35.6},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 150
		},
	--Rockford Drive Noth et boulevard del perro M
	Delivery8LS = {
			Pos   = {x = 548.6097, y = -208.3496, z = 52.58},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 200
		},
	--New empire way (airport) M
	Delivery9LS = {
			Pos   = {x = -1137.77, y = -2692.26, z = 13.02},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 225
		},
	--Rockford drive south M
	Delivery10LS = {
			Pos   = {x = -632.16, y = -1187.88, z = 13.38},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 300
		},
------------------------------------------- Blaine County
	-- panorama drive M
	Delivery1BC = {
			Pos   = {x = 1504.37, y = 3071.24, z = 46.6},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 225
		},
	-- route 68 M
	Delivery2BC = {
			Pos   = {x = 1209.81, y = 2712.98, z = 37.07},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 300
		},
	-- Algonquin boulevard et cholla springs avenue M
	Delivery3BC = {
			Pos   = {x = 1715.80, y = 3757.04, z = 33.06},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 350
		},
	-- Joshua road M
	Delivery4BC = {
			Pos   = {x = 191.11, y = 2792.65, z = 44.70},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 400
		},
	-- East joshua road M
	Delivery5BC = {
			Pos   = {x = 2712.18, y = 4357.11, z = 45.5},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 450
		},
	-- Seaview road M
	Delivery6BC = {
			Pos   = {x = 1709.27, y = 4804.74, z = 40.8},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 225
		},
	-- Paleto boulevard M
	Delivery7BC = {
			Pos   = {x = 19.9, y = 6279.87, z = 30.25},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 300
		},
	-- Paleto boulevard et Procopio drive M
	Delivery8BC = {
			Pos   = {x = 101.22, y = 6635.7, z = 30.50},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 350
		},
	-- Marina drive et joshua road M
	Delivery9BC = {
			Pos   = {x = 909.18, y = 3589.68, z = 32.25},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 400
		},
	-- Pyrite Avenue M
	Delivery10BC = {
			Pos   = {x = -313.06, y = 6286.99, z = 30.5},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 450
		},

	RetourCamion = {
			Pos   = {x = 162.5408, y = -3188.7770, z = 4.5},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 0
		},

	AnnulerMission = {
			Pos   = {x = 162.0788, y = -3210.7067, z = 4.5},
			Color = {r = 204, g = 204, b = 0},
			Size  = {x = 5.0, y = 5.0, z = 3.0},
			Color = {r = 204, g = 204, b = 0},
			Type  = 1,
			Paye = 0
		},
}
