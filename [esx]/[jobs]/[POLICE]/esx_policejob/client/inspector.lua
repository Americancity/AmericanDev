local Keys = {
  ["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
  ["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
  ["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
  ["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
  ["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
  ["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
  ["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
  ["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
  ["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}
local PlayerData              = {}
local isDead                  = false
local currentAction = nil

ESX                           = nil

_menuPool = NativeUI.CreatePool()
vestiairemenu = NativeUI.CreateMenu("LSPD", "~b~Vestiaire enquêteur")
_menuPool:Add(vestiairemenu)

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end

	while ESX.GetPlayerData().job == nil do
		Citizen.Wait(10)
	end

	PlayerData = ESX.GetPlayerData()
end)

function ShowNotification(text)
    SetNotificationTextEntry("STRING")
    AddTextComponentString(text)
    DrawNotification(false, false)
end

function cleanPlayer(playerPed)
	SetPedArmour(playerPed, 0)
	ClearPedBloodDamage(playerPed)
	ResetPedVisibleDamage(playerPed)
	ClearPedLastWeaponDamage(playerPed)
	ResetPedMovementClipset(playerPed, 0)
end

function AddVestiaire(menu)
	local playerPed = PlayerPedId()
    local pistol = NativeUI.CreateItem("Pistolet", "Prendre un holster.")
    menu:AddItem(pistol)
    local inter = NativeUI.CreateItem("Ceinturon d'intervention", "Prendre un ceinturon d'intervention.")
    menu:AddItem(inter)
    local badge = NativeUI.CreateItem("Badge", "Prendre un badge.")
    menu:AddItem(badge)
    local bullet = NativeUI.CreateItem("Gilet pare-balle", "Prendre un gilet pare-balle, modèle unique.")
    menu:AddItem(bullet)
    local civil = NativeUI.CreateItem("Tenue Civile", "Fin de service.")
    menu:AddItem(civil)
    menu.OnItemSelect = function(sender, item, index)
        if item == pistol then
            local male = {['sex'] = 0, ['tshirt_1'] = 130,  ['tshirt_2'] = 0}
            local female = {['sex'] = 1, ['tshirt_1'] = 160,  ['tshirt_2'] = 0}
			TriggerEvent('skinchanger:getSkin', function(skin)
				if skin.sex == 0 then
					TriggerEvent('skinchanger:loadSkin',male)
				else
					TriggerEvent('skinchanger:loadSkin',female)
				end
			end)
        end
        if item == inter then
            local male = {['sex'] = 0, ['tshirt_1'] = 122,  ['tshirt_2'] = 0}
            local female = {['sex'] = 1, ['tshirt_1'] = 152,  ['tshirt_2'] = 0}
			TriggerEvent('skinchanger:getSkin', function(skin)
				if skin.sex == 0 then
					TriggerEvent('skinchanger:loadSkin',male)
				else
					TriggerEvent('skinchanger:loadSkin',female)
				end
			end)
        end
        if item == badge then
            local male = {['sex'] = 0, ['chain_1'] = 128,  ['chain_2'] = 0}
            local female = {['sex'] = 1, ['chain_1'] = 98,  ['chain_2'] = 0}
			TriggerEvent('skinchanger:getSkin', function(skin)
				if skin.sex == 0 then
					TriggerEvent('skinchanger:loadSkin',male)
				else
					TriggerEvent('skinchanger:loadSkin',female)
				end
			end)
        end
        if item == bullet then
            local male = {['sex'] = 0, ['bproof_1'] = 11,  ['bproof_2'] = 1}
            local female = {['sex'] = 1, ['bproof_1'] = 13,  ['bproof_2'] = 1}
			TriggerEvent('skinchanger:getSkin', function(skin)
				if skin.sex == 0 then
					TriggerEvent('skinchanger:loadSkin',male)
				else
					TriggerEvent('skinchanger:loadSkin',female)
				end
			end)
        end
        if item == civil then
        	ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin)
				TriggerEvent('skinchanger:loadSkin', skin)
			end)
        end
    end
end

-- Display markers
Citizen.CreateThread(function()
  while true do

    Citizen.Wait(1)

    if PlayerData.job ~= nil and PlayerData.job.name == 'police' then
    	local playerPed = PlayerPedId()
    	local coords    = GetEntityCoords(playerPed)

    	if GetDistanceBetweenCoords(coords, 458.119, -993.046, 29.69, true) < 15 then
      		DrawMarker(0, 458.119, -993.046, 29.69, 0.0, 0.0, 0.0, 0, 0.0, 0.0, 1.5, 1.5, 0.5, 0, 24, 249, 28,  false, true, 2, false, false, false, false)
    	end
    end

  end
end)

local currentAction = nil
-- Enter / Exit marker events
Citizen.CreateThread(function()
  while true do

    Citizen.Wait(10)
    local playerPed = PlayerPedId()
    local coords = GetEntityCoords(playerPed)
    local isInMarker = false

    if PlayerData.job ~= nil and PlayerData.job.name == 'police' then
    	if GetDistanceBetweenCoords(coords, 458.119, -993.046, 29.69, true) < 1.5 then
      		isInMarker     = true
      		currentAction    = 'vestiaire'
      		ESX.ShowHelpNotification("Appuyez sur ~INPUT_CONTEXT~ pour ~y~ouvrir~s~ les vestiaires")
    	else
    		isInMarker     = false
    		currentAction = nil
    	end
    end

  end
end)


Citizen.CreateThread(function()
    while true do
        Citizen.Wait(0)
        if currentAction ~= nil and currentAction == 'vestiaire' then
        	local grade = PlayerData.job.grade_name
          	if grade == 'detective' or grade == 'lieutenant' or grade == 'boss' then
          		_menuPool:ProcessMenus()
          		if IsControlJustPressed(0, Keys['E']) then
          			vestiairemenu:Clear()
          			AddVestiaire(vestiairemenu)
					_menuPool:RefreshIndex()
            		vestiairemenu:Visible(not vestiairemenu:Visible())
          		end
          	else
          		ShowNotification("Vous n'avez pas les clés du vestiaire !")
          	end
        end
    end
end)