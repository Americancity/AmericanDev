Locales['fr'] = {
	-- Author : Feared
  	['press_e'] = 'Appuyer sur ~INPUT_CONTEXT~ pour savoir de quoi est ~r~morte~s~ la personne.',
	['hardmeele'] = 'Probablement un ~r~choc violent~s~ a la t�te.',
	['bullet'] = 'Probablement touch� par une ~r~balle~s~, il y a des impact dans le corps.',
	['knifes'] = 'Probablement touch� par un ~r~objet coupant~s~.',
	['bitten'] = 'Probablement attaqu� par un ~r~animal~s~.',
	['brokenlegs'] = 'Il est ~r~tomb� de haut~s~, il a probablement les jambes cass�.',
	['explosive'] = 'Il est presque completement d�figur� par des ~r~explosifs~s~.',
	['gas'] = 'Un ~r~gaz mortel~s~ a surement tu� cette personne.',
	['fireextinguisher'] = 'Surement a cause du ~r~gaz d"un exctincteur~s~.',
	['fire'] = 'Il est compl�tement ~r~brul�~s~.',
	['caraccident'] = 'Il est surement mort du au ~r~choc de l"accident~s~.',
	['drown'] = 'Il s"est ~r~noy�~s~.',
	['unknown'] = 'Impossible de ~r~savoir de quoi il est mort~s~.',
}
