Config                            = {}

Config.DrawDistance               = 100.0
Config.MarkerColor                = { r = 250, g = 250, b = 250 }
Config.MarkerSize                 = { x = 1.5, y = 1.5, z = 1.0 }
Config.MarkerSizeH                = { x = 3.0, y = 3.0, z = 2.0 }
Config.ReviveReward               = 150  -- revive reward, set to 0 if you don't want it enabled
Config.AntiCombatLog              = true -- enable anti-combat logging?
Config.LoadIpl                    = true -- disable if you're using fivem-ipl or other IPL loaders
Config.Locale                     = 'en'

local second = 1000
local minute = 60 * second

Config.EarlyRespawnTimer          = 15 * minute  -- Time til respawn is available
Config.BleedoutTimer              = 60 * minute -- Time til the player bleeds out

Config.EnablePlayerManagement     = true
Config.EnableSocietyOwnedVehicles = true

Config.RemoveWeaponsAfterRPDeath  = true
Config.RemoveCashAfterRPDeath     = false
Config.RemoveItemsAfterRPDeath    = true

-- Let the player pay for respawning early, if he can afford it.
Config.EarlyRespawnFine           = false
Config.EarlyRespawnFineAmount     = 300

Config.Blip = {
	Pos     = { x = 298.5694852539, y = -584.47808837891, z = 43.260845184326 },
	Sprite  = 61,
	Display = 4,
	Scale   = 1.2,
	Colour  = 38,
}

Config.HelicopterSpawner = {
	SpawnPoint = { x = 351.33, y = -588.5, z = 74.1 },
	Heading    = 0.0
}

-- https://wiki.fivem.net/wiki/Vehicles
Config.AuthorizedVehicles = {

	{
		model = 'ambulance',
		label = 'Ambulance'
	}

}

Config.AuthorizedHelico = {

	{
		model = 'polmav',
		label = 'Helico'
	}

}

Config.Zones = {

	HospitalInteriorEntering1 = { -- Main entrance
		Pos	= { x = 0, y = -0, z = 0 },
		Type = 1
	},

	HospitalInteriorInside1 = {
		Pos	= { x = 326.7742, y = -576.3787, z = 44.24},
		Type = -1
	},

	HospitalInteriorOutside1 = {
		Pos	= { x = 0, y = -0, z = 0 },
		Type = -1
	},

	HospitalInteriorExit1 = {
		Pos	= { x = 0, y = -0, z = 0 },
		Type = 1
	},

	HospitalInteriorEntering2 = { -- Lift go to the roof
	    Pos	= { x = 325.30, y = -598.77, z = 42.29 },
		Type = 1
	},

	HospitalInteriorInside2 = { -- Roof outlet
	    Pos	= { x = 344.02, y = -580.93, z = 74.16 },
		Type = -1
	},

	HospitalInteriorOutside2 = { -- Lift back from roof
	    Pos	= { x = 321.31,	y = -595.22, z = 43.29 },
		Type = -1
	},

	HospitalInteriorExit2 = { -- Roof entrance
		Pos	= { x = 338.57,	y = -583.82, z = 73.16 },
		Type = 1
	},

	AmbulanceActions = { -- Cloakroom
		Pos	= { x = 310.62, y = -599.97, z = 42.26 },
		Type = 0
	},

	BossActions = { -- Yo
		Pos	= { x = 230.973, y = -1368.97, z = 38.26 },
		Type = 0
	},

	VehicleSpawner = {
		Pos	= { x = 323.3, y = -557.65, z = 27.74 },
		Type = 1
	},

	VehicleSpawnPoint = {
		Pos	= { x = 328.22, y = -550.11, z = 27.74 },
		Type = -1
	},

	HelicoSpawner = {
		Pos	= { x = 339.3, y = -587.87, z = 73.1 },
		Type = 1
	},

	HelicoSpawnPoint = {
		Pos	= { x = 350.07, y = -588.4, z = 74.1 },
		Type = -1
	},

	HelicoDeleter = {
		Pos	= { x = 351.87, y = -587.954, z = 73.1 },
		Type = 1
	},

	VehicleDeleter = {
		Pos	= { x = 340.377, y = -562.15, z = 27.74 },
		Type = 1
	},

	Pharmacy = {
		Pos	= { x = 248.91, y = -1374.96, z = 38.53 },
		Type = 1
	},

	ParkingDoorGoOutInside = {
		Pos	= { x = 0, y = -0, z = 0 },
		Type = 1
	},

	ParkingDoorGoOutOutside = {
		Pos	= { x = 0, y = -0, z = 0 },
		Type = -1
	},

	ParkingDoorGoInInside = {
		Pos	= { x = 0, y = -0, z = 0.0 },
		Type = -1
	},

	ParkingDoorGoInOutside = {
		Pos	= { x = 0.0, y = -0.0, z = 0.0 },
		Type = 1
	},

	StairsGoTopTop = {
		Pos	= { x = 0.0, y = -0.0, z = 0.0 },
		Type = -1
	},

	StairsGoTopBottom = {
		Pos	= { x = 0.0, y = -0.0, z = 0.0 },
		Type = -1
	},

	StairsGoBottomTop = {
		Pos	= { x = 0.0, y = -0.0, z = 0.0 },
		Type = -1
	},
    Clothes = {
        Pos	= { x = 342.603, y = -586.301, z = 42.315 },
        Type = 1
    },
	StairsGoBottomBottom = {
		Pos	= { x = 0.0, y = -0.0, z = 0.0 },
		Type = -1
	}

}
