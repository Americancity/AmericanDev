local HasAlreadyEnteredMarker, LastZone, CurrentAction, CurrentActionMsg, CurrentActionData = nil, nil, nil, '', {}
local IsBusy = false
local mediccallcoords={x=303.764,y=-597.999,z=42.291}
local clothescoords={x=342.603,y=-586.301,z=42.315}
local shopcoords={x=316.665,y=-588.253,z=42.291}

function OpenAmbulanceActionsMenu()
	local elements = {
		{label = _U('cloakroom'), value = 'cloakroom'}
	}

	if Config.EnablePlayerManagement and ESX.PlayerData.job.grade_name == 'boss' then
		table.insert(elements, {label = _U('boss_actions'), value = 'boss_actions'})
		table.insert(elements, {label = 'Prendre son service en civil', value = 'serviceon'})
	end

	ESX.UI.Menu.CloseAll()

	ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'ambulance_actions',
	{
		css = 'job',
		title		= _U('ambulance'),
		align		= 'top-left',
		elements	= elements
	}, function(data, menu)
		if data.current.value == 'cloakroom' then
			OpenCloakroomMenu()
		elseif data.current.value == 'boss_actions' then
			TriggerEvent('esx_society:openBossMenu', 'ambulance', function(data, menu)
				menu.close()
			end, {wash = false})
		elseif data.current.value == 'serviceon' then
			TriggerServerEvent("player:serviceOn", "ambulance")
			ESX.ShowNotification("~g~Vous êtes en service !")
		end
	end, function(data, menu)
		menu.close()

		CurrentAction		= 'ambulance_actions_menu'
		CurrentActionMsg	= _U('open_menu')
		CurrentActionData	= {}
	end)
end

function OpenMobileAmbulanceActionsMenu()

	ESX.UI.Menu.CloseAll()

	ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'mobile_ambulance_actions',
	{
		css = 'job',
		title		= _U('ambulance'),
		align		= 'top-left',
		elements	= {
			{label = _U('ems_menu'), value = 'citizen_interaction'},
			{label = 'Prendre des notes', value = 'take_note'},
			{label = 'Annuler l\'appel', value = 'cancellCall'}
		}
	}, function(data, menu)
		if data.current.value == 'take_note' then
			TaskStartScenarioInPlace(GetPlayerPed(-1), 'CODE_HUMAN_MEDIC_TIME_OF_DEATH', 0, true)
		end
		if data.current.value == 'cancellCall' then
			TriggerEvent('call:cancelCall')
			ESX.ShowNotification("~r~Appel annulé")
		end
		if data.current.value == 'citizen_interaction' then
			ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'citizen_interaction',
			{
				css = 'job',
				title		= _U('ems_menu_title'),
				align		= 'top-left',
				elements	= {
					{label = _U('ems_menu_revive'), value = 'revive'},
					{label = _U('ems_menu_small'), value = 'small'},
					{label = _U('ems_menu_big'), value = 'big'},
					{label = 'Faire une facture', value = 'facture'},
					{label = _U('ems_menu_putincar'), value = 'put_in_vehicle'}
				}
			}, function(data, menu)
				if IsBusy then return end

				local closestPlayer, closestDistance = ESX.Game.GetClosestPlayer()

				if closestPlayer == -1 or closestDistance > 3.0 then
					ESX.ShowNotification(_U('no_players'))
				else

					if data.current.value == 'revive' then

						ESX.TriggerServerCallback('esx_ambulancejob:getItemAmount', function(quantity)
							if quantity > 0 then
								local closestPlayerPed = GetPlayerPed(closestPlayer)

								if IsPedDeadOrDying(closestPlayerPed, 1) then
									local playerPed = PlayerPedId()

									IsBusy = true
									ESX.ShowNotification(_U('revive_inprogress'))
									TaskStartScenarioInPlace(playerPed, 'CODE_HUMAN_MEDIC_TEND_TO_DEAD', 0, true)
									Citizen.Wait(10000)
									ClearPedTasks(playerPed)

									TriggerServerEvent('esx_ambulancejob:removeItem', 'medikit')
									TriggerServerEvent('esx_ambulancejob:revive', GetPlayerServerId(closestPlayer))
									IsBusy = false

									-- Show revive award?
									if Config.ReviveReward > 0 then
										ESX.ShowNotification(_U('revive_complete_award', GetPlayerName(closestPlayer), Config.ReviveReward))
									else
										ESX.ShowNotification(_U('revive_complete', GetPlayerName(closestPlayer)))
									end
								else
									ESX.ShowNotification(_U('player_not_unconscious'))
								end
							else
								ESX.ShowNotification(_U('not_enough_medikit'))
							end
						end, 'medikit')

					elseif data.current.value == 'small' then

						ESX.TriggerServerCallback('esx_ambulancejob:getItemAmount', function(quantity)
							if quantity > 0 then
								local closestPlayerPed = GetPlayerPed(closestPlayer)
								local health = GetEntityHealth(closestPlayerPed)

								if health > 0 then
									local playerPed = PlayerPedId()

									IsBusy = true
									ESX.ShowNotification(_U('heal_inprogress'))
									TaskStartScenarioInPlace(playerPed, 'CODE_HUMAN_MEDIC_TEND_TO_DEAD', 0, true)
									Citizen.Wait(10000)
									ClearPedTasks(playerPed)

									TriggerServerEvent('esx_ambulancejob:removeItem', 'bandage')
									TriggerServerEvent('esx_ambulancejob:heal', GetPlayerServerId(closestPlayer), 'small')
									ESX.ShowNotification(_U('heal_complete', GetPlayerName(closestPlayer)))
									IsBusy = false
								else
									ESX.ShowNotification(_U('player_not_conscious'))
								end
							else
								ESX.ShowNotification(_U('not_enough_bandage'))
							end
						end, 'bandage')

					elseif data.current.value == 'big' then

						ESX.TriggerServerCallback('esx_ambulancejob:getItemAmount', function(quantity)
							if quantity > 0 then
								local closestPlayerPed = GetPlayerPed(closestPlayer)
								local health = GetEntityHealth(closestPlayerPed)

								if health > 0 then
									local playerPed = PlayerPedId()

									IsBusy = true
									ESX.ShowNotification(_U('heal_inprogress'))
									TaskStartScenarioInPlace(playerPed, 'CODE_HUMAN_MEDIC_TEND_TO_DEAD', 0, true)
									Citizen.Wait(10000)
									ClearPedTasks(playerPed)

									TriggerServerEvent('esx_ambulancejob:removeItem', 'medikit')
									TriggerServerEvent('esx_ambulancejob:heal', GetPlayerServerId(closestPlayer), 'big')
									ESX.ShowNotification(_U('heal_complete', GetPlayerName(closestPlayer)))
									IsBusy = false
								else
									ESX.ShowNotification(_U('player_not_conscious'))
								end
							else
								ESX.ShowNotification(_U('not_enough_medikit'))
							end
						end, 'medikit')

					elseif data.current.value == 'put_in_vehicle' then
						TriggerServerEvent('esx_ambulancejob:putInVehicle', GetPlayerServerId(closestPlayer))
					elseif data.current.value == 'facture' then
						ESX.UI.Menu.Open(
						'dialog', GetCurrentResourceName(), 'billing',
						{
							title = 'Entrer un montant'
						},
						function(data, menu)

							local amount = tonumber(data.value)

							if amount == nil or amount <= 0 then
								ESX.ShowNotification('Montant invalide')
							else
								menu.close()

								local closestPlayer, closestDistance = ESX.Game.GetClosestPlayer()

								if closestPlayer == -1 or closestDistance > 3.0 then
									ESX.ShowNotification('Pas de personne à côté.')
								else
									local playerPed        = GetPlayerPed(-1)

									Citizen.CreateThread(function()
										TriggerServerEvent('esx_billing:sendBill', GetPlayerServerId(closestPlayer), 'society_ambulance', 'Ambulance', amount)
										TaskStartScenarioInPlace(playerPed, 'CODE_HUMAN_MEDIC_TIME_OF_DEATH', 0, true)
										Citizen.Wait(5000)
										ClearPedTasks(playerPed)
									end)
								end
							end
						end)
					end
				end
			end, function(data, menu)
				menu.close()
			end)
		end

	end, function(data, menu)
		menu.close()
	end)
end


AddEventHandler('esx_ambulancejob:hasEnteredMarker', function(zone)
	if zone == 'HospitalInteriorEntering1' then
		coords=Config.Zones.HospitalInteriorInside1.Pos
		SetEntityCoords(GetPlayerPed(-1), coords.x, coords.y, coords.z)
	elseif zone == 'HospitalInteriorExit1' then
		coords=Config.Zones.HospitalInteriorOutside1.Pos
		SetEntityCoords(GetPlayerPed(-1), coords.x, coords.y, coords.z)
	elseif zone == 'HospitalInteriorEntering2' then
		coords=Config.Zones.HospitalInteriorInside2.Pos
		SetEntityCoords(GetPlayerPed(-1), coords.x, coords.y, coords.z)
	elseif zone == 'HospitalInteriorExit2' then
		coords=Config.Zones.HospitalInteriorOutside2.Pos
		SetEntityCoords(GetPlayerPed(-1), coords.x, coords.y, coords.z)
	elseif zone == 'ParkingDoorGoOutInside' then
		coords=Config.Zones.ParkingDoorGoOutOutside.Pos
		SetEntityCoords(GetPlayerPed(-1), coords.x, coords.y, coords.z)
	elseif zone == 'ParkingDoorGoInOutside' then
		coords=Config.Zones.ParkingDoorGoInInside.Pos
		SetEntityCoords(GetPlayerPed(-1), coords.x, coords.y, coords.z)
	elseif zone == 'StairsGoTopBottom' then
		CurrentAction		= 'fast_travel_goto_top'
		CurrentActionMsg	= _U('fast_travel')
		CurrentActionData	= {pos = Config.Zones.StairsGoTopTop.Pos}
	elseif zone == 'StairsGoBottomTop' then
		CurrentAction		= 'fast_travel_goto_bottom'
		CurrentActionMsg	= _U('fast_travel')
		CurrentActionData	= {pos = Config.Zones.StairsGoBottomBottom.Pos}
	elseif zone == 'AmbulanceActions' then
		CurrentAction		= 'ambulance_actions_menu'
		CurrentActionMsg	= _U('open_menu')
		CurrentActionData	= {}
	elseif zone == 'VehicleSpawner' then
		CurrentAction		= 'vehicle_spawner_menu'
		CurrentActionMsg	= _U('veh_spawn')
		CurrentActionData	= {}
	elseif zone == 'HelicoSpawner' then
		CurrentAction		= 'helico_spawner_menu'
		CurrentActionMsg	= _U('veh_spawn')
		CurrentActionData	= {}
	elseif zone == 'Pharmacy' then
		CurrentAction		= 'pharmacy'
		CurrentActionMsg	= _U('open_pharmacy')
		CurrentActionData	= {}
	elseif zone == 'call' then
		CurrentAction = 'ambulance_call'
		CurrentActionMsg = '~INPUT_CONTEXT~ Appeler un médecin'
		CurrentActionData = {}
	elseif zone == 'clothes' then
		CurrentAction = 'ambulance_clothes'
		CurrentActionMsg = '~INPUT_CONTEXT~ Prendre sa tenue de patient'
		CurrentActionData = {}
	elseif zone == 'VehicleDeleter' or zone =='HelicoDeleter' then
		local playerPed = PlayerPedId()
		local coords	= GetEntityCoords(playerPed)

		if IsPedInAnyVehicle(playerPed, false) then
			local vehicle, distance = ESX.Game.GetClosestVehicle({
				x = coords.x,
				y = coords.y,
				z = coords.z
			})

			if distance ~= -1 and distance <= 2.0 then
				CurrentAction		= 'delete_vehicle'
				CurrentActionMsg	= _U('store_veh')
				CurrentActionData	= {vehicle = vehicle}
			end
		end
	end
end)

function FastTravel(pos)
	coords = pos
	SetEntityCoords(GetPlayerPed(-1), coords.x, coords.y, coords.z)
end

AddEventHandler('esx_ambulancejob:hasExitedMarker', function(zone)
	ESX.UI.Menu.CloseAll()
	CurrentAction = nil
end)

-- Create blips
Citizen.CreateThread(function()
	local blip = AddBlipForCoord(Config.Blip.Pos.x, Config.Blip.Pos.y, Config.Blip.Pos.z)

	SetBlipSprite(blip, Config.Blip.Sprite)
	SetBlipDisplay(blip, Config.Blip.Display)
	SetBlipScale(blip, Config.Blip.Scale)
	SetBlipColour(blip, Config.Blip.Colour)
	SetBlipAsShortRange(blip, true)

	BeginTextCommandSetBlipName("STRING")
	AddTextComponentString(_U('hospital'))
	EndTextCommandSetBlipName(blip)
end)

-- Display markers
Citizen.CreateThread(function()
	while true do
		Citizen.Wait(0)

		local coords = GetEntityCoords(PlayerPedId())
		for k,v in pairs(Config.Zones) do
			if(v.Type ~= -1 and GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < Config.DrawDistance) then
				if ESX.PlayerData.job ~= nil and ESX.PlayerData.job.name == 'ambulance' and k~= 'Clothes' then
					DrawMarker(v.Type, v.Pos.x, v.Pos.y, v.Pos.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, Config.MarkerSize.x, Config.MarkerSize.y, Config.MarkerSize.z, Config.MarkerColor.r, Config.MarkerColor.g, Config.MarkerColor.b, 100, false, true, 2, false, false, false, false)
				elseif k == 'HelicoDeleter' then
					DrawMarker(v.Type, v.Pos.x, v.Pos.y, v.Pos.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, Config.MarkerSizeH.x, Config.MarkerSizeH.y, Config.MarkerSizeH.z, Config.MarkerColor.r, Config.MarkerColor.g, Config.MarkerColor.b, 100, false, true, 2, false, false, false, false)
				elseif k ~= 'AmbulanceActions' and k ~= 'Clothes' and k ~= 'VehicleSpawner' and k ~= 'VehicleDeleter' and k ~= 'Pharmacy' and k ~= 'StairsGoTopBottom' and k ~= 'StairsGoBottomTop' then
					DrawMarker(v.Type, v.Pos.x, v.Pos.y, v.Pos.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, Config.MarkerSize.x, Config.MarkerSize.y, Config.MarkerSize.z, Config.MarkerColor.r, Config.MarkerColor.g, Config.MarkerColor.b, 100, false, true, 2, false, false, false, false)
				end
			end
		end
		if(GetDistanceBetweenCoords(coords, mediccallcoords.x,mediccallcoords.y,mediccallcoords.z) < 15) then
			DrawMarker(0, mediccallcoords.x, mediccallcoords.y, mediccallcoords.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, 1.5, 1.5, 0.5, 246, 255, 0, 100, false, true, 2, false, false, false, false)
		end
		if(GetDistanceBetweenCoords(coords, clothescoords.x,clothescoords.y,clothescoords.z) < 15) then
			DrawMarker(0, clothescoords.x, clothescoords.y, clothescoords.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, 1.5, 1.5, 0.5, 255, 255, 255, 100, false, true, 2, false, false, false, false)
		end
	end
end)

-- Activate menu when player is inside marker
Citizen.CreateThread(function()
	while true do
		Citizen.Wait(10)

		local coords		= GetEntityCoords(PlayerPedId())
		local isInMarker	= false
		local currentZone	= nil

		for k,v in pairs(Config.Zones) do
			if ESX.PlayerData.job ~= nil and ESX.PlayerData.job.name == 'ambulance' then
				if(GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < Config.MarkerSize.x) then
					isInMarker	= true
					currentZone = k
				end
			elseif k ~= 'AmbulanceActions' and k ~= 'VehicleSpawner' and k ~= 'VehicleDeleter' and k ~= 'Pharmacy' and k ~= 'StairsGoTopBottom' and k ~= 'StairsGoBottomTop' then
				if(GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < Config.MarkerSize.x) then
					isInMarker	= true
					currentZone = k
				end
			end
			if k == 'HelicoDeleter' then
				if(GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < Config.MarkerSizeH.x) then
					isInMarker	= true
					currentZone = k
				end
			end
		end
		if(GetDistanceBetweenCoords(coords, mediccallcoords.x,mediccallcoords.y,mediccallcoords.z) < 1.5) then
			isInMarker = true
			currentZone = 'call'
		end
		if(GetDistanceBetweenCoords(coords, clothescoords.x,clothescoords.y,clothescoords.z) < 1.5) then
			isInMarker = true
			currentZone = 'clothes'
		end

		if isInMarker and not hasAlreadyEnteredMarker then
			hasAlreadyEnteredMarker = true
			lastZone				= currentZone
			TriggerEvent('esx_ambulancejob:hasEnteredMarker', currentZone)
		end

		if not isInMarker and hasAlreadyEnteredMarker then
			hasAlreadyEnteredMarker = false
			TriggerEvent('esx_ambulancejob:hasExitedMarker', lastZone)
		end
	end
end)

-- Key Controls
Citizen.CreateThread(function()
	while true do
		Citizen.Wait(10)

		if CurrentAction ~= nil then
			local coords = GetEntityCoords(PlayerPedId())
			ESX.ShowHelpNotification(CurrentActionMsg)

			if IsControlJustReleased(0, 38) and ESX.PlayerData.job ~= nil and ESX.PlayerData.job.name == 'ambulance' then

				if CurrentAction == 'ambulance_actions_menu' then
					OpenAmbulanceActionsMenu()
				elseif CurrentAction == 'vehicle_spawner_menu' then
					OpenVehicleSpawnerMenu()
				elseif CurrentAction == 'helico_spawner_menu' then
					OpenHelicoSpawnerMenu()
				elseif CurrentAction == 'pharmacy' then
					OpenPharmacyMenu()
				elseif CurrentAction == 'fast_travel_goto_top' or CurrentAction == 'fast_travel_goto_bottom' then
					FastTravel(CurrentActionData.pos)
				elseif CurrentAction == 'delete_vehicle' then
					if Config.EnableSocietyOwnedVehicles then
						local vehicleProps = ESX.Game.GetVehicleProperties(CurrentActionData.vehicle)
						TriggerServerEvent('esx_society:putVehicleInGarage', 'ambulance', vehicleProps)
					end
					ESX.Game.DeleteVehicle(CurrentActionData.vehicle)
				elseif CurrentAction == 'ambulance_call' then
					TriggerServerEvent("call:makeCall","ambulance",coords,"BIPPER - Accueil hôpital")
					CurrentAction = nil
				elseif CurrentAction == 'ambulance_clothes' then
					TriggerEvent('skinchanger:getSkin', function(skin)
						ChangeClothes(skin)
					end)
					CurrentAction = nil
				end

				CurrentAction = nil

			end

		end

		if IsControlJustReleased(0, 166) and ESX.PlayerData.job ~= nil and ESX.PlayerData.job.name == 'ambulance' and not IsDead then
			OpenMobileAmbulanceActionsMenu()
		end
	end
end)

RegisterNetEvent('esx_ambulancejob:putInVehicle')
AddEventHandler('esx_ambulancejob:putInVehicle', function()
	local playerPed = PlayerPedId()
	local coords    = GetEntityCoords(playerPed)

	if IsAnyVehicleNearPoint(coords.x, coords.y, coords.z, 5.0) then
		local vehicle = GetClosestVehicle(coords.x, coords.y, coords.z, 5.0, 0, 71)

		if DoesEntityExist(vehicle) then
			local maxSeats = GetVehicleMaxNumberOfPassengers(vehicle)
			local freeSeat = nil

			for i=maxSeats - 1, 0, -1 do
				if IsVehicleSeatFree(vehicle, i) then
					freeSeat = i
					break
				end
			end

			if freeSeat ~= nil then
				TaskWarpPedIntoVehicle(playerPed, vehicle, freeSeat)
			end
		end
	end
end)


function OpenCloakroomMenu()
	ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'cloakroom',
	{
		css = 'vestiaire',
		title		= _U('cloakroom'),
		align		= 'top-left',
		elements = {
			{label = 'Chemise EMS', value = 'ambulance_wear'},
			{label = 'Manteau EMS', value = 'manteau_wear'},
			{label = 'Opération EMS', value = 'op_wear'},
			{label = 'Tenue civile', value = 'citizen_wear'},
		}
	}, function(data, menu)
		if data.current.value == 'citizen_wear' then
			ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin, jobSkin)
				TriggerEvent('skinchanger:loadSkin', skin)
				TriggerServerEvent("player:serviceOff", "ambulance")
				ESX.ShowNotification("~r~Fin de service !")
			end)
		elseif data.current.value == 'ambulance_wear' then
			TriggerEvent('skinchanger:getSkin', function(skin)
				TriggerServerEvent("player:serviceOn", "ambulance")
				clothes={['tshirt_1'] = 129, ['torso_1'] = 250, ['torso_2'] = 0, ['arms'] = 85, ['arms_2'] = 0, ['pants_1'] = 96, ['pants_2'] = 0, ['decals_1'] = 58, ['shoes_1'] = 24, ['chain_1'] = 126}
				TriggerEvent('skinchanger:loadClothes', skin, clothes)
				ESX.ShowNotification("~b~Voici votre tenue !")
			end)
			
		elseif data.current.value == 'manteau_wear' then
			TriggerEvent('skinchanger:getSkin', function(skin)
				TriggerServerEvent("player:serviceOn", "ambulance")
				clothes={['tshirt_1'] = 15, ['torso_1'] = 249, ['torso_2'] = 0, ['arms'] = 90, ['arms_2'] = 0, ['pants_1'] = 96, ['pants_2'] = 0, ['decals_1'] = 57, ['shoes_1'] = 24, ['chain_1'] = 126}
				TriggerEvent('skinchanger:loadClothes', skin, clothes)
				ESX.ShowNotification("~b~Voici votre tenue !")
			end)
			
		elseif data.current.value == 'op_wear' then
			TriggerEvent('skinchanger:getSkin', function(skin)
				TriggerServerEvent("player:serviceOn", "ambulance")
				clothes={['tshirt_1'] = 15, ['torso_1'] = 221, ['torso_2'] = 22, ['arms'] = 90, ['arms_2'] = 0, ['pants_1'] = 27, ['pants_2'] = 3, ['decals_1'] = 0, ['shoes_1'] = 24, ['chain_1'] = 0}
				TriggerEvent('skinchanger:loadClothes', skin, clothes)
				ESX.ShowNotification("~b~Voici votre tenue !")
			end)
			
		end

		menu.close()
		CurrentAction		= 'ambulance_actions_menu'
		CurrentActionMsg	= _U('open_menu')
		CurrentActionData	= {}
	end, function(data, menu)
		menu.close()
	end)
end

function OpenHelicoSpawnerMenu()

	ESX.UI.Menu.CloseAll()

	if Config.EnableSocietyOwnedVehicles then

		local elements = {}

		ESX.TriggerServerCallback('esx_society:getVehiclesInGarage', function(vehicles)
			for i=1, #vehicles, 1 do
				table.insert(elements, {
					label = GetDisplayNameFromVehicleModel(vehicles[i].model) .. ' [' .. vehicles[i].plate .. ']',
					value = vehicles[i]
				})
			end

			ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'vehicle_spawner',
			{
				css = 'vehicle',
				title		= _U('veh_menu'),
				align		= 'top-left',
				elements = elements
			}, function(data, menu)
				menu.close()

				local vehicleProps = data.current.value
				ESX.Game.SpawnVehicle(vehicleProps.model, Config.Zones.HelicoSpawnPoint.Pos, 230.0, function(vehicle)
					ESX.Game.SetVehicleProperties(vehicle, vehicleProps)
					SetVehicleModKit(vehicle, 0)
				    SetVehicleLivery(vehicle, 1)

					local playerPed = PlayerPedId()
					TaskWarpPedIntoVehicle(playerPed, vehicle, -1)
				end)
				TriggerServerEvent('esx_society:removeVehicleFromGarage', 'ambulance', vehicleProps)
			end, function(data, menu)
				menu.close()
				CurrentAction		= 'helico_spawner_menu'
				CurrentActionMsg	= _U('veh_spawn')
				CurrentActionData	= {}
			end)
		end, 'ambulance')
	end
end
function OpenVehicleSpawnerMenu()

	ESX.UI.Menu.CloseAll()

	if Config.EnableSocietyOwnedVehicles then

		local elements = {}

		ESX.TriggerServerCallback('esx_society:getVehiclesInGarage', function(vehicles)
			for i=1, #vehicles, 1 do
				table.insert(elements, {
					label = GetDisplayNameFromVehicleModel(vehicles[i].model) .. ' [' .. vehicles[i].plate .. ']',
					value = vehicles[i]
				})
			end

			ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'vehicle_spawner',
			{
				css = 'vehicle',
				title		= _U('veh_menu'),
				align		= 'top-left',
				elements = elements
			}, function(data, menu)
				menu.close()

				local vehicleProps = data.current.value
				ESX.Game.SpawnVehicle(vehicleProps.model, Config.Zones.VehicleSpawnPoint.Pos, 230.0, function(vehicle)
					ESX.Game.SetVehicleProperties(vehicle, vehicleProps)
					local playerPed = PlayerPedId()
					TaskWarpPedIntoVehicle(playerPed, vehicle, -1)
				end)
				TriggerServerEvent('esx_society:removeVehicleFromGarage', 'ambulance', vehicleProps)
			end, function(data, menu)
				menu.close()
				CurrentAction		= 'vehicle_spawner_menu'
				CurrentActionMsg	= _U('veh_spawn')
				CurrentActionData	= {}
			end)
		end, 'ambulance')

	else -- not society vehicles

		ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'vehicle_spawner',
		{
			css = 'vehicle',
			title		= _U('veh_menu'),
			align		= 'top-left',
			elements	= Config.AuthorizedVehicles
		}, function(data, menu)
			menu.close()

			ESX.Game.SpawnVehicle(data.current.model, Config.Zones.VehicleSpawnPoint.Pos, 230.0, function(vehicle)
				local playerPed = PlayerPedId()
				TaskWarpPedIntoVehicle(playerPed, vehicle, -1)
			end)
		end, function(data, menu)
			menu.close()
			CurrentAction		= 'vehicle_spawner_menu'
			CurrentActionMsg	= _U('veh_spawn')
			CurrentActionData	= {}
		end)

	end
end

function OpenPharmacyMenu()
	ESX.UI.Menu.CloseAll()

	ESX.UI.Menu.Open('default', GetCurrentResourceName(), 'pharmacy',
	{
		css = 'entreprise',
		title		= _U('pharmacy_menu_title'),
		align		= 'top-left',
		elements = {
			{label = _U('pharmacy_take', _U('medikit')), value = 'medikit'},
			{label = _U('pharmacy_take', _U('bandage')), value = 'bandage'},
			{label = _U('pharmacy_take', 'Boîte de médicaments'), value = 'pillbox'}
		}
	}, function(data, menu)
		TriggerServerEvent('esx_ambulancejob:giveItem', data.current.value)
	end, function(data, menu)
		menu.close()

		CurrentAction		= 'pharmacy'
		CurrentActionMsg	= _U('open_pharmacy')
		CurrentActionData	= {}
	end)
end

function WarpPedInClosestVehicle(ped)
	local coords = GetEntityCoords(ped)

	local vehicle, distance = ESX.Game.GetClosestVehicle({
		x = coords.x,
		y = coords.y,
		z = coords.z
	})

	if distance ~= -1 and distance <= 5.0 then
		local maxSeats = GetVehicleMaxNumberOfPassengers(vehicle)
		local freeSeat = nil

		for i=maxSeats - 1, 0, -1 do
			if IsVehicleSeatFree(vehicle, i) then
				freeSeat = i
				break
			end
		end

		if freeSeat ~= nil then
			TaskWarpPedIntoVehicle(ped, vehicle, freeSeat)
		end
	else
		ESX.ShowNotification(_U('no_vehicles'))
	end
end

RegisterNetEvent('esx_ambulancejob:heal')
AddEventHandler('esx_ambulancejob:heal', function(healType)
	local playerPed = PlayerPedId()
	local maxHealth = GetEntityMaxHealth(playerPed)

	if healType == 'small' then
		local health = GetEntityHealth(playerPed)
		local newHealth = math.min(maxHealth, math.floor(health + maxHealth / 8))
		SetEntityHealth(playerPed, newHealth)
	elseif healType == 'big' then
		SetEntityHealth(playerPed, maxHealth)
	end

	ESX.ShowNotification(_U('healed'))
end)

RegisterNetEvent('esx_ambulance:startpill')
AddEventHandler('esx_ambulance:startpill', function(source)
	startanimpill()
end)
function loadAnimDict( dict )
	while ( not HasAnimDictLoaded( dict ) ) do
		RequestAnimDict( dict )
		Citizen.Wait( 0 )
	end
end
function startanimpill()
	local ped = GetPlayerPed(-1)
	Citizen.CreateThread(function()
	    if DoesEntityExist( ped ) and not IsEntityDead( ped ) and not IsPedInAnyVehicle(PlayerPedId(), true) then
            loadAnimDict( "mp_suicide")
            TaskPlayAnim(ped, "mp_suicide", "pill", 8.0, 2.0, 3000, 48, 10, 0, 0, 0 )
            ESX.UI.Menu.CloseAll()
        end
    end)
end

function ChangeClothes(currentSkin)
	ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin, jobSkin)
		if skin['tshirt_1'] == currentSkin['tshirt_1'] then
			if skin.sex == 0 then
        		local clothesSkin = {
                	['tshirt_1'] = 15, ['tshirt_2'] = 0,
                	['ears_1'] = -1, ['ears_2'] = 0,
                	['torso_1'] = 114, ['torso_2'] = 0,
                	['decals_1'] = 0,  ['decals_2']= 0,
                	['mask_1'] = 0, ['mask_2'] = 0,
                	['arms'] = 14,
                	['pants_1'] = 56, ['pants_2'] = 0,
                	['shoes_1'] = 5, ['shoes_2'] = 0,
                	['helmet_1']    = -1, ['helmet_2'] = 0,
                	['bags_1'] = 0, ['bags_2'] = 0,
                	['glasses_1'] = 0, ['glasses_2'] = 0,
                	['chain_1'] = 0, ['chain_2'] = 0,
                	['bproof_1'] = 0,  ['bproof_2'] = 0
            	}
        		TriggerEvent('skinchanger:loadClothes', skin, clothesSkin)
    		else
        		local clothesSkin = {
           			['tshirt_1'] = 14, ['tshirt_2'] = 0,
		        	['ears_1'] = -1, ['ears_2'] = 0,
          			['torso_1'] = 105, ['torso_2'] 	= 0,
           			['decals_1'] = 0,  ['decals_2'] = 0,
           			['mask_1'] = 0, ['mask_2'] 	= 0,
           			['arms'] = 0,
           			['pants_1'] = 57, ['pants_2'] 	= 0,
           			['shoes_1'] = 5, ['shoes_2'] 	= 0,
           			['helmet_1']= -1, ['helmet_2'] 	= 0,
         			['bags_1'] = 0, ['bags_2']	= 0,
	       			['glasses_1'] = 0, ['glasses_2'] = 0,
	       			['chain_1'] = 0, ['chain_2'] = 0,
           			['bproof_1'] = 0,  ['bproof_2'] = 0
       			}
       			TriggerEvent('skinchanger:loadClothes', skin, clothesSkin)
       		end
		else
			TriggerEvent('skinchanger:loadSkin', skin)
		end	
	end)
end