Config                            = {}
Config.DrawDistance               = 100.0
Config.MaxInService               = -1
Config.EnablePlayerManagement     = false
Config.EnableSocietyOwnedVehicles = false
Config.Locale                     = 'fr'

Config.Zones = {

	stoneFarm = {
		Pos   = {x = 2948.56, y = 2796.25, z = 37.79},
		Size  = {x = 20.0, y = 20.0, z = 5.0},
		Color = {r = 136, g = 243, b = 0},
		Name  = "Récolte de pierre",
		Type  = 1
	},


	Traitement = {
		Pos   = {x = 1109.14, y = -2007.87, z = 30.0188},
		Size  = {x = 3.5, y = 3.5, z = 1.0},
		Color = {r = 136, g = 243, b = 216},
		Name  = "Traitement de la pierre",
		Type  = 1
	},


	SellFarm = {
		Pos   = {x = -169.481, y = -2659.16, z = 5.00103},
		Size  = {x = 3.5, y = 3.5, z = 1.0},
		Color = {r = 136, g = 243, b = 216},
		Name  = "Vente des produits",
		Type  = 1
	},

	minerjobActions = {
		Pos   = {x = 2569.48, y = 2720.27, z = 41.9486},
		Size  = {x = 1.5, y = 1.5, z = 0.5},
		Color = {r = 255, g = 233, b = 2},
		Name  = "Point d'action",
		Type  = 0
	 },

	VehicleSpawner = {
		Pos   = {x = 2574.13, y = 2725.64, z = 42.0673},
		Size = {x = 1.5, y = 1.5, z = 0.5},
		Color = {r = 2, g = 53, b = 255},
		Name  = "Garage véhicule",
		Type  = 0
	},

	VehicleSpawnPoint = {
		Pos   = {x = 2587.18, y = 2717.93, z = 41.6877},
		Size  = {x = 1.5, y = 1.5, z = 1.0},
		Color = {r = 136, g = 243, b = 216},
		Name  = "Spawn point",
		Type  = -1
	},

	VehicleDeleter = {
		Pos   = {x = 2587.18, y = 2717.93, z = 41.6877},
		Size  = {x = 3.0, y = 3.0, z = 0.5},
		Color = {r = 2, g = 255, b = 65},
		Name  = "Ranger son véhicule",
		Type  = 0
	}

}
