-----------------------------------------
-- Created and modify by L'ile Légale RP
-- SenSi and Kaminosekai
-----------------------------------------
Locales['fr'] = {
	-- General
	['cloakroom'] = 'Vestiaire',
	['vine_clothes_civil'] = 'Tenue Civil',
	['vine_clothes_vine'] = 'Tenue de travail',
	['veh_menu'] = 'Garage',
	['spawn_veh'] = 'appuyez sur ~INPUT_CONTEXT~ pour sortir véhicule',
	['amount_invalid'] = 'montant invalide',
	['press_to_open'] = 'appuyez sur ~INPUT_CONTEXT~ pour accéder au menu',
	['billing'] = 'facuration',
	['invoice_amount'] = 'montant de la facture',
	['no_players_near'] = 'aucun joueur à proximité',
	['store_veh'] = 'appuyez sur ~INPUT_CONTEXT~ pour ranger le véhicule',
	['comp_earned'] = 'vous avez gagné ~g~$',
	['deposit_stock'] = 'Déposer Stock',
	['take_stock'] = 'Prendre Stock',
	['boss_actions'] = 'action Patron',
	['quantity'] = 'Quantité',
	['quantity_invalid'] = '~r~Quantité invalide~w~',
	['inventory'] = 'Inventaire',
	['have_withdrawn'] = 'Vous avez retiré x',
	['added'] = 'Vous avez ajouté x',
	['not_enough_place'] = 'Vous n\'avez plus de place',
	['sale_in_prog'] = 'Revente en cours...',
	['van'] = 'Camion de travail',
	['open_menu'] = ' ',

	-- A modifier selon l'entreprise
	['minerjob_client'] = 'client vigne',
	['transforming_in_progress'] = 'Transformation de la pierre en cours',
	['stone_taken'] = 'Vous être en train de ramasser de la pierre',
	['press_traitement'] = 'appuyez sur ~INPUT_CONTEXT~ pour transformer votre pierre',
	['press_collect'] = 'appuyez sur ~INPUT_CONTEXT~ pour récolter de la pierre',
	['press_sell'] = 'appuyez sur ~INPUT_CONTEXT~ pour vendre vos produits',
	['no_gold_sale'] = 'Vous n\'avez plus d\'or',
	['no_iron_sale'] = 'Vous n\'avez plus de fer',
	['no_diamond_sale'] = 'Vous n\'avez plus de diamant',
	['not_enough_stone'] = 'Vous n\'avez plus de la pierre',
	['grand_cru'] = '~g~Génial, un minerai de valeur!~w~',
	['no_product_sale'] = 'Vous n\'avez plus de produits',
}
