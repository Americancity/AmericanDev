-----------------------------------------
-- Created and modify by L'ile Légale RP
-- SenSi and Kaminosekai
-----------------------------------------

ESX = nil
local PlayersTransforming  = {}
local PlayersSelling       = {}
local PlayersHarvesting = {}
local iron = 1
local gold = 1
local diamond = 1
TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

if Config.MaxInService ~= -1 then
	TriggerEvent('esx_service:activateService', 'miner', Config.MaxInService)
end

local function Harvest(source, zone)
	if PlayersHarvesting[source] == true then
		local xPlayer  = ESX.GetPlayerFromId(source)
		local randstone = math.random(1, 3)
		if zone == "stoneFarm" then
			local itemQuantity = xPlayer.getInventoryItem('stone').count
			if itemQuantity >= 50 then
				TriggerClientEvent('esx:showNotification', source, _U('not_enough_place'))
				return
			else
				SetTimeout(7000, function()
					xPlayer.addInventoryItem('stone', randstone)
					Harvest(source, zone)
				end)
			end
		end
	end
end

RegisterServerEvent('esx_minerjob:startHarvest')
AddEventHandler('esx_minerjob:startHarvest', function(zone)
	local _source = source

	if PlayersHarvesting[_source] == false then
		TriggerClientEvent('esx:showNotification', _source, '~r~C\'est pas bien de glitch ~w~')
		PlayersHarvesting[_source]=false
	else
		PlayersHarvesting[_source]=true
		TriggerClientEvent('esx:showNotification', _source, _U('stone_taken'))
		Harvest(_source,zone)
	end
end)


RegisterServerEvent('esx_minerjob:stopHarvest')
AddEventHandler('esx_minerjob:stopHarvest', function()
	local _source = source

	if PlayersHarvesting[_source] == true then
		PlayersHarvesting[_source]=false
		TriggerClientEvent('esx:showNotification', _source, 'Vous sortez de la ~r~zone')
	else
		TriggerClientEvent('esx:showNotification', _source, 'Vous pouvez ~g~récolter')
		PlayersHarvesting[_source]=true
	end
end)


local function Transform(source, zone)

	if PlayersTransforming[source] == true then

		local xPlayer  = ESX.GetPlayerFromId(source)
		if zone == "Traitement" then
			local itemQuantity = xPlayer.getInventoryItem('stone').count

			if itemQuantity <= 0 then
				TriggerClientEvent('esx:showNotification', source, _U('not_enough_stone'))
				return
			else
				local rand = math.random(0,100)
				if (rand >= 76) and (rand <= 82) then
					SetTimeout(1800, function()
						xPlayer.removeInventoryItem('stone', 1)
						xPlayer.addInventoryItem('gold', 1)
						TriggerClientEvent('esx:showNotification', source, _U('grand_cru'))
						Transform(source, zone)
					end)
			  elseif (rand >= 82) and (rand <= 85) then
					SetTimeout(1800, function()
						xPlayer.removeInventoryItem('stone', 1)
						xPlayer.addInventoryItem('diamond', 1)
						TriggerClientEvent('esx:showNotification', source, _U('grand_cru'))
						Transform(source, zone)
					end)
				else
				  SetTimeout(1800, function()
					  xPlayer.removeInventoryItem('stone', 1)
					  xPlayer.addInventoryItem('iron', 1)
						Transform(source, zone)
					end)
				end
			end
		end
	end
end

RegisterServerEvent('esx_minerjob:startTransform')
AddEventHandler('esx_minerjob:startTransform', function(zone)
	local _source = source

	if PlayersTransforming[_source] == false then
		TriggerClientEvent('esx:showNotification', _source, '~r~C\'est pas bien de glitch ~w~')
		PlayersTransforming[_source]=false
	else
		PlayersTransforming[_source]=true
		TriggerClientEvent('esx:showNotification', _source, _U('transforming_in_progress'))
		Transform(_source,zone)
	end
end)

RegisterServerEvent('esx_minerjob:stopTransform')
AddEventHandler('esx_minerjob:stopTransform', function()

	local _source = source

	if PlayersTransforming[_source] == true then
		PlayersTransforming[_source]=false
		TriggerClientEvent('esx:showNotification', _source, 'Vous sortez de la ~r~zone')

	else
		TriggerClientEvent('esx:showNotification', _source, 'Vous pouvez ~g~transformer votre raisin')
		PlayersTransforming[_source]=true

	end
end)

local function Sell(source, zone)

	if PlayersSelling[source] == true then
		local xPlayer  = ESX.GetPlayerFromId(source)

		if zone == 'SellFarm' then
			if xPlayer.getInventoryItem('iron').count <= 0 then
				iron = 0
			else
				iron = 1
			end

			if xPlayer.getInventoryItem('gold').count <= 0 then
				gold = 0
			else
				gold = 1
			end

			if xPlayer.getInventoryItem('diamond').count <= 0 then
				diamond = 0
			else
				diamond = 1
			end
			if iron == 0 and gold == 0 and diamond == 0 then
				TriggerClientEvent('esx:showNotification', source, _U('no_product_sale'))
				return
			elseif xPlayer.getInventoryItem('iron').count <= 0 and gold == 0 and diamond == 0 then
				TriggerClientEvent('esx:showNotification', source, _U('no_iron_sale'))
				iron = 0
				return
			elseif xPlayer.getInventoryItem('gold').count <= 0 and diamond == 0 and iron == 0 then
				TriggerClientEvent('esx:showNotification', source, _U('no_gold_sale'))
				gold = 0
				return
			elseif xPlayer.getInventoryItem('diamond').count <= 0 and gold == 0 and iron == 0 then
				TriggerClientEvent('esx:showNotification', source, _U('no_diamond_sale'))
				diamond = 0
				return
			elseif (gold == 1) then
			  SetTimeout(1100, function()
					local money = math.random(10,13) --player management 
					xPlayer.removeInventoryItem('gold', 1)
					xPlayer.addMoney(money)
					local societyAccount = nil
					TriggerEvent('esx_addonaccount:getSharedAccount', 'society_miner', function(account)
						societyAccount = account
					end)
					if societyAccount ~= nil then
						xPlayer.addMoney(money)
						--societyAccount.addMoney(money)
						TriggerClientEvent('esx:showNotification', xPlayer.source, _U('comp_earned') .. money)
					end
					Sell(source,zone)
				end)
				elseif (iron == 1) then
					SetTimeout(1100, function()
					 local money = math.random(6,9)
						xPlayer.removeInventoryItem('iron', 1)
						xPlayer.addMoney(money)
						local societyAccount = nil

						TriggerEvent('esx_addonaccount:getSharedAccount', 'society_miner', function(account)
							societyAccount = account
						end)
						if societyAccount ~= nil then
							xPlayer.addMoney(money)
							--societyAccount.addMoney(money)
							TriggerClientEvent('esx:showNotification', xPlayer.source, _U('comp_earned') .. money)
						end
						Sell(source,zone)
					end)
				elseif (diamond == 1) then
						SetTimeout(1100, function()
							local money = math.random(15,20)
							xPlayer.removeInventoryItem('diamond', 1)
							xPlayer.addMoney(money)
							local societyAccount = nil
							TriggerEvent('esx_addonaccount:getSharedAccount', 'society_miner', function(account)
								societyAccount = account
							end)
							if societyAccount ~= nil then
								xPlayer.addMoney(money)
								--societyAccount.addMoney(money)
								TriggerClientEvent('esx:showNotification', xPlayer.source, _U('comp_earned') .. money)
							end
							Sell(source,zone)
					  end)
				end
			end
		end
	end


RegisterServerEvent('esx_minerjob:startSell')
AddEventHandler('esx_minerjob:startSell', function(zone)

	local _source = source

	if PlayersSelling[_source] == false then
		TriggerClientEvent('esx:showNotification', _source, '~r~C\'est pas bien de glitch ~w~')
		PlayersSelling[_source]=false
	else
		PlayersSelling[_source]=true
		TriggerClientEvent('esx:showNotification', _source, _U('sale_in_prog'))
		Sell(_source, zone)
	end

end)

RegisterServerEvent('esx_minerjob:stopSell')
AddEventHandler('esx_minerjob:stopSell', function()

	local _source = source

	if PlayersSelling[_source] == true then
		PlayersSelling[_source]=false
		TriggerClientEvent('esx:showNotification', _source, 'Vous sortez de la ~r~zone')

	else
		TriggerClientEvent('esx:showNotification', _source, 'Vous pouvez ~g~vendre')
		PlayersSelling[_source]=true
	end

end)

RegisterServerEvent('esx_minerjob:getStockItem')
AddEventHandler('esx_minerjob:getStockItem', function(itemName, count)

	local xPlayer = ESX.GetPlayerFromId(source)

	TriggerEvent('esx_addoninventory:getSharedInventory', 'society_miner', function(inventory)

		local item = inventory.getItem(itemName)

		if item.count >= count then
			inventory.removeItem(itemName, count)
			xPlayer.addInventoryItem(itemName, count)
		else
			TriggerClientEvent('esx:showNotification', xPlayer.source, _U('quantity_invalid'))
		end

		TriggerClientEvent('esx:showNotification', xPlayer.source, _U('have_withdrawn') .. count .. ' ' .. item.label)

	end)

end)

ESX.RegisterServerCallback('esx_minerjob:getStockItems', function(source, cb)

	TriggerEvent('esx_addoninventory:getSharedInventory', 'society_miner', function(inventory)
		cb(inventory.items)
	end)

end)

RegisterServerEvent('esx_minerjob:putStockItems')
AddEventHandler('esx_minerjob:putStockItems', function(itemName, count)

	local xPlayer = ESX.GetPlayerFromId(source)

	TriggerEvent('esx_addoninventory:getSharedInventory', 'society_miner', function(inventory)

		local item = inventory.getItem(itemName)

		if item.count >= 0 then
			xPlayer.removeInventoryItem(itemName, count)
			inventory.addItem(itemName, count)
		else
			TriggerClientEvent('esx:showNotification', xPlayer.source, _U('quantity_invalid'))
		end

		TriggerClientEvent('esx:showNotification', xPlayer.source, _U('added') .. count .. ' ' .. item.label)

	end)
end)

ESX.RegisterServerCallback('esx_minerjob:getPlayerInventory', function(source, cb)

	local xPlayer    = ESX.GetPlayerFromId(source)
	local items      = xPlayer.inventory

	cb({
		items      = items
	})

end)
