-----------------------------------------
-- Created and modify by L'ile Légale RP
-- SenSi and Kaminosekai
-----------------------------------------
Locales['fr'] = {
	-- General
	['cloakroom'] = 'Vestiaire',
	['saucisson_clothes_civil'] = 'Tenue Civil',
	['saucisson_clothes_saucisson'] = 'Tenue de travail',
	['veh_menu'] = 'Garage',
	['spawn_veh'] = 'appuyez sur ~INPUT_CONTEXT~ pour sortir véhicule',
	['amount_invalid'] = 'montant invalide',
	['press_to_open'] = 'appuyez sur ~INPUT_CONTEXT~ pour accéder au menu',
	['billing'] = 'facuration',
	['invoice_amount'] = 'montant de la facture',
	['no_players_near'] = 'aucun joueur à proximité',
	['store_veh'] = 'appuyez sur ~INPUT_CONTEXT~ pour ranger le véhicule',
	['comp_earned'] = 'vous avez gagné ~g~$',
	['deposit_stock'] = 'Déposer Stock',
	['take_stock'] = 'Prendre Stock',
	['boss_actions'] = 'action Patron',
	['quantity'] = 'Quantité',
	['quantity_invalid'] = '~r~Quantité invalide~w~',
	['inventory'] = 'Inventaire',
	['have_withdrawn'] = 'Vous avez retiré x',
	['added'] = 'Vous avez ajouté x',
	['not_enough_place'] = 'Vous n\'avez plus de place',
	['sale_in_prog'] = 'Revente en cours...',
	['van'] = 'Camion de travail',
	['open_menu'] = ' ',

	-- A modifier selon l'entreprise
	['boucher_client'] = 'client boucher',
	['transforming_in_progress'] = 'Transformation de la viande en cours',
	['viande_taken'] = 'Vous être en train de ramasser de la viande',
	['press_traitement_saucisson'] = 'appuyez sur ~INPUT_CONTEXT~ pour faire du saucisson',
	['press_collect'] = 'appuyez sur ~INPUT_CONTEXT~ pour récuperer de la viande',
	['press_sell'] = 'appuyez sur ~INPUT_CONTEXT~ pour vendre vos produits',
	['no_jambon_sale'] = 'Vous n\'avez plus ou pas assez de jambon',
	['no_saucisson_sale'] = 'Vous n\'avez plus ou pas assez de saucisson',
	['not_enough_viande'] = 'Vous n\'avez plus de viande',
	['grand_cru'] = '~g~Génial, un viande de qualité !~w~',
	['no_product_sale'] = 'Vous n\'avez plus de produits',
	['press_traitement_jambon'] = 'appuyez sur ~INPUT_CONTEXT~ pour transformer votre viande',
	['used_jambon'] = 'Vous avez manger une tranche de jambon',
	['used_grand_cru'] = 'Vous avez manger une de la viande de qualité',
	['used_saucisson'] = 'Vous avez manger un saucisson',

}
