-----------------------------------------
-- Created and modify by L'ile Légale RP
-- SenSi and Kaminosekai
-----------------------------------------

ESX = nil
local PlayersTransforming  = {}
local PlayersSelling       = {}
local PlayersHarvesting = {}
local saucisson = 1
local jambon = 1
TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

if Config.MaxInService ~= -1 then
	TriggerEvent('esx_service:activateService', 'butcher', Config.MaxInService)
end

TriggerEvent('esx_phone:registerNumber', 'butcher', _U('boucher_client'), true, true)
TriggerEvent('esx_society:registerSociety', 'butcher', 'boucher', 'society_butcher', 'society_butcher', 'society_butcher', {type = 'private'})
local function Harvest(source, zone)
	if PlayersHarvesting[source] == true then

		local xPlayer  = ESX.GetPlayerFromId(source)
		if zone == "viandeFarm" then
			local itemQuantity = xPlayer.getInventoryItem('viande').count
			if itemQuantity >= 50 then
				TriggerClientEvent('esx:showNotification', source, _U('not_enough_place'))
				return
			else
				SetTimeout(1800, function()
					xPlayer.addInventoryItem('viande', 1)
					Harvest(source, zone)
				end)
			end
		end
	end
end

RegisterServerEvent('esx_boucherjob:startHarvest')
AddEventHandler('esx_boucherjob:startHarvest', function(zone)
	local _source = source

	if PlayersHarvesting[_source] == false then
		TriggerClientEvent('esx:showNotification', _source, '~r~C\'est pas bien de glitch ~w~')
		PlayersHarvesting[_source]=false
	else
		PlayersHarvesting[_source]=true
		TriggerClientEvent('esx:showNotification', _source, _U('viande_taken'))
		Harvest(_source,zone)
	end
end)


RegisterServerEvent('esx_boucherjob:stopHarvest')
AddEventHandler('esx_boucherjob:stopHarvest', function()
	local _source = source

	if PlayersHarvesting[_source] == true then
		PlayersHarvesting[_source]=false
		TriggerClientEvent('esx:showNotification', _source, 'Vous sortez de la ~r~zone')
	else
		TriggerClientEvent('esx:showNotification', _source, 'Vous pouvez ~g~récolter')
		PlayersHarvesting[_source]=true
	end
end)

RegisterServerEvent('esx_boucherjob:checkcount')
AddEventHandler('esx_boucherjob:checkcount', function()
	local _source = source
	local xPlayer  = ESX.GetPlayerFromId(source)
	local itemQuantity = xPlayer.getInventoryItem('viande').count
	if itemQuantity >= 51 then
		TriggerClientEvent('esx:showNotification', _source, '~r~vous avez plus de viande que la limite autorisé ! ~w~')
	end
end)

local function Transform(source, zone)

	if PlayersTransforming[source] == true then

		local xPlayer  = ESX.GetPlayerFromId(source)
		if zone == "CuisineSaucisson" then
			local itemQuantity = xPlayer.getInventoryItem('viande').count

			if itemQuantity <= 0 then
				TriggerClientEvent('esx:showNotification', source, _U('not_enough_viande'))
				return
			else
				local rand = math.random(0,100)
				if (rand >= 98) then
					SetTimeout(1800, function()
						xPlayer.removeInventoryItem('viande', 1)
						xPlayer.addInventoryItem('viande_de_qualité', 1)
						TriggerClientEvent('esx:showNotification', source, _U('viande_de_qualité'))
						Transform(source, zone)
					end)
				else
					SetTimeout(1800, function()
						xPlayer.removeInventoryItem('viande', 1)
						xPlayer.addInventoryItem('saucisson', 1)

						Transform(source, zone)
					end)
				end
			end
		elseif zone == "CuisineJambon" then
			local itemQuantity = xPlayer.getInventoryItem('viande').count
			if itemQuantity <= 0 then
				TriggerClientEvent('esx:showNotification', source, _U('not_enough_viande'))
				return
			else
				SetTimeout(1800, function()
					xPlayer.removeInventoryItem('viande', 1)
					xPlayer.addInventoryItem('jambon', 1)

					Transform(source, zone)
				end)
			end
		end
	end
end

RegisterServerEvent('esx_boucherjob:startTransform')
AddEventHandler('esx_boucherjob:startTransform', function(zone)
	local _source = source

	if PlayersTransforming[_source] == false then
		TriggerClientEvent('esx:showNotification', _source, '~r~C\'est pas bien de glitch ~w~')
		PlayersTransforming[_source]=false
	else
		PlayersTransforming[_source]=true
		TriggerClientEvent('esx:showNotification', _source, _U('transforming_in_progress'))
		Transform(_source,zone)
	end
end)

RegisterServerEvent('esx_boucherjob:stopTransform')
AddEventHandler('esx_boucherjob:stopTransform', function()

	local _source = source

	if PlayersTransforming[_source] == true then
		PlayersTransforming[_source]=false
		TriggerClientEvent('esx:showNotification', _source, 'Vous sortez de la ~r~zone')

	else
		TriggerClientEvent('esx:showNotification', _source, 'Vous pouvez ~g~transformer votre viande')
		PlayersTransforming[_source]=true

	end
end)

local function Sell(source, zone)

	if PlayersSelling[source] == true then
		local xPlayer  = ESX.GetPlayerFromId(source)

		if zone == 'SellFarm' then
			if xPlayer.getInventoryItem('saucisson').count <= 0 then
				saucisson = 0
			else
				saucisson = 1
			end

			if xPlayer.getInventoryItem('jambon').count <= 0 then
				jambon = 0
			else
				jambon = 1
			end

			if saucisson == 0 and jambon == 0 then
				TriggerClientEvent('esx:showNotification', source, _U('no_product_sale'))
				return
			elseif xPlayer.getInventoryItem('saucisson').count <= 0 and jambon == 0 then
				TriggerClientEvent('esx:showNotification', source, _U('no_vin_sale'))
				saucisson = 0
				return
			elseif xPlayer.getInventoryItem('jambon').count <= 0 and saucisson == 0then
				TriggerClientEvent('esx:showNotification', source, _U('no_jambon_sale'))
				jambon = 0
				return
			else
				if (jambon == 1) then
					SetTimeout(1100, function()
						local money = math.random(6,9)
						xPlayer.removeInventoryItem('jambon', 1)
						local societyAccount = nil

						TriggerEvent('esx_addonaccount:getSharedAccount', 'society_butcher', function(account)
							societyAccount = account
						end)
						if societyAccount ~= nil then
							xPlayer.addMoney(money)
							--societyAccount.addMoney(money)
							TriggerClientEvent('esx:showNotification', xPlayer.source, _U('comp_earned') .. money)
						end
						Sell(source,zone)
					end)
				elseif (saucisson == 1) then
					SetTimeout(1100, function()
						local money = math.random(6,9) -- player management money = math.random(60,70)
						xPlayer.removeInventoryItem('saucisson', 1)
						local societyAccount = nil

						TriggerEvent('esx_addonaccount:getSharedAccount', 'society_butcher', function(account)
							societyAccount = account
						end)
						if societyAccount ~= nil then
							xPlayer.addMoney(money)
							--societyAccount.addMoney(money)
							TriggerClientEvent('esx:showNotification', xPlayer.source, _U('comp_earned') .. money)
						end
						Sell(source,zone)
					end)
				end

			end
		end
	end
end

RegisterServerEvent('esx_boucherjob:startSell')
AddEventHandler('esx_boucherjob:startSell', function(zone)

	local _source = source

	if PlayersSelling[_source] == false then
		TriggerClientEvent('esx:showNotification', _source, '~r~C\'est pas bien de glitch ~w~')
		PlayersSelling[_source]=false
	else
		PlayersSelling[_source]=true
		TriggerClientEvent('esx:showNotification', _source, _U('sale_in_prog'))
		Sell(_source, zone)
	end

end)

RegisterServerEvent('esx_boucherjob:stopSell')
AddEventHandler('esx_boucherjob:stopSell', function()

	local _source = source

	if PlayersSelling[_source] == true then
		PlayersSelling[_source]=false
		TriggerClientEvent('esx:showNotification', _source, 'Vous sortez de la ~r~zone')

	else
		TriggerClientEvent('esx:showNotification', _source, 'Vous pouvez ~g~vendre')
		PlayersSelling[_source]=true
	end

end)

RegisterServerEvent('esx_boucherjob:getStockItem')
AddEventHandler('esx_boucherjob:getStockItem', function(itemName, count)

	local xPlayer = ESX.GetPlayerFromId(source)

	TriggerEvent('esx_addoninventory:getSharedInventory', 'society_butcher', function(inventory)

		local item = inventory.getItem(itemName)

		if item.count >= count then
			inventory.removeItem(itemName, count)
			xPlayer.addInventoryItem(itemName, count)
		else
			TriggerClientEvent('esx:showNotification', xPlayer.source, _U('quantity_invalid'))
		end

		TriggerClientEvent('esx:showNotification', xPlayer.source, _U('have_withdrawn') .. count .. ' ' .. item.label)

	end)

end)

ESX.RegisterServerCallback('esx_boucherjob:getStockItems', function(source, cb)

	TriggerEvent('esx_addoninventory:getSharedInventory', 'society_butcher', function(inventory)
		cb(inventory.items)
	end)

end)

RegisterServerEvent('esx_boucherjob:putStockItems')
AddEventHandler('esx_boucherjob:putStockItems', function(itemName, count)

	local xPlayer = ESX.GetPlayerFromId(source)

	TriggerEvent('esx_addoninventory:getSharedInventory', 'society_butcher', function(inventory)

		local item = inventory.getItem(itemName)

		if item.count >= 0 then
			xPlayer.removeInventoryItem(itemName, count)
			inventory.addItem(itemName, count)
		else
			TriggerClientEvent('esx:showNotification', xPlayer.source, _U('quantity_invalid'))
		end

		TriggerClientEvent('esx:showNotification', xPlayer.source, _U('added') .. count .. ' ' .. item.label)

	end)
end)

ESX.RegisterServerCallback('esx_boucherjob:getPlayerInventory', function(source, cb)

	local xPlayer    = ESX.GetPlayerFromId(source)
	local items      = xPlayer.inventory

	cb({
		items      = items
	})

end)


ESX.RegisterUsableItem('jambon', function(source)

	local xPlayer = ESX.GetPlayerFromId(source)

	xPlayer.removeInventoryItem('jambon', 1)

	TriggerClientEvent('esx_status:add', source, 'hunger', 250000)
	TriggerClientEvent('esx_basicneeds:onEat', source)
	TriggerClientEvent('esx:showNotification', source, _U('used_jambon'))

end)

ESX.RegisterUsableItem('viande_de_qualité', function(source)

	local xPlayer = ESX.GetPlayerFromId(source)

	xPlayer.removeInventoryItem('viande_de_qualité', 1)

	TriggerClientEvent('esx_status:add', source, 'hunger', 1000000)
	TriggerClientEvent('esx_basicneeds:onEat', source)
	TriggerClientEvent('esx:showNotification', source, _U('used_grand_cru'))

end)

ESX.RegisterUsableItem('saucisson', function(source)

	local xPlayer = ESX.GetPlayerFromId(source)

	xPlayer.removeInventoryItem('saucisson', 1)

	TriggerClientEvent('esx_status:add', source, 'hunger', 250000)
	TriggerClientEvent('esx_basicneeds:onEat', source)
	TriggerClientEvent('esx:showNotification', source, _U('used_saucisson'))

end)
