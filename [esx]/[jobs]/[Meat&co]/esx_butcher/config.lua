Config                            = {}
Config.DrawDistance               = 100.0
Config.MaxInService               = -1
Config.EnablePlayerManagement     = false
Config.EnableSocietyOwnedVehicles = false
Config.Locale                     = 'fr'

Config.Zones = {

	viandeFarm = {
		Pos   = {x = 959.58, y = -2125.45, z = 30.46},
		Size  = {x = 3.5, y = 3.5, z = 1.0},
		Color = {r = 136, g = 243, b = 216},
		Name  = "Récupération des morceaux de viande",
		Type  = 1
	},


	CuisineSaucisson = {
		Pos   = {x = -1619.73, y = -813.63, z = 9.08},
		Size  = {x = 3.5, y = 3.5, z = 1.0},
		Color = {r = 136, g = 243, b = 216},
		Name  = "Préparation du saucisson",
		Type  = 1
	},

	CuisineJambon = {
		Pos   = {x = -1590.17, y = -838.43, z = 8.97},
		Size  = {x = 3.5, y = 3.5, z = 1.0},
		Color = {r = 136, g = 243, b = 216},
		Name  = "Découpe du Jambon",
		Type  = 1
	},

	SellFarm = {
		Pos   = {x = 107.89, y = -1814.52, z = 25.55},
		Size  = {x = 1.5, y = 1.5, z = 1.0},
		Color = {r = 136, g = 243, b = 216},
		Name  = "Vente des produits",
		Type  = 1
	},

	boucherActions = {
		Pos   = {x = -1604.36, y = -832.33, z = 9.08},
		Size  = {x = 1.5, y = 1.5, z = 0.5},
		Color = {r = 255, g = 233, b = 2},
		Name  = "Point d'action",
		Type  = 0
	 },

	VehicleSpawner = {
		Pos   = {x = -1604.07, y = -850.99, z = 9.04},
		Size = {x = 1.5, y = 1.5, z = 0.5},
		Color = {r = 2, g = 53, b = 255},
		Name  = "Garage véhicule",
		Type  = 0
	},

	VehicleSpawnPoint = {
		Pos   = {x = -1596.98, y = -843.99, z = 9.50},
		Size  = {x = 1.5, y = 1.5, z = 1.0},
		Color = {r = 136, g = 243, b = 216},
		Name  = "Spawn point",
		Type  = -1
	},

	VehicleDeleter = {
		Pos   = {x = -1650.19, y = -827.89, z = 9.04},
		Size  = {x = 3.0, y = 3.0, z = 0.5},
		Color = {r = 2, g = 255, b = 65},
		Name  = "Ranger son véhicule",
		Type  = 0
	}

}
