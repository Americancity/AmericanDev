ESX                = nil





local PlayersSelling       = {}
local jagerbomb = 1
local golem = 1
local whiskycoca = 1
local vodkaenergy = 1
local vodkafruit = 1
local rhumfruit = 1
local teqpaf = 1
local rhumcoca = 1
local mojito = 1
local mixapero = 1
local metreshooter = 1
local jagercerbere = 1
local itemcount = 1

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

if Config.MaxInService ~= -1 then
  TriggerEvent('esx_service:activateService', 'unicorn', Config.MaxInService)
end

TriggerEvent('esx_phone:registerNumber', 'unicorn', _U('unicorn_customer'), true, true)
TriggerEvent('esx_society:registerSociety', 'unicorn', 'Unicorn', 'society_unicorn', 'society_unicorn', 'society_unicorn', {type = 'private'})



RegisterServerEvent('esx_unicornjob:getStockItem')
AddEventHandler('esx_unicornjob:getStockItem', function(itemName, count)

  local _source = source
  local xPlayer = ESX.GetPlayerFromId(_source)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_unicorn', function(inventory)

    local item = inventory.getItem(itemName)

    if item.count >= count then
      inventory.removeItem(itemName, count)
      xPlayer.addInventoryItem(itemName, count)
    else
      TriggerClientEvent('esx:showNotification', xPlayer.source, _U('quantity_invalid'))
    end

    TriggerClientEvent('esx:showNotification', xPlayer.source, _U('you_removed') .. count .. ' ' .. item.label)

  end)

end)

ESX.RegisterServerCallback('esx_unicornjob:getStockItems', function(source, cb)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_unicorn', function(inventory)
    cb(inventory.items)
  end)

end)

RegisterServerEvent('esx_unicornjob:putStockItems')
AddEventHandler('esx_unicornjob:putStockItems', function(itemName, count)

  local _source = source
  local xPlayer = ESX.GetPlayerFromId(_source)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_unicorn', function(inventory)

    local item = inventory.getItem(itemName)
    local playerItemCount = xPlayer.getInventoryItem(itemName).count

    if item.count >= 0 and count <= playerItemCount then
      xPlayer.removeInventoryItem(itemName, count)
      inventory.addItem(itemName, count)
    else
      TriggerClientEvent('esx:showNotification', xPlayer.source, _U('invalid_quantity'))
    end

    TriggerClientEvent('esx:showNotification', xPlayer.source, _U('you_added') .. count .. ' ' .. item.label)

  end)

end)


RegisterServerEvent('esx_unicornjob:getFridgeStockItem')
AddEventHandler('esx_unicornjob:getFridgeStockItem', function(itemName, count)

  local _source = source
  local xPlayer = ESX.GetPlayerFromId(_source)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_unicorn_fridge', function(inventory)

    local item = inventory.getItem(itemName)

    if item.count >= count then
      inventory.removeItem(itemName, count)
      xPlayer.addInventoryItem(itemName, count)
    else
      TriggerClientEvent('esx:showNotification', xPlayer.source, _U('quantity_invalid'))
    end

    TriggerClientEvent('esx:showNotification', xPlayer.source, _U('you_removed') .. count .. ' ' .. item.label)

  end)

end)

ESX.RegisterServerCallback('esx_unicornjob:getFridgeStockItems', function(source, cb)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_unicorn_fridge', function(inventory)
    cb(inventory.items)
  end)

end)

RegisterServerEvent('esx_unicornjob:putFridgeStockItems')
AddEventHandler('esx_unicornjob:putFridgeStockItems', function(itemName, count)

  local _source = source
  local xPlayer = ESX.GetPlayerFromId(_source)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_unicorn_fridge', function(inventory)

    local item = inventory.getItem(itemName)
    local playerItemCount = xPlayer.getInventoryItem(itemName).count

    if item.count >= 0 and count <= playerItemCount then
      xPlayer.removeInventoryItem(itemName, count)
      inventory.addItem(itemName, count)
    else
      TriggerClientEvent('esx:showNotification', xPlayer.source, _U('invalid_quantity'))
    end

    TriggerClientEvent('esx:showNotification', xPlayer.source, _U('you_added') .. count .. ' ' .. item.label)

  end)

end)


RegisterServerEvent('esx_unicornjob:buyItem')
AddEventHandler('esx_unicornjob:buyItem', function(itemName, price, itemLabel)

    local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)
    local limit = xPlayer.getInventoryItem(itemName).limit
    local qtty = xPlayer.getInventoryItem(itemName).count
    local societyAccount = nil

    TriggerEvent('esx_addonaccount:getSharedAccount', 'society_unicorn', function(account)
        societyAccount = account
      end)
    
    if societyAccount ~= nil and societyAccount.money >= price then
        if qtty < limit then
            societyAccount.removeMoney(price)
            xPlayer.addInventoryItem(itemName, 1)
            TriggerClientEvent('esx:showNotification', _source, _U('bought') .. itemLabel)
        else
            TriggerClientEvent('esx:showNotification', _source, _U('max_item'))
        end
    else
        TriggerClientEvent('esx:showNotification', _source, _U('not_enough'))
    end

end)


RegisterServerEvent('esx_unicornjob:craftingCoktails')
AddEventHandler('esx_unicornjob:craftingCoktails', function(itemValue)

    local _source = source
    local _itemValue = itemValue
    TriggerClientEvent('esx:showNotification', _source, _U('assembling_cocktail'))

    if _itemValue == 'jagerbomb' then        

            local xPlayer           = ESX.GetPlayerFromId(_source)

            local alephQuantity     = xPlayer.getInventoryItem('energy').count
            local bethQuantity      = xPlayer.getInventoryItem('jager').count
            if alephQuantity < 1 then
                TriggerClientEvent('esx:showNotification', _source, _U('not_enough') .. _U('energy') .. '~w~')
            elseif bethQuantity < 1 then
                TriggerClientEvent('esx:showNotification', _source, _U('not_enough') .. _U('jager') .. '~w~')
            else
              TriggerClientEvent('esx_unicornjob:startcraft', source)
              SetTimeout(10000, function()
                local chanceToMiss = math.random(100)
                if chanceToMiss <= Config.MissCraft then
                    TriggerClientEvent('esx:showNotification', _source, _U('craft_miss'))
                    xPlayer.removeInventoryItem('energy', 1)
                    xPlayer.removeInventoryItem('jager', 1)
                else
                    TriggerClientEvent('esx:showNotification', _source, _U('craft') .. _U('jagerbomb') .. ' ~w~!')
                    xPlayer.removeInventoryItem('energy', 1)
                    xPlayer.removeInventoryItem('jager', 1)
                    xPlayer.addInventoryItem('jagerbomb', 1)
                end
              end)
            end
    end

    if _itemValue == 'golem' then       

            local xPlayer           = ESX.GetPlayerFromId(_source)

            local alephQuantity     = xPlayer.getInventoryItem('limonade').count
            local bethQuantity      = xPlayer.getInventoryItem('vodka').count
            local gimelQuantity     = xPlayer.getInventoryItem('ice').count

            if alephQuantity < 2 then
                TriggerClientEvent('esx:showNotification', _source, _U('not_enough') .. _U('limonade') .. '~w~')
            elseif bethQuantity < 2 then
                TriggerClientEvent('esx:showNotification', _source, _U('not_enough') .. _U('vodka') .. '~w~')
            elseif gimelQuantity < 1 then
                TriggerClientEvent('esx:showNotification', _source, _U('not_enough') .. _U('ice') .. '~w~')
            else
              TriggerClientEvent('esx_unicornjob:startcraft', source)
              SetTimeout(10000, function()
                local chanceToMiss = math.random(100)
                if chanceToMiss <= Config.MissCraft then
                    TriggerClientEvent('esx:showNotification', _source, _U('craft_miss'))
                    xPlayer.removeInventoryItem('limonade', 1)
                    xPlayer.removeInventoryItem('vodka', 1)
                    xPlayer.removeInventoryItem('ice', 1)
                else
                    TriggerClientEvent('esx:showNotification', _source, _U('craft') .. _U('golem') .. ' ~w~!')
                    xPlayer.removeInventoryItem('limonade', 1)
                    xPlayer.removeInventoryItem('vodka', 1)
                    xPlayer.removeInventoryItem('ice', 1)
                    xPlayer.addInventoryItem('golem', 1)
                end
              end)
            end
    end
    
    if _itemValue == 'whiskycoca' then      

            local xPlayer           = ESX.GetPlayerFromId(_source)

            local alephQuantity     = xPlayer.getInventoryItem('soda').count
            local bethQuantity      = xPlayer.getInventoryItem('whisky').count

            if alephQuantity < 1 then
                TriggerClientEvent('esx:showNotification', _source, _U('not_enough') .. _U('soda') .. '~w~')
            elseif bethQuantity < 1 then
                TriggerClientEvent('esx:showNotification', _source, _U('not_enough') .. _U('whisky') .. '~w~')
            else
              TriggerClientEvent('esx_unicornjob:startcraft', source)
              SetTimeout(10000, function()
                local chanceToMiss = math.random(100)
                if chanceToMiss <= Config.MissCraft then
                    TriggerClientEvent('esx:showNotification', _source, _U('craft_miss'))
                    xPlayer.removeInventoryItem('soda', 1)
                    xPlayer.removeInventoryItem('whisky', 1)
                else
                    TriggerClientEvent('esx:showNotification', _source, _U('craft') .. _U('whiskycoca') .. ' ~w~!')
                    xPlayer.removeInventoryItem('soda', 1)
                    xPlayer.removeInventoryItem('whisky', 1)
                    xPlayer.addInventoryItem('whiskycoca', 1)
                end
              end)
            end
    end

    if _itemValue == 'rhumcoca' then        

            local xPlayer           = ESX.GetPlayerFromId(_source)

            local alephQuantity     = xPlayer.getInventoryItem('soda').count
            local bethQuantity      = xPlayer.getInventoryItem('rhum').count

            if alephQuantity < 1 then
                TriggerClientEvent('esx:showNotification', _source, _U('not_enough') .. _U('soda') .. '~w~')
            elseif bethQuantity < 1 then
                TriggerClientEvent('esx:showNotification', _source, _U('not_enough') .. _U('rhum') .. '~w~')
            else
              TriggerClientEvent('esx_unicornjob:startcraft', source)
              SetTimeout(10000, function()
                local chanceToMiss = math.random(100)
                if chanceToMiss <= Config.MissCraft then
                    TriggerClientEvent('esx:showNotification', _source, _U('craft_miss'))
                    xPlayer.removeInventoryItem('soda', 1)
                    xPlayer.removeInventoryItem('rhum', 1)
                else
                    TriggerClientEvent('esx:showNotification', _source, _U('craft') .. _U('rhumcoca') .. ' ~w~!')
                    xPlayer.removeInventoryItem('soda', 1)
                    xPlayer.removeInventoryItem('rhum', 1)
                    xPlayer.addInventoryItem('rhumcoca', 1)
                end
              end)
            end
    end

    if _itemValue == 'vodkaenergy' then     

            local xPlayer           = ESX.GetPlayerFromId(_source)

            local alephQuantity     = xPlayer.getInventoryItem('energy').count
            local bethQuantity      = xPlayer.getInventoryItem('vodka').count
            local gimelQuantity     = xPlayer.getInventoryItem('ice').count

            if alephQuantity < 1 then
                TriggerClientEvent('esx:showNotification', _source, _U('not_enough') .. _U('energy') .. '~w~')
            elseif bethQuantity < 1 then
                TriggerClientEvent('esx:showNotification', _source, _U('not_enough') .. _U('vodka') .. '~w~')
            elseif gimelQuantity < 1 then
                TriggerClientEvent('esx:showNotification', _source, _U('not_enough') .. _U('ice') .. '~w~')
            else
              TriggerClientEvent('esx_unicornjob:startcraft', source)
              SetTimeout(10000, function()
                local chanceToMiss = math.random(100)
                if chanceToMiss <= Config.MissCraft then
                    TriggerClientEvent('esx:showNotification', _source, _U('craft_miss'))
                    xPlayer.removeInventoryItem('energy', 1)
                    xPlayer.removeInventoryItem('vodka', 1)
                    xPlayer.removeInventoryItem('ice', 1)
                else
                    TriggerClientEvent('esx:showNotification', _source, _U('craft') .. _U('vodkaenergy') .. ' ~w~!')
                    xPlayer.removeInventoryItem('energy', 1)
                    xPlayer.removeInventoryItem('vodka', 1)
                    xPlayer.removeInventoryItem('ice', 1)
                    xPlayer.addInventoryItem('vodkaenergy', 1)
                end
              end)
            end
    end

    if _itemValue == 'vodkafruit' then
            local xPlayer           = ESX.GetPlayerFromId(_source)

            local alephQuantity     = xPlayer.getInventoryItem('jusfruit').count
            local bethQuantity      = xPlayer.getInventoryItem('vodka').count
            local gimelQuantity     = xPlayer.getInventoryItem('ice').count

            if alephQuantity < 1 then
                TriggerClientEvent('esx:showNotification', _source, _U('not_enough') .. _U('jusfruit') .. '~w~')
            elseif bethQuantity < 1 then
                TriggerClientEvent('esx:showNotification', _source, _U('not_enough') .. _U('vodka') .. '~w~')
            elseif gimelQuantity < 1 then
                TriggerClientEvent('esx:showNotification', _source, _U('not_enough') .. _U('ice') .. '~w~')
            else
              TriggerClientEvent('esx_unicornjob:startcraft', source)
              SetTimeout(10000, function()
                local chanceToMiss = math.random(100)
                if chanceToMiss <= Config.MissCraft then
                    TriggerClientEvent('esx:showNotification', _source, _U('craft_miss'))
                    xPlayer.removeInventoryItem('jusfruit', 1)
                    xPlayer.removeInventoryItem('vodka', 1)
                    xPlayer.removeInventoryItem('ice', 1)
                else
                    TriggerClientEvent('esx:showNotification', _source, _U('craft') .. _U('vodkafruit') .. ' ~w~!')
                    xPlayer.removeInventoryItem('jusfruit', 1)
                    xPlayer.removeInventoryItem('vodka', 1)
                    xPlayer.removeInventoryItem('ice', 1)
                    xPlayer.addInventoryItem('vodkafruit', 1) 
                end
              end)
            end
    end

    if _itemValue == 'rhumfruit' then
            local xPlayer           = ESX.GetPlayerFromId(_source)

            local alephQuantity     = xPlayer.getInventoryItem('jusfruit').count
            local bethQuantity      = xPlayer.getInventoryItem('rhum').count
            local gimelQuantity     = xPlayer.getInventoryItem('ice').count

            if alephQuantity < 1 then
                TriggerClientEvent('esx:showNotification', _source, _U('not_enough') .. _U('jusfruit') .. '~w~')
            elseif bethQuantity < 1 then
                TriggerClientEvent('esx:showNotification', _source, _U('not_enough') .. _U('rhum') .. '~w~')
            elseif gimelQuantity < 1 then
                TriggerClientEvent('esx:showNotification', _source, _U('not_enough') .. _U('ice') .. '~w~')
            else
              TriggerClientEvent('esx_unicornjob:startcraft', source)
              SetTimeout(10000, function()
                local chanceToMiss = math.random(100)
                if chanceToMiss <= Config.MissCraft then
                    TriggerClientEvent('esx:showNotification', _source, _U('craft_miss'))
                    xPlayer.removeInventoryItem('jusfruit', 1)
                    xPlayer.removeInventoryItem('rhum', 1)
                    xPlayer.removeInventoryItem('ice', 1)
                else
                    TriggerClientEvent('esx:showNotification', _source, _U('craft') .. _U('rhumfruit') .. ' ~w~!')
                    xPlayer.removeInventoryItem('jusfruit', 1)
                    xPlayer.removeInventoryItem('rhum', 1)
                    xPlayer.removeInventoryItem('ice', 1)
                    xPlayer.addInventoryItem('rhumfruit', 1)
                end
              end)
            end
    end

    if _itemValue == 'teqpaf' then       

            local xPlayer           = ESX.GetPlayerFromId(_source)

            local alephQuantity     = xPlayer.getInventoryItem('limonade').count
            local bethQuantity      = xPlayer.getInventoryItem('tequila').count

            if alephQuantity < 1 then
                TriggerClientEvent('esx:showNotification', _source, _U('not_enough') .. _U('limonade') .. '~w~')
            elseif bethQuantity < 1 then
                TriggerClientEvent('esx:showNotification', _source, _U('not_enough') .. _U('tequila') .. '~w~')
            else
              TriggerClientEvent('esx_unicornjob:startcraft', source)
              SetTimeout(10000, function()
                local chanceToMiss = math.random(100)
                if chanceToMiss <= Config.MissCraft then
                    TriggerClientEvent('esx:showNotification', _source, _U('craft_miss'))
                    xPlayer.removeInventoryItem('limonade', 1)
                    xPlayer.removeInventoryItem('tequila', 1)
                else
                    TriggerClientEvent('esx:showNotification', _source, _U('craft') .. _U('teqpaf') .. ' ~w~!')
                    xPlayer.removeInventoryItem('limonade', 1)
                    xPlayer.removeInventoryItem('tequila', 1)
                    xPlayer.addInventoryItem('teqpaf', 1)
                end
              end)
            end
    end

    if _itemValue == 'mojito' then       

            local xPlayer           = ESX.GetPlayerFromId(_source)

            local alephQuantity     = xPlayer.getInventoryItem('rhum').count
            local bethQuantity      = xPlayer.getInventoryItem('limonade').count
            local gimelQuantity     = xPlayer.getInventoryItem('menthe').count
            local daletQuantity      = xPlayer.getInventoryItem('ice').count

            if alephQuantity < 1 then
                TriggerClientEvent('esx:showNotification', _source, _U('not_enough') .. _U('rhum') .. '~w~')
            elseif bethQuantity < 1 then
                TriggerClientEvent('esx:showNotification', _source, _U('not_enough') .. _U('limonade') .. '~w~')
            elseif gimelQuantity < 1 then
                TriggerClientEvent('esx:showNotification', _source, _U('not_enough') .. _U('menthe') .. '~w~')
            elseif daletQuantity < 1 then
                TriggerClientEvent('esx:showNotification', _source, _U('not_enough') .. _U('ice') .. '~w~')
            else
              TriggerClientEvent('esx_unicornjob:startcraft', source)
              SetTimeout(10000, function()
                local chanceToMiss = math.random(100)
                if chanceToMiss <= Config.MissCraft then
                    TriggerClientEvent('esx:showNotification', _source, _U('craft_miss'))
                    xPlayer.removeInventoryItem('rhum', 1)
                    xPlayer.removeInventoryItem('limonade', 1)
                    xPlayer.removeInventoryItem('menthe', 1)
                    xPlayer.removeInventoryItem('ice', 1)
                else
                    TriggerClientEvent('esx:showNotification', _source, _U('craft') .. _U('mojito') .. ' ~w~!')
                    xPlayer.removeInventoryItem('rhum', 1)
                    xPlayer.removeInventoryItem('limonade', 1)
                    xPlayer.removeInventoryItem('menthe', 1)
                    xPlayer.removeInventoryItem('ice', 1)
                    xPlayer.addInventoryItem('mojito', 1)
                end
              end)
            end
    end

    if _itemValue == 'mixapero' then     

            local xPlayer           = ESX.GetPlayerFromId(_source)

            local alephQuantity     = xPlayer.getInventoryItem('bolcacahuetes').count
            local bethQuantity      = xPlayer.getInventoryItem('bolnoixcajou').count
            local gimelQuantity     = xPlayer.getInventoryItem('bolpistache').count
            local daletQuantity     = xPlayer.getInventoryItem('bolchips').count

            if alephQuantity < 1 then
                TriggerClientEvent('esx:showNotification', _source, _U('not_enough') .. _U('bolcacahuetes') .. '~w~')
            elseif bethQuantity < 1 then
                TriggerClientEvent('esx:showNotification', _source, _U('not_enough') .. _U('bolnoixcajou') .. '~w~')
            elseif gimelQuantity < 1 then
                TriggerClientEvent('esx:showNotification', _source, _U('not_enough') .. _U('bolpistache') .. '~w~')
            elseif daletQuantity < 1 then
                TriggerClientEvent('esx:showNotification', _source, _U('not_enough') .. _U('bolchips') .. '~w~')
            else
              TriggerClientEvent('esx_unicornjob:startcraft', source)
              SetTimeout(10000, function()
                local chanceToMiss = math.random(100)
                if chanceToMiss <= Config.MissCraft then
                    TriggerClientEvent('esx:showNotification', _source, _U('craft_miss'))
                    xPlayer.removeInventoryItem('bolcacahuetes', 1)
                    xPlayer.removeInventoryItem('bolnoixcajou', 1)
                    xPlayer.removeInventoryItem('bolpistache', 1)
                    xPlayer.removeInventoryItem('bolchips', 1)
                else
                    TriggerClientEvent('esx:showNotification', _source, _U('craft') .. _U('mixapero') .. ' ~w~!')
                    xPlayer.removeInventoryItem('bolcacahuetes', 1)
                    xPlayer.removeInventoryItem('bolnoixcajou', 1)
                    xPlayer.removeInventoryItem('bolpistache', 1)
                    xPlayer.removeInventoryItem('bolchips', 1)
                    xPlayer.addInventoryItem('mixapero', 1)
                end
              end)
            end
    end

    if _itemValue == 'metreshooter' then       

            local xPlayer           = ESX.GetPlayerFromId(_source)

            local alephQuantity     = xPlayer.getInventoryItem('jager').count
            local bethQuantity      = xPlayer.getInventoryItem('vodka').count
            local gimelQuantity     = xPlayer.getInventoryItem('whisky').count
            local daletQuantity     = xPlayer.getInventoryItem('tequila').count

            if alephQuantity < 1 then
                TriggerClientEvent('esx:showNotification', _source, _U('not_enough') .. _U('jager') .. '~w~')
            elseif bethQuantity < 1 then
                TriggerClientEvent('esx:showNotification', _source, _U('not_enough') .. _U('vodka') .. '~w~')
            elseif gimelQuantity < 1 then
                TriggerClientEvent('esx:showNotification', _source, _U('not_enough') .. _U('whisky') .. '~w~')
            elseif daletQuantity < 1 then
                TriggerClientEvent('esx:showNotification', _source, _U('not_enough') .. _U('tequila') .. '~w~')
            else
              TriggerClientEvent('esx_unicornjob:startcraft', source)
              SetTimeout(10000, function()
                local chanceToMiss = math.random(100)
                if chanceToMiss <= Config.MissCraft then
                    TriggerClientEvent('esx:showNotification', _source, _U('craft_miss'))
                    xPlayer.removeInventoryItem('jager', 1)
                    xPlayer.removeInventoryItem('vodka', 1)
                    xPlayer.removeInventoryItem('whisky', 1)
                    xPlayer.removeInventoryItem('tequila', 1)
                else
                    TriggerClientEvent('esx:showNotification', _source, _U('craft') .. _U('metreshooter') .. ' ~w~!')
                    xPlayer.removeInventoryItem('jager', 1)
                    xPlayer.removeInventoryItem('vodka', 1)
                    xPlayer.removeInventoryItem('whisky', 1)
                    xPlayer.removeInventoryItem('tequila', 1)
                    xPlayer.addInventoryItem('metreshooter', 1)
                end
              end)
            end
    end

    if _itemValue == 'jagercerbere' then       

            local xPlayer           = ESX.GetPlayerFromId(_source)

            local alephQuantity     = xPlayer.getInventoryItem('jagerbomb').count
            local bethQuantity      = xPlayer.getInventoryItem('vodka').count
            local gimelQuantity     = xPlayer.getInventoryItem('tequila').count

            if alephQuantity < 1 then
                TriggerClientEvent('esx:showNotification', _source, _U('not_enough') .. _U('jagerbomb') .. '~w~')
            elseif bethQuantity < 1 then
                TriggerClientEvent('esx:showNotification', _source, _U('not_enough') .. _U('vodka') .. '~w~')
            elseif gimelQuantity < 1 then
                TriggerClientEvent('esx:showNotification', _source, _U('not_enough') .. _U('tequila') .. '~w~')
            else
              TriggerClientEvent('esx_unicornjob:startcraft', source)
              SetTimeout(10000, function()
                local chanceToMiss = math.random(100)
                if chanceToMiss <= Config.MissCraft then
                    TriggerClientEvent('esx:showNotification', _source, _U('craft_miss'))
                    xPlayer.removeInventoryItem('jagerbomb', 1)
                    xPlayer.removeInventoryItem('vodka', 1)
                    xPlayer.removeInventoryItem('tequila', 1)
                else
                    TriggerClientEvent('esx:showNotification', _source, _U('craft') .. _U('jagercerbere') .. ' ~w~!')
                    xPlayer.removeInventoryItem('jagerbomb', 1)
                    xPlayer.removeInventoryItem('vodka', 1)
                    xPlayer.removeInventoryItem('tequila', 1)
                    xPlayer.addInventoryItem('jagercerbere', 1)
                end
              end) 
            end
    end

end)


ESX.RegisterServerCallback('esx_unicornjob:getVaultWeapons', function(source, cb)

  TriggerEvent('esx_datastore:getSharedDataStore', 'society_unicorn', function(store)

    local weapons = store.get('weapons')

    if weapons == nil then
      weapons = {}
    end

    cb(weapons)

  end)

end)

ESX.RegisterServerCallback('esx_unicornjob:addVaultWeapon', function(source, cb, weaponName)

  local _source = source
  local xPlayer = ESX.GetPlayerFromId(_source)

  xPlayer.removeWeapon(weaponName)

  TriggerEvent('esx_datastore:getSharedDataStore', 'society_unicorn', function(store)

    local weapons = store.get('weapons')

    if weapons == nil then
      weapons = {}
    end

    local foundWeapon = false

    for i=1, #weapons, 1 do
      if weapons[i].name == weaponName then
        weapons[i].count = weapons[i].count + 1
        foundWeapon = true
      end
    end

    if not foundWeapon then
      table.insert(weapons, {
        name  = weaponName,
        count = 1
      })
    end

     store.set('weapons', weapons)

     cb()

  end)

end)

ESX.RegisterServerCallback('esx_unicornjob:removeVaultWeapon', function(source, cb, weaponName)

  local _source = source
  local xPlayer = ESX.GetPlayerFromId(_source)

  xPlayer.addWeapon(weaponName, 1000)

  TriggerEvent('esx_datastore:getSharedDataStore', 'society_unicorn', function(store)

    local weapons = store.get('weapons')

    if weapons == nil then
      weapons = {}
    end

    local foundWeapon = false

    for i=1, #weapons, 1 do
      if weapons[i].name == weaponName then
        weapons[i].count = (weapons[i].count > 0 and weapons[i].count - 1 or 0)
        foundWeapon = true
      end
    end

    if not foundWeapon then
      table.insert(weapons, {
        name  = weaponName,
        count = 0
      })
    end

     store.set('weapons', weapons)

     cb()

  end)

end)

ESX.RegisterServerCallback('esx_unicornjob:getPlayerInventory', function(source, cb)

  local _source = source
  local xPlayer = ESX.GetPlayerFromId(_source)
  local items      = xPlayer.inventory

  cb({
    items      = items
  })

end)

function Sell(source, zone)
    if PlayersSelling[source] == true then
        local xPlayer  = ESX.GetPlayerFromId(source)
        
            if xPlayer.getInventoryItem('jagerbomb').count <= 0 then
                jagerbomb = 0
            else
                jagerbomb = 1
            end
            if xPlayer.getInventoryItem('golem').count <= 0 then
                golem = 0
            else
                golem = 1
            end
            if xPlayer.getInventoryItem('whiskycoca').count <= 0 then
                whiskycoca = 0
            else
                whiskycoca = 1
            end
            if xPlayer.getInventoryItem('vodkaenergy').count <= 0 then
                vodkaenergy = 0
            else
                vodkaenergy = 1
            end
            if xPlayer.getInventoryItem('vodkafruit').count <= 0 then
                vodkafruit = 0
            else
                vodkafruit = 1
            end    
            if xPlayer.getInventoryItem('rhumfruit').count <= 0 then
                rhumfruit = 0
            else
                rhumfruit = 1
            end   
            if xPlayer.getInventoryItem('teqpaf').count <= 0 then
                teqpaf = 0
            else
                teqpaf = 1
            end 
            if xPlayer.getInventoryItem('rhumcoca').count <= 0 then
                rhumcoca = 0
            else
                rhumcoca = 1
            end 
            if xPlayer.getInventoryItem('mojito').count <= 0 then
                mojito = 0
            else
                mojito = 1
            end 
            if xPlayer.getInventoryItem('mixapero').count <= 0 then
                mixapero = 0
            else
                mixapero = 1
            end 
            if xPlayer.getInventoryItem('metreshooter').count <= 0 then
                metreshooter = 0
            else
                metreshooter = 1
            end 
            if xPlayer.getInventoryItem('jagercerbere').count <= 0 then
                jagercerbere = 0
            else
                jagercerbere = 1
            end 
            
            itemcount = jagercerbere+metreshooter+mixapero+mojito+rhumcoca+teqpaf+rhumfruit+vodkafruit+vodkaenergy+whiskycoca+golem+jagerbomb

            if itemcount <= 0 then
                TriggerClientEvent('esx:showNotification', source, ('aucun produits a vendre'))
                return
            elseif (jagerbomb == 1) then
                    SetTimeout(1100, function()
                        local money = math.random(150,200) --player management 
                        xPlayer.removeInventoryItem('jagerbomb', 1)
                        local societyAccount = nil
                        TriggerEvent('esx_addonaccount:getSharedAccount', 'society_unicorn', function(account)
                            societyAccount = account
                        end)
                        if societyAccount ~= nil then
                            societyAccount.addMoney(money)
                            TriggerClientEvent('esx:showNotification', xPlayer.source, 'votre sociétée a gagner ' .. money)
                        end
                        Sell(source,zone)
                    end)
            elseif (golem == 1) then
                    SetTimeout(1100, function()
                     local money = math.random(225,275)
                        xPlayer.removeInventoryItem('golem', 1)
                        local societyAccount = nil

                        TriggerEvent('esx_addonaccount:getSharedAccount', 'society_unicorn', function(account)
                            societyAccount = account
                        end)
                        if societyAccount ~= nil then
                            societyAccount.addMoney(money)
                            TriggerClientEvent('esx:showNotification', xPlayer.source, 'votre sociétée a gagner ' .. money)
                        end
                        Sell(source,zone)
                    end)
            elseif (whiskycoca == 1) then
                    SetTimeout(1100, function()
                            local money = math.random(150,200)
                            xPlayer.removeInventoryItem('whiskycoca', 1)
                            local societyAccount = nil
                            TriggerEvent('esx_addonaccount:getSharedAccount', 'society_unicorn', function(account)
                                societyAccount = account
                            end)
                            if societyAccount ~= nil then
                                societyAccount.addMoney(money)
                                TriggerClientEvent('esx:showNotification', xPlayer.source, 'votre sociétée a gagner ' .. money)
                            end
                            Sell(source,zone)
                    end)
            elseif (whiskycoca == 1) then
                    SetTimeout(1100, function()
                            local money = math.random(150,200)
                            xPlayer.removeInventoryItem('whiskycoca', 1)
                            local societyAccount = nil
                            TriggerEvent('esx_addonaccount:getSharedAccount', 'society_unicorn', function(account)
                                societyAccount = account
                            end)
                            if societyAccount ~= nil then
                                societyAccount.addMoney(money)
                                TriggerClientEvent('esx:showNotification', xPlayer.source, 'votre sociétée a gagner ' .. money)
                            end
                            Sell(source,zone)
                    end)
            elseif (rhumcoca == 1) then
                    SetTimeout(1100, function()
                            local money = math.random(150,200)
                            xPlayer.removeInventoryItem('rhumcoca', 1)
                            local societyAccount = nil
                            TriggerEvent('esx_addonaccount:getSharedAccount', 'society_unicorn', function(account)
                                societyAccount = account
                            end)
                            if societyAccount ~= nil then
                                societyAccount.addMoney(money)
                                TriggerClientEvent('esx:showNotification', xPlayer.source, 'votre sociétée a gagner ' .. money)
                            end
                            Sell(source,zone)
                    end)
            elseif (vodkaenergy == 1) then
                    SetTimeout(1100, function()
                            local money = math.random(225,275)
                            xPlayer.removeInventoryItem('vodkaenergy', 1)
                            local societyAccount = nil
                            TriggerEvent('esx_addonaccount:getSharedAccount', 'society_unicorn', function(account)
                                societyAccount = account
                            end)
                            if societyAccount ~= nil then
                                societyAccount.addMoney(money)
                                TriggerClientEvent('esx:showNotification', xPlayer.source, 'votre sociétée a gagner ' .. money)
                            end
                            Sell(source,zone)
                    end)
            elseif (vodkafruit == 1) then
                    SetTimeout(1100, function()
                            local money = math.random(225,275)
                            xPlayer.removeInventoryItem('vodkafruit', 1)
                            local societyAccount = nil
                            TriggerEvent('esx_addonaccount:getSharedAccount', 'society_unicorn', function(account)
                                societyAccount = account
                            end)
                            if societyAccount ~= nil then
                                societyAccount.addMoney(money)
                                TriggerClientEvent('esx:showNotification', xPlayer.source, 'votre sociétée a gagner ' .. money)
                            end
                            Sell(source,zone)
                    end)
            elseif (rhumfruit == 1) then
                    SetTimeout(1100, function()
                            local money = math.random(225,275)
                            xPlayer.removeInventoryItem('rhumfruit', 1)
                            local societyAccount = nil
                            TriggerEvent('esx_addonaccount:getSharedAccount', 'society_unicorn', function(account)
                                societyAccount = account
                            end)
                            if societyAccount ~= nil then
                                societyAccount.addMoney(money)
                                TriggerClientEvent('esx:showNotification', xPlayer.source, 'votre sociétée a gagner ' .. money)
                            end
                            Sell(source,zone)
                    end)
            elseif (whiskycoca == 1) then
                    SetTimeout(1100, function()
                            local money = math.random(150,275)
                            xPlayer.removeInventoryItem('whiskycoca', 1)
                            local societyAccount = nil
                            TriggerEvent('esx_addonaccount:getSharedAccount', 'society_unicorn', function(account)
                                societyAccount = account
                            end)
                            if societyAccount ~= nil then
                                societyAccount.addMoney(money)
                                TriggerClientEvent('esx:showNotification', xPlayer.source, 'votre sociétée a gagner ' .. money)
                            end
                            Sell(source,zone)
                    end)
            elseif (teqpaf == 1) then
                    SetTimeout(1100, function()
                            local money = math.random(150,175)
                            xPlayer.removeInventoryItem('teqpaf', 1)
                            local societyAccount = nil
                            TriggerEvent('esx_addonaccount:getSharedAccount', 'society_unicorn', function(account)
                                societyAccount = account
                            end)
                            if societyAccount ~= nil then
                                societyAccount.addMoney(money)
                                TriggerClientEvent('esx:showNotification', xPlayer.source, 'votre sociétée a gagner ' .. money)
                            end
                            Sell(source,zone)
                    end)
            elseif (mojito == 1) then
                    SetTimeout(1100, function()
                            local money = math.random(300,325)
                            xPlayer.removeInventoryItem('mojito', 1)
                            local societyAccount = nil
                            TriggerEvent('esx_addonaccount:getSharedAccount', 'society_unicorn', function(account)
                                societyAccount = account
                            end)
                            if societyAccount ~= nil then
                                societyAccount.addMoney(money)
                                TriggerClientEvent('esx:showNotification', xPlayer.source, 'votre sociétée a gagner ' .. money)
                            end
                            Sell(source,zone)
                    end)
            elseif (mixapero == 1) then
                    SetTimeout(1100, function()
                            local money = math.random(275,300)
                            xPlayer.removeInventoryItem('mixapero', 1)
                            local societyAccount = nil
                            TriggerEvent('esx_addonaccount:getSharedAccount', 'society_unicorn', function(account)
                                societyAccount = account
                            end)
                            if societyAccount ~= nil then
                                societyAccount.addMoney(money)
                                TriggerClientEvent('esx:showNotification', xPlayer.source, 'votre sociétée a gagner ' .. money)
                            end
                            Sell(source,zone)
                    end)
            elseif (metreshooter == 1) then
                    SetTimeout(1100, function()
                            local money = math.random(300,325)
                            xPlayer.removeInventoryItem('metreshooter', 1)
                            local societyAccount = nil
                            TriggerEvent('esx_addonaccount:getSharedAccount', 'society_unicorn', function(account)
                                societyAccount = account
                            end)
                            if societyAccount ~= nil then
                                societyAccount.addMoney(money)
                                TriggerClientEvent('esx:showNotification', xPlayer.source, 'votre sociétée a gagner ' .. money)
                            end
                            Sell(source,zone)
                    end)
            elseif (jagercerbere == 1) then
                    SetTimeout(1100, function()
                            local money = math.random(225,250)
                            xPlayer.removeInventoryItem('jagercerbere', 1)
                            local societyAccount = nil
                            TriggerEvent('esx_addonaccount:getSharedAccount', 'society_unicorn', function(account)
                                societyAccount = account
                            end)
                            if societyAccount ~= nil then
                                societyAccount.addMoney(money)
                                TriggerClientEvent('esx:showNotification', xPlayer.source, 'votre sociétée a gagner ' .. money)
                            end
                            Sell(source,zone)
                    end)
            end
        
    end
end    

RegisterServerEvent('esx_unicornjob:startSell')
AddEventHandler('esx_unicornjob:startSell', function(zone)

    local _source = source

    PlayersSelling[_source]=true
    TriggerClientEvent('esx:showNotification', _source, 'vente en cours ...')
    Sell(_source, zone)
end)
RegisterServerEvent('esx_unicornjob:stopSell')
AddEventHandler('esx_unicornjob:stopSell', function()

    local _source = source
    PlayersSelling[_source] = false

end)