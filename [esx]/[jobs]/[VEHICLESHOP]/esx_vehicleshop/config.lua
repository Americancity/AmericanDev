Config                            = {}
Config.DrawDistance               = 100.0
Config.MarkerColor                = { r = 120, g = 120, b = 240 }
Config.EnablePlayerManagement     = true -- enables the actual car dealer job. You'll need esx_addonaccount, esx_billing and esx_society
Config.EnableOwnedVehicles        = true
Config.EnableSocietyOwnedVehicles = false -- use with EnablePlayerManagement disabled, or else it wont have any effects
Config.ResellPercentage           = 50

Config.Locale                     = 'fr'

Config.LicenseEnable = false -- require people to own drivers license when buying vehicles? Only applies if EnablePlayerManagement is disabled. Requires esx_license

-- looks like this: 'LLL NNN'
-- The maximum plate length is 8 chars (including spaces & symbols), don't go past it!
Config.PlateLetters  = 4
Config.PlateNumbers  = 4
Config.PlateUseSpace = false

Config.Zones = {

	ShopEntering = {
		Pos   = { x = -55.58, y = -1097.98, z = 25.422 },
		Size  = { x = 1.5, y = 1.5, z = 1.0 },
		Type  = 1
	},

	ShopInside = {
		Pos     = { x = -15.84, y = -1102.99, z = 26.36 },
		Size    = { x = 1.5, y = 1.5, z = 1.0 },
		Heading = 160.26,
		Type    = -1
	},

	ShopOutside = {
		Pos     = { x = -14.68, y = -1099.73, z = 26.36 },
		Size    = { x = 1.5, y = 1.5, z = 1.0 },
		Heading = 160.26,
		Type    = -1
	},


	farmlp = {
		Pos     = { x = 217.48, y = -451.28, z = 43.35 },
		Size    = { x = 1.5, y = 1.5, z = 1.0 },
		Types = 1,
		Type    = 23
	},
	transformit = {
		Pos     = { x = -28.12, y = -1103.85, z = 25.43 },
		Size    = { x = 1.5, y = 1.5, z = 1.0 },
		Types = 1,
		Type    = 23
	},	
	farmvehdoc = {
		Pos     = { x = 222.73, y = -453.27, z = 43.35 },
		Size    = { x = 1.5, y = 1.5, z = 1.0 },
		Types = 1,
		Type    = 23
	},

	BossActions = {
		Pos   = { x = -32.065, y = -1114.277, z = 25.422 },
		Size  = { x = 1.5, y = 1.5, z = 1.0 },
		Type  = -1
	},

	GiveBackVehicle = {
		Pos   = { x = -18.227, y = -1078.558, z = 25.675 },
		Size  = { x = 3.0, y = 3.0, z = 1.0 },
		Type  = (Config.EnablePlayerManagement and 1 or -1)
	},

	ResellVehicle = {
		Pos   = { x = -44.630, y = -1080.738, z = 25.683 },
		Size  = { x = 3.0, y = 3.0, z = 1.0 },
		Type  = 1
	}

}
