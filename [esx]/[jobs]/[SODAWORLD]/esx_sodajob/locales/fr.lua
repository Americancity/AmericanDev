-----------------------------------------
-- Created and modify by L'ile Légale RP
-- SenSi and Kaminosekai
-----------------------------------------
Locales['fr'] = {
	-- General
	['cloakroom'] = 'Vestiaire',
	['ecola_clothes_civil'] = 'Tenue Civil',
	['ecola_clothes_ecola'] = 'Tenue de travail',
	['veh_menu'] = 'Garage',
	['spawn_veh'] = 'appuyez sur ~INPUT_CONTEXT~ pour sortir véhicule',
	['amount_invalid'] = 'montant invalide',
	['press_to_open'] = 'appuyez sur ~INPUT_CONTEXT~ pour accéder au menu',
	['billing'] = 'facuration',
	['invoice_amount'] = 'montant de la facture',
	['no_players_near'] = 'aucun joueur à proximité',
	['store_veh'] = 'appuyez sur ~INPUT_CONTEXT~ pour ranger le véhicule',
	['comp_earned'] = 'vous avez gagné ~g~$',
	['deposit_stock'] = 'Déposer Stock',
	['take_stock'] = 'Prendre Stock',
	['boss_actions'] = 'action Patron',
	['quantity'] = 'Quantité',
	['quantity_invalid'] = '~r~Quantité invalide~w~',
	['inventory'] = 'Inventaire',
	['have_withdrawn'] = 'Vous avez retiré x',
	['added'] = 'Vous avez ajouté x',
	['not_enough_place'] = 'Vous n\'avez plus de place',
	['sale_in_prog'] = 'Revente en cours...',
	['van'] = 'Camion de travail',
	['open_menu'] = ' ',

	-- A modifier selon l'entreprise
	['sodaworld_client'] = 'client sodaworld',
	['transforming_in_progress'] = 'Transformation des canette en cours',
	['canette_taken'] = 'Vous être en train de ramasser des canette',
	['press_traitement'] = 'appuyez sur ~INPUT_CONTEXT~ pour transformer votre canette',
	['press_collect'] = 'appuyez sur ~INPUT_CONTEXT~ pour récolter du canette',
	['press_sell'] = 'appuyez sur ~INPUT_CONTEXT~ pour vendre vos produits',
	['no_sprunk_sale'] = 'Vous n\'avez plus ou pas assez de canette sprunk ',
	['no_vin_sale'] = 'Vous n\'avez plus ou pas assez de vin',
	['not_enough_Canette'] = 'Vous n\'avez plus de canette',
	['crystal_pepso'] = '~g~Génial, un crystal pepso !~w~',
	['no_product_sale'] = 'Vous n\'avez plus de produits',
	['press_traitement_sprunk'] = 'appuyez sur ~INPUT_CONTEXT~ pour transformer votre canette',
	['used_sprunk'] = 'Vous avez bu une canette de Sprunk',
	['used_crystal_pepso'] = 'Vous avez bu une bouteille de crystal pepso',
	['used_ecola'] = 'Vous avez bu une canette de Ecola',

}
