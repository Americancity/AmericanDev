-----------------------------------------
-- Created and modify by L'ile Légale RP
-- SenSi and Kaminosekai
-----------------------------------------

ESX = nil
local PlayersTransforming  = {}
local PlayersSelling       = {}
local PlayersHarvesting = {}
local ecola = 1
local sprunk = 1
TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

if Config.MaxInService ~= -1 then
	TriggerEvent('esx_service:activateService', 'soda', Config.MaxInService)
end

TriggerEvent('esx_phone:registerNumber', 'soda', _U('sodaworld_client'), true, true)
TriggerEvent('esx_society:registerSociety', 'soda', 'sodaworld', 'society_soda', 'society_soda', 'society_soda', {type = 'private'})
local function Harvest(source, zone)
	if PlayersHarvesting[source] == true then

		local xPlayer  = ESX.GetPlayerFromId(source)
		if zone == "CanetteFarm" then
			local itemQuantity = xPlayer.getInventoryItem('canette').count
			if itemQuantity >= 50 then
				TriggerClientEvent('esx:showNotification', source, _U('not_enough_place'))
				return
			else
				SetTimeout(1800, function()
					xPlayer.addInventoryItem('canette', 1)
					Harvest(source, zone)
				end)
			end
		end
	end
end

RegisterServerEvent('esx_sodaworldjob:startHarvest')
AddEventHandler('esx_sodaworldjob:startHarvest', function(zone)
	local _source = source

	if PlayersHarvesting[_source] == false then
		TriggerClientEvent('esx:showNotification', _source, '~r~C\'est pas bien de glitch ~w~')
		PlayersHarvesting[_source]=false
	else
		PlayersHarvesting[_source]=true
		TriggerClientEvent('esx:showNotification', _source, _U('canette_taken'))
		Harvest(_source,zone)
	end
end)


RegisterServerEvent('esx_sodaworldjob:stopHarvest')
AddEventHandler('esx_sodaworldjob:stopHarvest', function()
	local _source = source

	if PlayersHarvesting[_source] == true then
		PlayersHarvesting[_source]=false
		TriggerClientEvent('esx:showNotification', _source, 'Vous sortez de la ~r~zone')
	else
		TriggerClientEvent('esx:showNotification', _source, 'Vous pouvez ~g~récolter')
		PlayersHarvesting[_source]=true
	end
end)


local function Transform(source, zone)

	if PlayersTransforming[source] == true then

		local xPlayer  = ESX.GetPlayerFromId(source)
		if zone == "Traitementecola" then
			local itemQuantity = xPlayer.getInventoryItem('canette').count

			if itemQuantity <= 0 then
				TriggerClientEvent('esx:showNotification', source, _U('not_enough_Canette'))
				return
			else
				local rand = math.random(0,100)
				if (rand >= 98) then
					SetTimeout(1800, function()
						xPlayer.removeInventoryItem('canette', 1)
						xPlayer.addInventoryItem('crystal_pepso', 1)
						TriggerClientEvent('esx:showNotification', source, _U('crystal_pepso'))
						Transform(source, zone)
					end)
				else
					SetTimeout(1800, function()
						xPlayer.removeInventoryItem('canette', 1)
						xPlayer.addInventoryItem('ecola', 1)

						Transform(source, zone)
					end)
				end
			end
		elseif zone == "TraitementSprunk" then
			local itemQuantity = xPlayer.getInventoryItem('canette').count
			if itemQuantity <= 0 then
				TriggerClientEvent('esx:showNotification', source, _U('not_enough_Canette'))
				return
			else
				SetTimeout(1800, function()
					xPlayer.removeInventoryItem('canette', 1)
					xPlayer.addInventoryItem('sprunk', 1)

					Transform(source, zone)
				end)
			end
		end
	end
end

RegisterServerEvent('esx_sodaworldjob:startTransform')
AddEventHandler('esx_sodaworldjob:startTransform', function(zone)
	local _source = source

	if PlayersTransforming[_source] == false then
		TriggerClientEvent('esx:showNotification', _source, '~r~C\'est pas bien de glitch ~w~')
		PlayersTransforming[_source]=false
	else
		PlayersTransforming[_source]=true
		TriggerClientEvent('esx:showNotification', _source, _U('transforming_in_progress'))
		Transform(_source,zone)
	end
end)

RegisterServerEvent('esx_sodaworldjob:stopTransform')
AddEventHandler('esx_sodaworldjob:stopTransform', function()

	local _source = source

	if PlayersTransforming[_source] == true then
		PlayersTransforming[_source]=false
		TriggerClientEvent('esx:showNotification', _source, 'Vous sortez de la ~r~zone')

	else
		TriggerClientEvent('esx:showNotification', _source, 'Vous pouvez ~g~transformer votre canette')
		PlayersTransforming[_source]=true

	end
end)

local function Sell(source, zone)

	if PlayersSelling[source] == true then
		local xPlayer  = ESX.GetPlayerFromId(source)

		if zone == 'SellFarm' then
			if xPlayer.getInventoryItem('ecola').count <= 0 then
				ecola = 0
			else
				ecola = 1
			end

			if xPlayer.getInventoryItem('sprunk').count <= 0 then
				sprunk = 0
			else
				sprunk = 1
			end

			if ecola == 0 and sprunk == 0 then
				TriggerClientEvent('esx:showNotification', source, _U('no_product_sale'))
				return
			elseif xPlayer.getInventoryItem('ecola').count <= 0 and sprunk == 0 then
				TriggerClientEvent('esx:showNotification', source, _U('no_ecola_sale'))
				ecola = 0
				return
			elseif xPlayer.getInventoryItem('sprunk').count <= 0 and ecola == 0then
				TriggerClientEvent('esx:showNotification', source, _U('no_sprunk_sale'))
				sprunk = 0
				return
			else
				if (sprunk == 1) then
					SetTimeout(1100, function()
						local money = math.random(6,9)-- player manage money = math.random(50,60)
						xPlayer.removeInventoryItem('sprunk', 1)
						local societyAccount = nil

						TriggerEvent('esx_addonaccount:getSharedAccount', 'society_soda', function(account)
							societyAccount = account
						end)
						if societyAccount ~= nil then
							xPlayer.addMoney(money)
							--societyAccount.addMoney(money)
							TriggerClientEvent('esx:showNotification', xPlayer.source, _U('comp_earned') .. money)
						end
						Sell(source,zone)
					end)
				elseif (ecola == 1) then
					SetTimeout(1100, function()
						local money = math.random(6,9)
						xPlayer.removeInventoryItem('ecola', 1)
						local societyAccount = nil

						TriggerEvent('esx_addonaccount:getSharedAccount', 'society_soda', function(account)
							societyAccount = account
						end)
						if societyAccount ~= nil then
							xPlayer.addMoney(money)
							--societyAccount.addMoney(money)
							TriggerClientEvent('esx:showNotification', xPlayer.source, _U('comp_earned') .. money)
						end
						Sell(source,zone)
					end)
				end

			end
		end
	end
end

RegisterServerEvent('esx_sodaworldjob:startSell')
AddEventHandler('esx_sodaworldjob:startSell', function(zone)

	local _source = source

	if PlayersSelling[_source] == false then
		TriggerClientEvent('esx:showNotification', _source, '~r~C\'est pas bien de glitch ~w~')
		PlayersSelling[_source]=false
	else
		PlayersSelling[_source]=true
		TriggerClientEvent('esx:showNotification', _source, _U('sale_in_prog'))
		Sell(_source, zone)
	end

end)

RegisterServerEvent('esx_sodaworldjob:stopSell')
AddEventHandler('esx_sodaworldjob:stopSell', function()

	local _source = source

	if PlayersSelling[_source] == true then
		PlayersSelling[_source]=false
		TriggerClientEvent('esx:showNotification', _source, 'Vous sortez de la ~r~zone')

	else
		TriggerClientEvent('esx:showNotification', _source, 'Vous pouvez ~g~vendre')
		PlayersSelling[_source]=true
	end

end)

RegisterServerEvent('esx_sodaworldjob:getStockItem')
AddEventHandler('esx_sodaworldjob:getStockItem', function(itemName, count)

	local xPlayer = ESX.GetPlayerFromId(source)

	TriggerEvent('esx_addoninventory:getSharedInventory', 'society_soda', function(inventory)

		local item = inventory.getItem(itemName)

		if item.count >= count then
			inventory.removeItem(itemName, count)
			xPlayer.addInventoryItem(itemName, count)
		else
			TriggerClientEvent('esx:showNotification', xPlayer.source, _U('quantity_invalid'))
		end

		TriggerClientEvent('esx:showNotification', xPlayer.source, _U('have_withdrawn') .. count .. ' ' .. item.label)

	end)

end)

ESX.RegisterServerCallback('esx_sodaworldjob:getStockItems', function(source, cb)

	TriggerEvent('esx_addoninventory:getSharedInventory', 'society_soda', function(inventory)
		cb(inventory.items)
	end)

end)

RegisterServerEvent('esx_sodaworldjob:putStockItems')
AddEventHandler('esx_sodaworldjob:putStockItems', function(itemName, count)

	local xPlayer = ESX.GetPlayerFromId(source)

	TriggerEvent('esx_addoninventory:getSharedInventory', 'society_soda', function(inventory)

		local item = inventory.getItem(itemName)

		if item.count >= 0 then
			xPlayer.removeInventoryItem(itemName, count)
			inventory.addItem(itemName, count)
		else
			TriggerClientEvent('esx:showNotification', xPlayer.source, _U('quantity_invalid'))
		end

		TriggerClientEvent('esx:showNotification', xPlayer.source, _U('added') .. count .. ' ' .. item.label)

	end)
end)

ESX.RegisterServerCallback('esx_sodaworldjob:getPlayerInventory', function(source, cb)

	local xPlayer    = ESX.GetPlayerFromId(source)
	local items      = xPlayer.inventory

	cb({
		items      = items
	})

end)


ESX.RegisterUsableItem('sprunk', function(source)

	local xPlayer = ESX.GetPlayerFromId(source)

	xPlayer.removeInventoryItem('sprunk', 1)

	TriggerClientEvent('esx_status:add', source, 'hunger', 10000)
	TriggerClientEvent('esx_status:add', source, 'thirst', 250000)
	TriggerClientEvent('esx_basicneeds:onDrink', source)
	TriggerClientEvent('esx:showNotification', source, _U('used_sprunk'))

end)

ESX.RegisterUsableItem('crystal_pepso', function(source)

	local xPlayer = ESX.GetPlayerFromId(source)

	xPlayer.removeInventoryItem('crystal_pepso', 1)

	TriggerClientEvent('esx_status:add', source, 'hunger', 20000)
	TriggerClientEvent('esx_status:add', source, 'thirst', 1000000)
	TriggerClientEvent('esx_basicneeds:onDrink', source)
	TriggerClientEvent('esx:showNotification', source, _U('used_crystal_pepso'))

end)

ESX.RegisterUsableItem('ecola', function(source)

	local xPlayer = ESX.GetPlayerFromId(source)

	xPlayer.removeInventoryItem('ecola', 1)

	TriggerClientEvent('esx_status:add', source, 'hunger', 10000)
	TriggerClientEvent('esx_status:add', source, 'thirst', 250000)
	TriggerClientEvent('esx_basicneeds:onDrink', source)
	TriggerClientEvent('esx:showNotification', source, _U('used_ecola'))

end)
