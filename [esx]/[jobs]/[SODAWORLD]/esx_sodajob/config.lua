Config                            = {}
Config.DrawDistance               = 100.0
Config.MaxInService               = -1
Config.EnablePlayerManagement     = false
Config.EnableSocietyOwnedVehicles = false
Config.Locale                     = 'fr'

Config.Zones = {

	CanetteFarm = {
		Pos   = {x = 728.675, y = -1387.609, z = 25.423},
		Size  = {x = 3.5, y = 3.5, z = 1.0},
		Color = {r = 136, g = 243, b = 216},
		Name  = "Récupération des canettes",
		Type  = 1
	},


	Traitementecola = {
		Pos   = {x = 885.375, y = -889.142, z = 25.442},
		Size  = {x = 3.5, y = 3.5, z = 1.0},
		Color = {r = 136, g = 243, b = 216},
		Name  = "Remplissage de canette - eCola",
		Type  = 1
	},

	TraitementSprunk = {
		Pos   = {x = 873.424, y = -954.151, z = 25.268},
		Size  = {x = 3.5, y = 3.5, z = 1.0},
		Color = {r = 136, g = 243, b = 216},
		Name  = "Remplissage de canette - Sprunk",
		Type  = 1
	},

	SellFarm = {
		Pos   = {x = -1487.214, y = -382.634, z = 39.163},
		Size  = {x = 1.5, y = 1.5, z = 1.0},
		Color = {r = 136, g = 243, b = 216},
		Name  = "Vente des produits",
		Type  = 1
	},

	sodaActions = {
		Pos   = {x = 740.272, y = -1399.10, z = 26.883},
		Size  = {x = 1.5, y = 1.5, z = 0.5},
		Color = {r = 255, g = 233, b = 2},
		Name  = "Point d'action",
		Type  = 0
	 },

	VehicleSpawner = {
		Pos   = {x = 746.830, y = -1399.598, z = 25.603},
		Size = {x = 1.5, y = 1.5, z = 0.5},
		Color = {r = 2, g = 53, b = 255},
		Name  = "Garage véhicule",
		Type  = 0
	},

	VehicleSpawnPoint = {
		Pos   = {x = 764.811, y = -1409.888, z = 25.552},
		Size  = {x = 1.5, y = 1.5, z = 1.0},
		Color = {r = 136, g = 243, b = 216},
		Name  = "Spawn point",
		Type  = -1
	},

	VehicleDeleter = {
		Pos   = {x = 758.729, y = -1413.150, z = 25.518},
		Size  = {x = 3.0, y = 3.0, z = 0.5},
		Color = {r = 2, g = 255, b = 65},
		Name  = "Ranger son véhicule",
		Type  = 0
	}

}
