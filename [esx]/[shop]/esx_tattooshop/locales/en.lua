Locales ['en'] = {
	['tattoo_shop_nearby'] = 'appuyer sur ~INPUT_PICKUP~ pour acceder au ~y~tattoo shop~s~.',
	['money_amount']       = '<span style = "color: green;"> $%s</span>',
	['part']               = 'part',
	['go_back_to_menu']    = '< retour',
	['tattoo']             = 'tattoo',
	['tattoos']            = 'tattoos',
	['tattoo_shop']        = 'salon de tatouage',
	['bought_tattoo']      = 'vous avez acheter un ~y~tattoo~s~ pour ~r~$%s~s~',
	['not_enough_money']   = 'vous n\'avez pas ~r~assez d\'argent~s~ pour ce tatouage! il vous manque ~r~$%s~s~'
}
