Citizen.CreateThread(function()
    BikerGang = exports['bob74_ipl']:GetBikerGangObject()

    -- Name
    -------
    -- Lets give our gang a cool and fashioned name
    BikerGang.Name.Set("Nom Du MC", BikerGang.Name.Colors.white, BikerGang.Name.Fonts.font1)
    -- Don't forget to enable it
    BikerGang.Clubhouse.ClubName.Enable(true)


    -- Emblem
    ---------
    -- Choosing the logo and rotating it from 90°
    BikerGang.Emblem.Set(BikerGang.Emblem.Logo.eagle, 90.0)

    -- and enabling it
    BikerGang.Clubhouse.Emblem.Enable(true)


    -- Missions wall
    ----------------
    -- Enabling the wall, it doesn't show any missions by default
    BikerGang.Clubhouse.MissionsWall.Enable(true)

    -- Setting the first mission (left)
    -- Here I choose to use existing missions ("By the pound")
    BikerGang.Clubhouse.MissionsWall.SetMission(BikerGang.Clubhouse.MissionsWall.Position.left, Clubhouse.MissionsWall.Missions.Titles.weaponOfChoice, Clubhouse.MissionsWall.Missions.Descriptions.weaponOfChoice, Clubhouse.MissionsWall.Missions.Pictures.weaponOfChoice, -423.0253, -1681.888)

    -- Here is a custom mission containing the mugshot of the player
    local pedheadshotTexture = exports['bob74_ipl']:GetPedheadshotTexture(GetPlayerPed(-1))
    if (pedheadshotTexture ~= nil) then
        BikerGang.Clubhouse.MissionsWall.SetMission(BikerGang.Clubhouse.MissionsWall.Position.middle, "Objective:", "Find a new president.", pedheadshotTexture, 3000.0, 3000.0)
    end

    -- And there is no third mission
    BikerGang.Clubhouse.MissionsWall.SelectMission(BikerGang.Clubhouse.MissionsWall.Position.none)

    --[[
        Note:

        Be careful when you use Ped headshots (mugshots):
        You must call SetStreamedTextureDictAsNoLongerNeeded(mugshotTexture) before reloading your resource.
        Otherwise, the texture will be lost but it will stay in memory and will take one mughost texture slot for nothing.

        if (mugshotTexture ~= "") then
            SetStreamedTextureDictAsNoLongerNeeded(mugshotTexture)
        end
    ]]


    -- Members pictures
    -------------------
    -- We need a curently loaded Ped to get its mugshot.
    -- That's why we are going to spawn Peds under the ground and remove them later.

    -- Coordinates to spawn the Ped
    x, y, z = table.unpack(GetEntityCoords(GetPlayerPed(-1), true))

    -- Requesting the Ped model
    RequestModel("cs_bradcadaver")
    while not HasModelLoaded("cs_bradcadaver") do Wait(1) end

    -- Spawning the Ped underground and freezing it
    local presidentPed = CreatePed(4, "cs_bradcadaver", x, y, z - 500.0, 0.0, false, true)
    FreezeEntityPosition(presidentPed, true)

    -- Setting the picture by sending the Ped to the function
    BikerGang.Clubhouse.Members.President.Set(presidentPed)
    -- We enable the picture to see it
    BikerGang.Clubhouse.Members.President.Enable(true)

    -- We don't need the Ped anymore, clean up.
    SetPedAsNoLongerNeeded(presidentPed)
    DeletePed(presidentPed)


    RequestModel("mp_m_exarmy_01")
    while not HasModelLoaded("mp_m_exarmy_01") do Wait(1) end
    local vicePresidentPed = CreatePed(4, "mp_m_exarmy_01", x, y, z - 505.0, 0.0, false, true)
    FreezeEntityPosition(vicePresidentPed, true)
    BikerGang.Clubhouse.Members.VicePresident.Set(vicePresidentPed)
    BikerGang.Clubhouse.Members.VicePresident.Enable(true)
    SetPedAsNoLongerNeeded(vicePresidentPed)
    DeletePed(vicePresidentPed)

    RequestModel("hc_gunman")
    while not HasModelLoaded("hc_gunman") do Wait(1) end
    local roadCaptainPed = CreatePed(4, "hc_gunman", x, y, z - 510.0, 0.0, false, true)
    FreezeEntityPosition(roadCaptainPed, true)
    BikerGang.Clubhouse.Members.RoadCaptain.Set(roadCaptainPed)
    BikerGang.Clubhouse.Members.RoadCaptain.Enable(true)
    SetPedAsNoLongerNeeded(roadCaptainPed)
    DeletePed(roadCaptainPed)

    RequestModel("hc_driver")
    while not HasModelLoaded("hc_driver") do Wait(1) end
    local enforcerPed = CreatePed(4, "hc_driver", x, y, z - 515.0, 0.0, false, true)
    FreezeEntityPosition(enforcerPed, true)
    BikerGang.Clubhouse.Members.Enforcer.Set(enforcerPed)
    BikerGang.Clubhouse.Members.Enforcer.Enable(true)
    SetPedAsNoLongerNeeded(enforcerPed)
    DeletePed(enforcerPed)

    RequestModel("mp_m_niko_01")
    while not HasModelLoaded("mp_m_niko_01") do Wait(1) end
    local sergeantAtArmsPed = CreatePed(4, "mp_m_niko_01", x, y, z - 520.0, 0.0, false, true)
    FreezeEntityPosition(sergeantAtArmsPed, true)
    BikerGang.Clubhouse.Members.SergeantAtArms.Set(sergeantAtArmsPed)
    BikerGang.Clubhouse.Members.SergeantAtArms.Enable(true)
    SetPedAsNoLongerNeeded(sergeantAtArmsPed)
    DeletePed(sergeantAtArmsPed)
end)
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- appart
Citizen.CreateThread(function()
    FinanceOrganization = exports['bob74_ipl']:GetFinanceOrganizationObject()

    -- Setting the name parameters
    FinanceOrganization.Name.Set("American City", FinanceOrganization.Name.Style.normal, FinanceOrganization.Name.Colors.black, FinanceOrganization.Name.Fonts.font6)

    -- Enabling the name to be able to see it
    FinanceOrganization.Office.Enable(true)
end)
