Config                            = {}

Config.Teleporters = {
	['Taxi'] = {
		['Job'] = 'none',
		['Enter'] = {
			['x'] = 440.220,
			['y'] = -621.830,
			['z'] = 27.802,
			['Information'] = ''
		},

		['Exit'] = {
			['x'] = 438.120,
			['y'] = -623.014,
			['z'] = 27.728,
			['Information'] = ''
		}
	},

	['H1'] = {
		['Job'] = 'none',
		['Enter'] = {
			['x'] = 333.997,
			['y'] = -569.768,
			['z'] = 42.391,
			['Information'] = ''
		},

		['Exit'] = {
			['x'] = 246.848,
			['y'] = -1372.019,
			['z'] = 23.600,
			['Information'] = ''
		}
	},

	['H2'] = {
		['Job'] = 'none',
		['Enter'] = {
			['x'] = 308.61,
			['y'] = -602.57,
			['z'] = 42.29,
			['Information'] = ''
		},

		['Exit'] = {
			['x'] = 319.416,
			['y'] = -559.777,
			['z'] = 27.760,
			['Information'] = ''
		}
	}

	--Next here
}
