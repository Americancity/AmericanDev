resource_manifest_version '44febabe-d386-4d18-afbe-5e627f4af937'

client_script 'main.lua'

server_scripts {

	'@mysql-async/lib/MySQL.lua',
	'cagoule/server/main.lua'
	
}


client_scripts {
	'cagoule/client/main.lua'
}


ui_page {
	'cagoule/html/ui.html'
}

files {
	'cagoule/html/ui.html',
	'cagoule/html/css/app.css',
	'cagoule/html/js/mustache.min.js',
	'cagoule/html/js/app.js',
	
	
}