--[[    "PLAYER",
  "CIVMALE",
  "CIVFEMALE",
  "COP",
  "SECURITY_GUARD",
  "PRIVATE_SECURITY",
  "FIREMAN",
  "GANG_1",
  "GANG_2",
  "GANG_9",
  "GANG_10",
  "AMBIENT_GANG_LOST",
  "AMBIENT_GANG_MEXICAN",
  "AMBIENT_GANG_FAMILY",
  "AMBIENT_GANG_BALLAS",
  "AMBIENT_GANG_MARABUNTE",
  "AMBIENT_GANG_CULT",
  "AMBIENT_GANG_SALVA",
  "AMBIENT_GANG_WEICHENG",
  "AMBIENT_GANG_HILLBILLY",
  "DEALER",
  "HATES_PLAYER",
  "HEN",
  "WILD_ANIMAL",
  "SHARK",
  "COUGAR",
  "NO_RELATIONSHIP",
  "SPECIAL",
  "MISSION2",
  "MISSION3",
  "MISSION4",
  "MISSION5",
  "MISSION6",
  "MISSION7",
  "MISSION8",
  "ARMY",
  "GUARD_DOG",
  "AGGRESSIVE_INVESTIGATE",
  "MEDIC",
  "PRISONER",
  "DOMESTIC_ANIMAL",
  "DEER"]]

local PlayerData              = {}
ESX = nil

RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
  ESX.PlayerData = xPlayer
  PlayerLoaded = true

end)

RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)
  ESX.PlayerData.job = job
end)
Citizen.CreateThread(function()
  while ESX == nil do
    TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
    Citizen.Wait(0)
  end

  while ESX.GetPlayerData().job == nil do
    Citizen.Wait(10)
  end

  PlayerData = ESX.GetPlayerData()
end)

local cv = false
local viewm = nil
local viewm2 = false

local relationshipTypes = {
  "AMBIENT_GANG_LOST",
}

Citizen.CreateThread(function()
    while true do
        Wait(50)
        ped = PlayerPedId()
          for _, group in ipairs(relationshipTypes) do
              SetRelationshipBetweenGroups(1, GetHashKey('PLAYER'), GetHashKey(group))
              SetRelationshipBetweenGroups(1, GetHashKey(group), GetHashKey('PLAYER'))
          end
    end
end)

local holstered = true

local weapons = {
	"WEAPON_PISTOL",
	"WEAPON_PISTOL_MK2",
  "WEAPON_APPISTOL",
  "WEAPON_HEAVYPISTOL",
	"WEAPON_PISTOL50",
  "WEAPON_SNSPISTOL",
  "WEAPON_VINTAGEPISTOL",
	"WEAPON_FLAREGUN",
  "WEAPON_KNIFE",
  "WEAPON_DAGGER",
	"WEAPON_MACHETE",
  "WEAPON_SNSPISTOL_MK2",
  "WEAPON_REVOLVER",
  "WEAPON_REVOLVER_MK2",
  "WEAPON_DOUBLEACTION",
}
local weaponsC = {
	"WEAPON_COMBATPISTOL",
}
local gunpull = false
Citizen.CreateThread(function()
	while true do
		Citizen.Wait(10)
    if gunpull then
      DisablePlayerFiring(ped, gunpull)
    end
  end
end)
Citizen.CreateThread(function()
	while true do
		Citizen.Wait(0)
		if DoesEntityExist( ped ) and not IsEntityDead( ped ) and not IsPedInAnyVehicle(PlayerPedId(), true) then
			loadAnimDict( "combat@reaction_aim@pistol" )
      loadAnimDict( "reaction@intimidation@1h")
			if CheckWeapon(ped) then
				if holstered then
          gunpull = true
          TaskPlayAnim(ped, "combat@reaction_aim@pistol", "-90", 8.0, 2.0, -1, 48, 10, 0, 0, 0 )
					Citizen.Wait(1200)
					ClearPedTasks(ped)
					holstered = false
          gunpull = false
				end
      elseif CheckWeaponC(ped) then
        if holstered then
          gunpull = true
          TaskPlayAnim(ped, "combat@reaction_aim@pistol", "0", 8.0, 2.0, -1, 48, 10, 0, 0, 0 )
          Citizen.Wait(1200)
          ClearPedTasks(ped)
          holstered = false
          gunpull = false
        end
			elseif not CheckWeapon(ped) then
				if not holstered then
					TaskPlayAnim(ped, "reaction@intimidation@1h", "outro", 8.0, 2.0, -1, 48, 10, 0, 0, 0 )
          gunpull = true
					Citizen.Wait(2000)
					ClearPedTasks(ped)

					holstered = true
          gunpull = false
				end
      end
    end
	end
end)

function CheckWeapon(ped)
	for i = 1, #weapons do
		if GetHashKey(weapons[i]) == GetSelectedPedWeapon(ped) then
			return true
		end
	end
	return false
end

function CheckWeaponC(ped)
	for i = 1, #weaponsC do
		if GetHashKey(weaponsC[i]) == GetSelectedPedWeapon(ped) then
			return true
		end
	end
	return false
end

function loadAnimDict( dict )
	while ( not HasAnimDictLoaded( dict ) ) do
		RequestAnimDict( dict )
		Citizen.Wait( 0 )
	end
end

function getviewmod(view)
  if cv == false then 
    viewm = GetFollowVehicleCamViewMode()
  end
end

Citizen.CreateThread(function()
  SetBlackout(false)
  while true do
    Wait(1)
    getviewmod()
    if (IsPedInAnyVehicle(GetPlayerPed(-1), false)) and IsPedArmed(GetPlayerPed(-1), 7) then
      cv = true
      SetFollowVehicleCamViewMode(4)
    elseif viewm2 == true then
      SetFollowVehicleCamViewMode(viewm)
      viewm2 = false
      cv = false
    else
      viewm2 = true
    end
  end
end)



-- hide in trunk
    local player = PlayerPedId()
    local inside = false

  Citizen.CreateThread(function()
      while true do
        Citizen.Wait(5)
        local speed = GetEntitySpeed(player) 

          player = PlayerPedId()
      local plyCoords = GetEntityCoords(player, false)
      local vehicle = VehicleInFront()

        if IsDisabledControlPressed(0, 19) and IsDisabledControlJustReleased(1, 44) and GetVehiclePedIsIn(player, false) == 0 and DoesEntityExist(vehicle) and IsEntityAVehicle(vehicle) then
        SetVehicleDoorOpen(vehicle, 5, false, false)      
          if not inside then
              AttachEntityToEntity(player, vehicle, -1, 0.0, -2.2, 0.5, 0.0, 0.0, 0.0, false, false, false, false, 20, true)              
              RaiseConvertibleRoof(vehicle, false)
              if IsEntityAttached(player) then
               SetTextComponentFormat("STRING")
               AddTextComponentString('ALT + A pour sortir')
               DisplayHelpTextFromStringLabel(0, 1, 1, -1) 
               ClearPedTasksImmediately(player)
               Citizen.Wait(100)             
               TaskPlayAnim(player, 'timetable@floyd@cryingonbed@base', 'base', 1.0, -1, -1, 1, 0, 0, 0, 0)  
                  if not (IsEntityPlayingAnim(player, 'timetable@floyd@cryingonbed@base', 'base', 3) == 1) then
                    Streaming('timetable@floyd@cryingonbed@base', function()
                    TaskPlayAnim(playerPed, 'timetable@floyd@cryingonbed@base', 'base', 1.0, -1, -1, 49, 0, 0, 0, 0)
                    end)
                end    
                
            inside = true                         
            else
            inside = false
            end         
          elseif inside and IsDisabledControlPressed(0, 19) and IsDisabledControlJustReleased(1, 44) and speed < 1 then
            DetachEntity(player, true, true)
            SetEntityVisible(player, true, true)
            ClearPedTasks(player)   
            inside = false
          ClearAllHelpMessages()          

          end
          Citizen.Wait(2000)
          SetVehicleDoorShut(vehicle, 5, false)     
        end
        if DoesEntityExist(vehicle) and IsEntityAVehicle(vehicle) and not inside and GetVehiclePedIsIn(player, false) == 0 then
 
      elseif DoesEntityExist(vehicle) and inside then
            car = GetEntityAttachedTo(player)
            carxyz = GetEntityCoords(car, 0)
            local visible = true
            DisableAllControlActions(0)
            DisableAllControlActions(1)
            DisableAllControlActions(2)
            EnableControlAction(0, 0, true) --- V - camera
            EnableControlAction(0, 249, true) --- N - push to talk  
            EnableControlAction(2, 1, true) --- camera moving
            EnableControlAction(2, 2, true) --- camera moving 
            EnableControlAction(0, 177, true) --- BACKSPACE
            EnableControlAction(0, 200, true) --- ESC                  
      elseif not DoesEntityExist(vehicle) and inside then
            DetachEntity(player, true, true)
            SetEntityVisible(player, true, true)
            ClearPedTasks(player)     
            inside = false    
            ClearAllHelpMessages()                
      end   
      end
  end)

function Streaming(animDict, cb)
  if not HasAnimDictLoaded(animDict) then
    RequestAnimDict(animDict)

    while not HasAnimDictLoaded(animDict) do
      Citizen.Wait(1)
    end
  end

  if cb ~= nil then
    cb()
  end
end 
function VehicleInFront()
    local pos = GetEntityCoords(player)
    local entityWorld = GetOffsetFromEntityInWorldCoords(player, 0.0, 6.0, 0.0)
    local rayHandle = CastRayPointToPoint(pos.x, pos.y, pos.z, entityWorld.x, entityWorld.y, entityWorld.z, 10, player, 0)
    local _, _, _, _, result = GetRaycastResult(rayHandle)
    return result
end
local pedindex = {}



function SetWeaponDrops()
    local handle, ped = FindFirstPed()
    local finished = false 

    repeat 
        if not IsEntityDead(ped) then
            SetPedDropsWeaponsWhenDead(ped, false) 
        end
        finished, ped = FindNextPed(handle)
    until not finished

    EndFindPed(handle)
end

Citizen.CreateThread(function()
    while true do
        SetWeaponDrops()
        Citizen.Wait(500)
    end
end)


function ShowNotification(text)
  SetNotificationTextEntry("STRING")
  AddTextComponentString(text)
  DrawNotification(false, false)
end
