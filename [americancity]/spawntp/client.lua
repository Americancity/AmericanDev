AddEventHandler('esx_jobs:publicTeleports', function(position)
  SetEntityCoords(GetPlayerPed(-1), position.x, position.y, position.z)
end)


-- Show top left hint
Citizen.CreateThread(function()
  while true do
    Wait(0)
    if hintIsShowed == true then
      SetTextComponentFormat("STRING")
      AddTextComponentString(hintToDisplay)
      DisplayHelpTextFromStringLabel(0, 0, 1, -1)
    end
  end
end)

-- Display public markers
Citizen.CreateThread(function()
  while true do
    Wait(0)
    local coords = GetEntityCoords(GetPlayerPed(-1))
    for k,v in pairs(Config.PublicZones) do
      if(v.Marker ~= -1 and GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < Config.DrawDistance) then
        DrawMarker(v.Marker, v.Pos.x, v.Pos.y, v.Pos.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, v.Size.x, v.Size.y, v.Size.z, v.Color.r, v.Color.g, v.Color.b, 100, false, true, 2, false, false, false, false)
      end
    end
  end
end)

-- Activate public marker
Citizen.CreateThread(function()
  while true do
    Wait(0)
    local coords      = GetEntityCoords(GetPlayerPed(-1))
    local position    = nil
    local zone        = nil


    if IsControlJustReleased(0, 38) and isInPublicMarker then
      TriggerEvent('esx_jobs:publicTeleports', position)
    end

    for k,v in pairs(Config.PublicZones) do
      if(GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < v.Size.x) then
        isInPublicMarker = true
        position = v.Teleport
        zone = v
        break
      else
        isInPublicMarker  = false
      end
    end

    -- hide or show top left zone hints
    if isInPublicMarker then
      hintToDisplay = zone.Hint
      hintIsShowed = true
    else
      if not isInMarker then
        hintToDisplay = "No Hint to Display!"
        hintIsShowed = false
      end
    end
  end
end)
