-------------- DO NOT TRADE/SELL THIS SCRIPT IT IS 100% FREE ON FIVEM FORUMS ------------------------
--[[
	The Tutorial System [BY KRIZFROST]
]]
--[[
	Tips:
	• DO NOT EDIT THE CODE UNLESS YOU KNOW WHAT YOU ARE DOING
	Information:
	• The only reason I had implemented ESX is to display the help notifications as well as the basic notifications that appear besides that this could honestly be a standalone script
	• To change the prices change the price on in server.lua
	• If you'd like to get help on this and see more of my projects join the discord https://discord.gg/yjyugBu
	Credits:
	• KrizFrost - Base Code

]]
--- Blip Added

local blips = {
    -- Example {title="", colour=, id=, x=, y=, z=},

     {title="Tutorial", colour=17, id=66, x = -195.14, y = -830.31, z = 29.8}
  }

Citizen.CreateThread(function()

    for _, info in pairs(blips) do
      info.blip = AddBlipForCoord(info.x, info.y, info.z)
      SetBlipSprite(info.blip, info.id)
      SetBlipDisplay(info.blip, 4)
      SetBlipScale(info.blip, 1.0)
      SetBlipColour(info.blip, info.colour)
      SetBlipAsShortRange(info.blip, true)
	  BeginTextCommandSetBlipName("STRING")
      AddTextComponentString(info.title)
      EndTextCommandSetBlipName(info.blip)
    end
end)

Key = 38 --- E


local ESX = nil
local PlayerData                = {}
--[[
	Loading ESX Data
]]

RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
  PlayerData = xPlayer
end)


RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)
  PlayerData.job = job
end)

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(0)
	end
end)

--[[
	Location of Tutorial Spot If you change this you need to change the blip location as well
]]
TutorialStation = {
	{-195.14, -830.31, 29.8}
}


function DrawSpecialText(m_text, showtime)
	SetTextEntry_2("STRING")
	AddTextComponentString(m_text)
	DrawSubtitleTimed(showtime, 1)
end

Citizen.CreateThread(function ()
	while true do
		Citizen.Wait(0)
			for i = 1, #TutorialStation do
				tutcoords = TutorialStation[i]
				DrawMarker(-27, tutcoords[1], tutcoords[2], tutcoords[3], 0, 0, 0, 0, 0, 0, 5.0, 5.0, 2.0, 0, 157, 0, 155, 0, 0, 2, 0, 0, 0, 0)
				if GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)), tutcoords[1], tutcoords[2], tutcoords[3], true ) < 5 then
				 ESX.ShowHelpNotification('Appuyer sur ~INPUT_CONTEXT~ pour ouvrir les menu')
					if(IsControlJustPressed(1, Key)) then
						TutorialFunction() --- Tutorial Function
						insidemarkercheck = true
					end
				end
				if GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)), tutcoords[1], tutcoords[2], tutcoords[3], true ) > 5 then
					    if ESX.UI.Menu.IsOpen('default', GetCurrentResourceName(), 'civtutorial_menu') then
       						ESX.UI.Menu.CloseAll()
   					end
				end
			end
	end
end)
local menuEnabled = false
function ToggleActionMenu()
	-- Make the menuEnabled variable not itself
	-- e.g. not true = false, not false = true
	menuEnabled = not menuEnabled

	if ( menuEnabled ) then
		-- Focuses on the NUI, the second parameter toggles the
		-- onscreen mouse cursor.
		SetNuiFocus( true, true )

		-- Sends a message to the JavaScript side, telling it to
		-- open the menu.
		SendNUIMessage({
			showmenu = true
		})
	else
		-- Bring the focus back to the game
		SetNuiFocus( false )

		-- Sends a message to the JavaScript side, telling it to
		-- close the menu.
		SendNUIMessage({
			hidemenu = true
		})
	end
end


--DO-NOT-EDIT-BELLOW-THIS-LINE--
--DO-NOT-EDIT-BELLOW-THIS-LINE--
--DO-NOT-EDIT-BELLOW-THIS-LINE--
--DO-NOT-EDIT-BELLOW-THIS-LINE--

function TutorialFunction() -- Tutorial Menu and Majic ( DO NOT EDIT UNLESS YOU KNOW WHAT YOU ARE DOING )

    local elements = {
        { label = 'City tour', value = "basic_tutorial" }
    }

    ESX.UI.Menu.Open(
        'default', GetCurrentResourceName(), 'civtutorial_menu',
        {
            title    = "<font color='lightblue' font-size='15px'>Tutorial Menu</font>",
            align    = 'center',
            elements = elements
        },
    function(data, menu)

        local action = data.current.value

        if action == "basic_tutorial" then
       local playerPed = GetPlayerPed(-1)
	local coords    = GetEntityCoords(playerPed)
	local cam = CreateCam("DEFAULT_SCRIPTED_CAMERA", 1)
    TriggerEvent('chat:clear')  --- Clear current chat
    TriggerEvent('chat:toggleChat') --- Toggle chat chat
    createdCamera = cam --- Create Cam
    SetEntityVisible(playerPed, false, 0) --- Make Player Invisible
    SetEntityCoordsNoOffset(playerPed, -103.8, -921.06, 287.29, false, false, false, true) --- Teleport Infront of Maze Bank IN Air
    FreezeEntityPosition(playerPed, true) --- Freeze The Player There
    SetFocusEntity(playerPed) ---- Focus on the player (To Render the building)
    SetCamCoord(cam, -103.8, -921.06, 287.29) --- Set the camera there
    RenderScriptCams(1, 0, 0, 1, 1) --- Render Cams
    ESX.ShowHelpNotification('Binevenue dans ce city tour d\' ~b~American City~w~. ')
    Citizen.Wait(250)
    Citizen.Wait(5000)
    ESX.ShowHelpNotification('Ce tutorial vous présenteras les lieux important de la ville.')
    Citizen.Wait(6050)
    ESX.ShowHelpNotification('commençons')
    Citizen.Wait(2000)
    Citizen.Trace('By KrizFrost, modified by Jobscraft')
    DoScreenFadeOut(800) --- Fade Out the screen
    Citizen.Wait(3000) --- Wait (3 Seconds)
    SetEntityCoordsNoOffset(playerPed, -249.46, -991.05, 35.23, false, false, false, true) --- Teleport Player Infront Job Center
    FreezeEntityPosition(playerPed, true) --- Freeze Player
    SetFocusEntity(playerPed) --- Focus on the player (To Render Building)
    Citizen.Wait(800) --- Wait 0.8 Seconds
    SetCamCoord(cam, -249.46, -991.05, 35.23) -- Set the Camera Infront of the Job Center
    RenderScriptCams(1, 0, 0, 1, 1) -- Render Cams
    DoScreenFadeIn(800) --- Fade In Showing the Screen
    Citizen.Wait(1000)
    Citizen.Trace('By KrizFrost, modified by Jobscraft')
    ESX.ShowHelpNotification('~b~Pole Emploie\n~w~ ici vous pourrez choisir votre premier job.')
    Citizen.Wait(5000)
    ESX.ShowNotification('celui-ci est libre d\'accés, dans certain job vous pourrez vendre vos resource aux joueurs')
    Citizen.Wait(2000)
    ESX.ShowNotification('pour les entreprises privés vous devrez candidater sur le discord ou directement en ville .')
    Citizen.Wait(8000)
    DoScreenFadeOut(800) --- Fade Out the screen
    Citizen.Wait(3000) --- Wait (3 Seconds)
    SetEntityCoordsNoOffset(playerPed, 205.83, -823.55, 35.42, false, false, false, true) --- Teleport Player Infront Legion Garage
    FreezeEntityPosition(playerPed, true) --- Freeze Player
    SetFocusEntity(playerPed) --- Focus on the player (To Render Building)
    Citizen.Wait(800) --- Wait 0.8 Seconds
    SetCamCoord(cam, 205.83, -823.55, 35.42) -- Set the Camera Infront of the Legion Garage
    RenderScriptCams(1, 0, 0, 1, 1) -- Render Cams
    DoScreenFadeIn(800) --- Fade In Showing the Screen
    Citizen.Trace('By KrizFrost, modified by Jobscraft')
    Citizen.Wait(1000)
    ESX.ShowHelpNotification('~b~Les Garages\n~w~il y a plusieurs garage où vous pourrez stocker vos vehicules.')
    Citizen.Wait(9000)
    ESX.ShowHelpNotification('tout les véhicules on un coffre qui est sauvgarder')
    Citizen.Wait(9000)
    ESX.ShowHelpNotification('~o~Rappel:\ntout abus du systeme de coffre (notament pour l\'illegal) sera tres fortement sanctionné')
    Citizen.Wait(9000)
    DoScreenFadeOut(800) --- Fade Out the screen
    Citizen.Wait(3000) --- Wait (3 Seconds)
    SetEntityCoordsNoOffset(playerPed, 1123.065, 2703.52, 55.96, false, false, false, true) --- Teleport Player Infront Pink Hotel
    FreezeEntityPosition(playerPed, true) --- Freeze Player
    SetFocusEntity(playerPed) --- Focus on the player (To Render Building)
    Citizen.Wait(800) --- Wait 0.8 Seconds
    SetCamCoord(cam, 1123.065, 2703.52, 53.96) -- Set the Camera Infront of the Pink Hotel
    RenderScriptCams(1, 0, 0, 1, 1) -- Render Cams
    SetCamRot(cam, 0.0, 0.0, 174.89)
    DoScreenFadeIn(800) --- Fade In Showing the Screen
    Citizen.Wait(1000)
    ESX.ShowHelpNotification('~b~Propriétés\n~w~il y en as plus d\'une centaine dans la ville')
    Citizen.Wait(9000)
    ESX.ShowHelpNotification('vous pouvez y stocker des armes, items et de l\'argent.')
    Citizen.Wait(8000)
    ESX.ShowHelpNotification('il y as plusieurs prix différent, pas assez d\'argent pour l\'acheter? louez la !')
    Citizen.Wait(9000)
    ESX.ShowHelpNotification('~o~Rappel:\nil est interdit d\'abuser du systeme de stockage (coffre commun a toutes vos maisons)')
    Citizen.Wait(8000)
    DoScreenFadeOut(800) --- Fade Out the screen
    Citizen.Wait(3000) --- Wait (3 Seconds)
    SetEntityCoordsNoOffset(playerPed, 1305.59, -1624.05, 58.97, false, false, false, true) --- Teleport Player Infront Grove Street
    FreezeEntityPosition(playerPed, true) --- Freeze Player
    SetFocusEntity(playerPed) --- Focus on the player (To Render Building)
    Citizen.Wait(800) --- Wait 0.8 Seconds
    SetCamCoord(cam, 1305.59, -1624.05, 58.97) -- Set the Camera Infront of the Grove Street
    RenderScriptCams(1, 0, 0, 1, 1) -- Render Cams
    SetCamRot(cam, 0.0, 0.0, 214.0)
    DoScreenFadeIn(800) --- Fade In Showing the Screen
    Citizen.Wait(1000)
    ESX.ShowHelpNotification('~r~tatoo\n~w~comme les magasins de vêtements, vous pouvez personnaliser votre perso ')
    Citizen.Wait(9000)
    ESX.ShowHelpNotification('~o~Rappel:\nAttention! les tatooages sont permanant!')
    Citizen.Wait(8000)
    DoScreenFadeOut(800) --- Fade Out the screen
    Citizen.Wait(3000) --- Wait (3 Seconds)
    SetEntityCoordsNoOffset(playerPed, 6.44, -1140.07, 31.93, false, false, false, true) --- Teleport Player Infront Ammunation
    FreezeEntityPosition(playerPed, true) --- Freeze Player
    SetFocusEntity(playerPed) --- Focus on the player (To Render Building)
    Citizen.Wait(800) --- Wait 0.8 Seconds
    SetCamCoord(cam, 6.44, -1140.07, 31.93) -- Set the Camera Infront of the Ammunation
    RenderScriptCams(1, 0, 0, 1, 1) -- Render Cams
    SetCamRot(cam, 0.0, 0.0, 0.0)
    DoScreenFadeIn(800) --- Fade In Showing the Screen
    Citizen.Wait(1000)
    ESX.ShowHelpNotification('~b~Permis Port d\'Armes\n~w~vous pouvez vous en procurer un via la LSPD')
    Citizen.Wait(9000)
    ESX.ShowHelpNotification('vous pourez ensuite acheter vos armes ici.')
    Citizen.Wait(8000)
    DoScreenFadeOut(800) --- Fade Out the screen
    Citizen.Wait(3000) --- Wait (3 Seconds)
    SetEntityCoordsNoOffset(playerPed, 1885.56, 2596.50, 70.71, false, false, false, true) --- Teleport Player prison
    FreezeEntityPosition(playerPed, true) --- Freeze Player
    SetFocusEntity(playerPed) --- Focus on the player (To Render Building)
    Citizen.Wait(800) --- Wait 0.8 Seconds
    SetCamCoord(cam, 1885.84, 2596.50, 70.71) -- Set the Camera Infront of the Drug Dealer
    RenderScriptCams(1, 0, 0, 1, 1) -- Render Cams
    SetCamRot(cam, -45.0, 0.0, 89.81)
    DoScreenFadeIn(800) --- Fade In Showing the Screen
    Citizen.Wait(1000)
    ESX.ShowHelpNotification('~r~Prison fédéral\n~w~seul les visites sont autorisé. une évasion doit étre voter')
    Citizen.Wait(9000)
    ESX.ShowHelpNotification('~o~Rappel:\ntoutes évasion non validé serat sanctionné')
    Citizen.Wait(8000)
    DoScreenFadeOut(800) --- Fade Out the screen
    Citizen.Wait(3000) --- Wait (3 Seconds)
    SetEntityCoordsNoOffset(playerPed, 287.89, -580.68, 44.30, false, false, false, true) --- Teleport Player Hospital
    FreezeEntityPosition(playerPed, true) --- Freeze Player
    SetFocusEntity(playerPed) --- Focus on the player (To Render Building)
    Citizen.Wait(800) --- Wait 0.8 Seconds
    SetCamCoord(cam, 287.89, -580.68, 44.30) -- Set the Camera Infront of the Hospital
    RenderScriptCams(1, 0, 0, 1, 1) -- Render Cams
    SetCamRot(cam, 0.0, 0.0, -90.0)
    DoScreenFadeIn(800) --- Fade In Showing the Screen
    Citizen.Wait(1000)
    ESX.ShowHelpNotification('~r~Hôpital\n~w~lors que vous n\'étes pas relever par un medecin, vous respawnez ici')
    Citizen.Wait(9000)
    ESX.ShowHelpNotification('les mêmes règles que le coma simple s\'apliques. cepandant votre esprit sera un peut confus (oubliez 2/3 detail sur la cause) ')
    Citizen.Wait(8000)
    ESX.ShowHelpNotification('ce tour est as présent terminer, l\'équipe d\'American city vous souhaite un bon jeux')
    SetEntityCoordsNoOffset(playerPed, -195.14, -830.31, 31.08, false, false, false, true) --- Teleport Player Back to start
    FreezeEntityPosition(playerPed, true) --- Freeze The Player There
    SetEntityVisible(playerPed, true, 0) --- set visable
    FreezeEntityPosition(playerPed, false) -- unfreeze
    DestroyCam(createdCamera, 0)
    DestroyCam(createdCamera, 0)
    RenderScriptCams(0, 0, 1, 1, 1)
    createdCamera = 0
    ClearTimecycleModifier("scanline_cam_cheap")
    SetFocusEntity(GetPlayerPed(PlayerId()))
    ---- End Delete Camera
        end

        menu.close()

    end, function(data, menu)
        menu.close()
    end)

end
-------------- DO NOT TRADE/SELL THIS SCRIPT IT IS 100% FREE ON FIVEM FORUMS ------------------------
-------------- DO NOT TRADE/SELL THIS SCRIPT IT IS 100% FREE ON FIVEM FORUMS ------------------------
-------------- DO NOT TRADE/SELL THIS SCRIPT IT IS 100% FREE ON FIVEM FORUMS ------------------------
-------------- DO NOT TRADE/SELL THIS SCRIPT IT IS 100% FREE ON FIVEM FORUMS ------------------------
-------------- DO NOT TRADE/SELL THIS SCRIPT IT IS 100% FREE ON FIVEM FORUMS ------------------------
