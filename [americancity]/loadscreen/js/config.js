var config =
{    
    /*
        Do we want to show the image?
        Note that imageSize still affects the size of the image and where the progressbars are located.
    */
    enableImage: true,
 
    /*
        Relative path the the logo we want to display.
    */
    image: "img/logo.png",

    /*
        Cursor image
    */
    cursorImage: "img/cursor.png",
 
    /*
        How big should the logo be?
        The values are: [width, height].
        Recommended to use square images less than 1024px.
    */
    imageSize: [512, 512],
 
    /*
        Define the progressbar type
            0 = Single progressbar
            1 = Multiple progressbars
            2 = Collapsed progressbars
     */
    progressBarType: 1,
 
    /*
        Here you can disable some of progressbars.
        Only applys if `singleProgressbar` is false.
    */
    progressBars:
    {
        "INIT_CORE": {
            enabled: false, //NOTE: Disabled because INIT_CORE seems to not get called properly. (race condition).
        },
 
        "INIT_BEFORE_MAP_LOADED": {
            enabled: true,
        },
 
        "MAP": {
            enabled: true,
        },
 
        "INIT_AFTER_MAP_LOADED": {
            enabled: true,
        },
 
        "INIT_SESSION": {
            enabled: false,
        }
    },
 
    /*
        Loading messages will be randomly picked from the array.
        The message is located on the left side above the progressbar.
        The text will slowly fade in and out, each time with another message.
        You can use UTF-8 emoticons inside loading messages!
    */
    loadingMessages:
    [
        "Chargement de vos bagages... 🛄",
        "Largage de vos bagages en cour... 🚮",
        "Toutes utilisation des WC abusive sera sanctionné !🚽",
        "En cas de surcharge, La compagnie s'autorise a vous ejecter en plein vol.... 🚻",
        "Nous vous rappelons qu'un dealer est disponnible en arriere cabine... 🚬",
        "En cas d'urgence, des parachutes en papier sont diponible sous vos sieges... 📜",
        "La temperature exterieur est actuelemnt de - 37 degrés ... 🌡",
        "L'achat de votre billet inscrit automatiquement la compagnie sur votre testament. 📄",
        "Merci de pas ouvrir les portes durant le vol ... 🚪",
        "American city airline vous souhaite la bienvenue a Los Santos ✈",
        "Ouf!",
        "Nous vous rappelons que la compagnie n'est responssable d'aucun deces lier au vol ... ⚖",
        "Pour toutes consomation de drogue merci de vous rendre dans le compartiment prevus a cette effet ... 🚭",
    ],
 
    /*
        Rotate the loading message every 5000 milliseconds (default value).
    */
    loadingMessageSpeed: 5 * 1000,
 
    /*
        Array of music id's to play in the loadscreen.
        Enter your youtube video id's here. In order to obtain the video ID
        Take whats after the watch?v= on a youtube link.
        https://www.youtube.com/watch?v=<videoid>
        Do not include the playlist id or anything, it should be a 11 digit code.
       
        Do not use videos that:
            - Do not allow embedding.
            - Copyrighted music (youtube actively blocks this).
    */
    music:
    [
        "T38v3-SSGcM", "mmDVrZKnk_k", "GMDbnONFLBU", "J1H2_VTGtk0",
        "mMAneUKqXJc", "F_J2t3VgWVQ", "H0h6GEFek5w", "raE7uP2L1Vo",
    ],
 
 
    /*
        Set to false if you do not want any music.
    */
    enableMusic: true,
 
    /*
        Default volume for the player. Please keep this under 50%, to not blowout someones eardrums x)
     */
    musicVolume: 10,
 
    /*
        Should the background change images?
        True: it will not change backgrounds.
        False: it will change backgrounds.
    */
    staticBackground: false,
   
    /*
        Array of images you'd like to display as the background.
        Provide a path to a local image, using images via url is not recommended.
    */
    background:
    [
        "img/bg1.jpg",
        "img/bg2.jpg",
        "img/bg3.jpg",
		"img/bg4.jpg",
    ],
 
    /*
        Time in milliseconds on how fast the background
        should swap images.
     */
    backgroundSpeed: 10 * 1000,

    /*
        Which style of animation should the background transition be?
        zoom = background zooms in and out.
        slide = transtion backgrounds from sliding right and back again.
        fade = fade the background out and back in.
    */
    backgroundStyle: "zoom",

    /*
        An on-screen logger, for indicating breaking bugs! :(
        Should we enable the on-screen logger? Handy for debugging or indicating errornous behaviour!
    */
    enableLog: true,
}
